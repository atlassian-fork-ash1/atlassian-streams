package com.atlassian.streams.pageobjects;

import javax.inject.Inject;

import com.atlassian.pageobjects.PageBinder;

import org.openqa.selenium.WebDriver;

public class Gadget
{
    @Inject
    private WebDriver driver;
    
    @Inject
    private PageBinder pageBinder;
    
    private final String id;

    public Gadget(String id)
    {
        this.id = id;
    }
    
    public <T> T viewAs(Class<T> cls)
    {
        driver.switchTo().frame(id);
        return pageBinder.bind(cls);
    }

}
