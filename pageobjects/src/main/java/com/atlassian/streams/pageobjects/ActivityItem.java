package com.atlassian.streams.pageobjects;

import java.net.URI;
import java.util.NoSuchElementException;
import java.util.concurrent.atomic.AtomicReference;

import javax.inject.Inject;

import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.streams.api.common.Option;
import com.atlassian.webdriver.AtlassianWebDriver;

import com.google.common.base.Function;
import com.google.common.base.Predicate;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import static com.atlassian.streams.api.common.Option.none;
import static com.atlassian.streams.api.common.Option.some;
import static com.atlassian.streams.pageobjects.WebElements.exists;
import static com.google.common.collect.Iterables.find;
import static com.google.common.collect.Iterables.transform;

/**
 * Represents a single entry in the Activity Stream UI.
 */
public class ActivityItem
{
    private static final By SUMMARY = By.className("activity-item-summary");
    private static final By DESCRIPTION = By.className("activity-item-description");

    @Inject
    private AtlassianWebDriver driver;
    
    @Inject
    private PageBinder pageBinder;

    private final AtomicReference<WebElement> container;

    @WaitUntil
    public void waitUntilLoadingIsFinished()
    {
        driver.waitUntil(new Function<WebDriver, Boolean>()
        {
            public Boolean apply(WebDriver driver)
            {
                return !driver.findElement(By.className("activity-stream-container")).getAttribute("class").contains("loading");
            }
        });
    }
    
    public ActivityItem(WebElement container)
    {
        this.container = new AtomicReference<WebElement>(container);
    }

    public String getSummaryText()
    {
        return container.get().findElement(SUMMARY).getText();
    }
    
    public String getDescriptionText()
    {
        if (hasDescription())
        {
            return container.get().findElement(DESCRIPTION).getText();
        }
        else
        {
            return null;
        }
    }
    
    public boolean hasDescription()
    {
        return exists(container.get(), DESCRIPTION);
    }
    
    public Iterable<ActivityItemInlineAction> getInlineActions()
    {
        return transform(container.get().findElements(By.className("activity-item-action")), toInlineAction);
    }
    
    /**
     * Gets the first inline action with the given textual display.
     * 
     * @param withText
     * @return
     */
    public Option<ActivityItemInlineAction> getInlineAction(String withText)
    {
        try
        {
            return some(find(getInlineActions(), withText(withText)));
        }
        catch (NoSuchElementException e)
        {
            return none();
        }
    }
    
    public WebElement getBy(By by)
    {
        if (exists(container.get(), by))
        {
            return container.get().findElement(by);
        }
        else
        {
            return null;
        }
    }
    
    /**
     * Follow the streams entry's alternate link. Return the page type specified after following the link.
     * 
     * @param <T> the type to return
     * @param clazz the type to return
     * @return the page navigated to after following the alternate link
     */
    public <T> T followAlternateLink(Class<T> clazz)
    {
        String url = container.get().findElement(By.className("activity-item-link")).getAttribute("value");
        driver.navigate().to(url);
        return pageBinder.bind(clazz, URI.create(url));
    }
    
    private Function<WebElement, ActivityItemInlineAction> toInlineAction = new Function<WebElement, ActivityItemInlineAction>()
    {
        public ActivityItemInlineAction apply(WebElement item)
        {
            return pageBinder.bind(ActivityItemInlineAction.class, item, container.get());
        }
    };
    
    private Predicate<ActivityItemInlineAction> withText(String withText)
    {
        return new WithText(withText);
    }
    
    private static class WithText implements Predicate<ActivityItemInlineAction> 
    {
        private final String text;
        
        public WithText(String text)
        {
            this.text = text;
        }
        
        public boolean apply(ActivityItemInlineAction inlineAction)
        {
            return inlineAction.getText().equalsIgnoreCase(text);
        }
    };
}
