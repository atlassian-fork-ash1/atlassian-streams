package com.atlassian.streams.thirdparty.rest;

import java.net.URI;
import java.util.Map;

import com.atlassian.streams.thirdparty.api.Activity;
import com.atlassian.streams.thirdparty.api.ActivityQuery;

import com.google.common.collect.ImmutableMap;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Iterables.size;

public class LinkBuilder
{
    private final ThirdPartyStreamsUriBuilder uriBuilder;

    public LinkBuilder(ThirdPartyStreamsUriBuilder uriBuilder)
    {
        this.uriBuilder = checkNotNull(uriBuilder, "uriBuilder");
    }
    
    public Map<String, URI> build(Iterable<Activity> activities, ActivityQuery query)
    {
        int startIndex = query.getStartIndex();
        int maxResults = query.getMaxResults();
        ImmutableMap.Builder<String, URI> links = ImmutableMap.builder();

        links.put("self", uriBuilder.buildActivityCollectionUri(maxResults, startIndex));
        if (startIndex != 0)
        {
            links.put("prev", uriBuilder.buildActivityCollectionUri(maxResults, Math.max(0, startIndex - maxResults)));
        }
        if (size(activities) == maxResults)
        {
            links.put("next", uriBuilder.buildActivityCollectionUri(maxResults, startIndex + maxResults));
        }

        return links.build();
    }

    public Map<String, URI> build(Activity activity)
    {
        ImmutableMap.Builder<String, URI> links = ImmutableMap.builder();

        for (Long activityId : activity.getActivityId())
        {
            links.put("self", uriBuilder.buildActivityUri(activityId));
            links.put("activities", uriBuilder.buildActivityCollectionUri());
        }

        return links.build();
    }
}
