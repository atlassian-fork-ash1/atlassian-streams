package com.atlassian.streams.thirdparty.ao;

import java.net.URI;

import net.java.ao.Entity;
import net.java.ao.Preload;

@Preload ()
public interface ActorEntity extends Entity
{
    public String getUsername();
    public void setUsername(String username);
    
    public String getFullName();
    public void setFullName(String fullName);
    
    public URI getProfilePageUri();
    public void setProfilePageUri(URI profilePageUri);
    
    public URI getProfilePictureUri();
    public void setProfilePictureUri(URI profilePictureUri);
}
