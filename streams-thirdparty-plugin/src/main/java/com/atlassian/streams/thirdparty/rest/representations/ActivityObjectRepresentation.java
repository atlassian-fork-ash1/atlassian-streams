package com.atlassian.streams.thirdparty.rest.representations;

import java.net.URI;

import com.atlassian.streams.api.Html;
import com.atlassian.streams.api.UserProfile;
import com.atlassian.streams.api.common.Either;
import com.atlassian.streams.api.common.Option;
import com.atlassian.streams.thirdparty.api.ActivityObject;
import com.atlassian.streams.thirdparty.api.ValidationErrors;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;

import static com.atlassian.streams.api.Html.html;
import static com.atlassian.streams.api.Html.htmlToString;
import static com.atlassian.streams.api.common.Either.left;
import static com.atlassian.streams.api.common.Either.right;
import static com.atlassian.streams.api.common.Option.none;
import static com.atlassian.streams.api.common.Option.option;
import static com.google.common.base.Functions.toStringFunction;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * JSON representation class used by the Activity REST API.
 */
public class ActivityObjectRepresentation
{
    @JsonProperty String content;
    @JsonProperty String displayName;
    @JsonProperty String id;
    @JsonProperty MediaLinkRepresentation image;
    @JsonProperty String objectType;
    @JsonProperty String summary;
    @JsonProperty String url;

    /**
     * Constructor used for Jackson deserialization.  Use {@link #builder()} for all other purposes.
     */
    @JsonCreator
    public ActivityObjectRepresentation(@JsonProperty("content") String content,
                                        @JsonProperty("displayName") String displayName,
                                        @JsonProperty("id") String id,
                                        @JsonProperty("image") MediaLinkRepresentation image,
                                        @JsonProperty("objectType") String objectType,
                                        @JsonProperty("summary") String summary,
                                        @JsonProperty("uri") String url)
    {
        this.content = content;
        this.displayName = displayName;
        this.id = id;
        this.image = image;
        this.objectType = objectType;
        this.summary = summary;
        this.url = url;
    }

    /**
     * Returns a {@link Builder} for creating a new instance of this class.
     */
    public static Builder builder()
    {
        return new Builder();
    }

    private ActivityObjectRepresentation(Builder builder)
    {
        this.content = builder.content.getOrElse((String) null);
        this.displayName = builder.displayName.getOrElse((String) null);
        this.id = builder.id.getOrElse((String) null);
        this.image = builder.image.getOrElse((MediaLinkRepresentation) null);
        this.objectType = builder.objectType.getOrElse((String) null);
        this.summary = builder.summary.map(htmlToString()).getOrElse((String) null);
        this.url = builder.url.getOrElse((String) null);
    }

    public Either<ValidationErrors, ActivityObject> toActivityObject()
    {
        return ActivityObject.builder()
            .displayName(option(getDisplayName()))
            .idString(option(getId()))
            .typeString(option(getObjectType()))
            .summary(option(getSummary()).map(html()))
            .urlString(option(getUrl()))
            .build();
    }

    public Either<ValidationErrors, UserProfile> toUserProfile()
    {
        ValidationErrors.Builder errors = new ValidationErrors.Builder();
        Option<URI> profileUri = errors.checkAbsoluteUriString(option(url), "url");
        Option<URI> pictureUri;
        if (image == null)
        {
            pictureUri = none(URI.class);
        }
        else
        {
            pictureUri = errors.checkAbsoluteUriString(option(image.getUrl()), "image.url");
        }
        if (errors.isEmpty())
        {
            return right(new UserProfile.Builder(id == null ? "" : id)
                .fullName(displayName == null ? "" : displayName)
                .profilePageUri(profileUri)
                .profilePictureUri(pictureUri)
                .build());
        }
        return left(errors.build());
    }
    
    public String getContent()
    {
        return content;
    }

    public String getDisplayName()
    {
        return displayName;
    }

    public String getId()
    {
        return id;
    }

    public MediaLinkRepresentation getImage()
    {
        return image;
    }

    public String getObjectType()
    {
        return objectType;
    }

    public String getSummary()
    {
        return summary;
    }

    public String getUrl()
    {
        return url;
    }
    
    public static class Builder
    {
        private Option<String> content = none();
        private Option<String> displayName = none();
        private Option<String> id = none();
        private Option<MediaLinkRepresentation> image = none();
        private Option<String> objectType = none();
        private Option<Html> summary = none();
        private Option<String> url = none();
        
        public Builder content(Option<String> content)
        {
            this.content = checkNotNull(content, "content");
            return this;
        }
        
        public Builder displayName(Option<String> displayName)
        {
            this.displayName = checkNotNull(displayName, "displayName");
            return this;
        }

        public Builder id(Option<URI> id)
        {
            this.id = checkNotNull(id, "id").map(toStringFunction());
            return this;
        }
        
        public Builder idString(Option<String> id)
        {
            this.id = checkNotNull(id, "id");
            return this;
        }

        public Builder image(Option<MediaLinkRepresentation> image)
        {
            this.image = checkNotNull(image, "image");
            return this;
        }
        
        public Builder objectType(Option<URI> objectType)
        {
            this.objectType = checkNotNull(objectType, "objectType").map(toStringFunction());
            return this;
        }
        
        public Builder objectTypeString(Option<String> objectType)
        {
            this.objectType = checkNotNull(objectType, "objectType");
            return this;
        }
        
        public Builder summary(Option<Html> summary)
        {
            this.summary = checkNotNull(summary, "summary");
            return this;
        }
        
        public Builder url(Option<URI> url)
        {
            this.url = checkNotNull(url, "url").map(toStringFunction());
            return this;
        }

        public Builder urlString(Option<String> url)
        {
            this.url = checkNotNull(url, "url");
            return this;
        }
        
        public ActivityObjectRepresentation build()
        {
            return new ActivityObjectRepresentation(this);
        }
    }
}
