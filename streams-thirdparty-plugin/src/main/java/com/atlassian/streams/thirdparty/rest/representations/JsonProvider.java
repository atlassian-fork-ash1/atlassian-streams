package com.atlassian.streams.thirdparty.rest.representations;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyReader;
import javax.ws.rs.ext.MessageBodyWriter;
import javax.ws.rs.ext.Provider;

import org.codehaus.jackson.jaxrs.JacksonJsonProvider;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.SerializationConfig;

import static com.atlassian.streams.thirdparty.rest.MediaTypes.STREAMS_THIRDPARTY_JSON;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

/**
 * {@code JsonProvider} is an implementation of the {@code MessageBodyReader} and {@code MessageBodyWriter} interfaces.
 * It provides serialization and deserialization of objects annotated with Jackson annotations.  The implementation
 * simply wraps the {@link JacksonJsonProvider} which would not be loaded by the REST module otherwise.
 */
@Provider
@Produces({APPLICATION_JSON, STREAMS_THIRDPARTY_JSON})
@Consumes({APPLICATION_JSON, STREAMS_THIRDPARTY_JSON})
public class JsonProvider implements MessageBodyReader<Object>, MessageBodyWriter<Object>
{
    private final JacksonJsonProvider provider = new JacksonJsonProvider();

    @SuppressWarnings("deprecation")
    public JsonProvider()
    {
        provider.setMapper(new ErrorResponseStatusObjectMapper());
        provider.configure(SerializationConfig.Feature.AUTO_DETECT_GETTERS, false);
        provider.configure(SerializationConfig.Feature.AUTO_DETECT_FIELDS, false);
        // the WRITE_NULL_PROPERTIES feature is deprecated, but there is no way to get to the configuration without
        // extending the JacksonJsonProvider
        provider.configure(SerializationConfig.Feature.WRITE_NULL_PROPERTIES, false);
        provider.configure(SerializationConfig.Feature.WRITE_DATES_AS_TIMESTAMPS, false);

        provider.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    // ----------------- delegates for MessageBodyWriter

    public long getSize(Object value, Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType)
    {
        return provider.getSize(value, type, genericType, annotations, mediaType);
    }

    public boolean isWriteable(Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType)
    {
        return provider.isWriteable(type, genericType, annotations, mediaType);
    }

    public void writeTo(Object value, Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType,
            MultivaluedMap<String, Object> httpHeaders, OutputStream entityStream) throws IOException
    {
        provider.writeTo(value, type, genericType, annotations, mediaType, httpHeaders, entityStream);
    }

    // ----------------- delegates for MessageBodyReader

    public boolean isReadable(Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType)
    {
        return provider.isReadable(type, genericType, annotations, mediaType);
    }

    public Object readFrom(Class<Object> type, Type genericType, Annotation[] annotations, MediaType mediaType,
            MultivaluedMap<String, String> httpHeaders, InputStream entityStream) throws IOException
    {
        return provider.readFrom(type, genericType, annotations, mediaType, httpHeaders, entityStream);
    }
}
