package com.atlassian.streams.thirdparty.rest.representations;

import java.net.URI;

import com.atlassian.streams.api.common.Either;
import com.atlassian.streams.api.common.Option;
import com.atlassian.streams.thirdparty.api.Image;
import com.atlassian.streams.thirdparty.api.ValidationErrors;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;

import static com.atlassian.streams.api.common.Option.none;
import static com.atlassian.streams.api.common.Option.option;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * JSON representation class used by the Activity REST API.
 */
public class MediaLinkRepresentation
{
    @JsonProperty Integer duration;
    @JsonProperty Integer height;
    @JsonProperty String url;
    @JsonProperty Integer width;

    /**
     * Constructor used for Jackson deserialization.  Use {@link #builder()} for all other purposes.
     */
    @JsonCreator
    public MediaLinkRepresentation(@JsonProperty("duration") Integer duration,
                                   @JsonProperty("height") Integer height,
                                   @JsonProperty("url") String url,
                                   @JsonProperty("width") Integer width)
    {
        this.duration = duration;
        this.height = height;
        this.url = url;
        this.width = width;
    }

    /**
     * Returns a {@link Builder} for creating a new instance of this class.
     */
    public static Builder builder(URI url)
    {
        return new Builder(url);
    }
    
    private MediaLinkRepresentation(Builder builder)
    {
        this.duration = builder.duration.getOrElse((Integer) null);
        this.height = builder.height.getOrElse((Integer) null);
        this.url = builder.url;
        this.width = builder.width.getOrElse((Integer) null);
    }
    
    public Either<ValidationErrors, Image> toImage()
    {
        if (getUrl() == null)
        {
            return Either.<ValidationErrors, Image>left(ValidationErrors.validationError("url is required"));
        }
        return new Image.Builder(getUrl()).height(option(getHeight())).width(option(getWidth())).build();
    }

    public Integer getDuration()
    {
        return duration;
    }

    public Integer getHeight()
    {
        return height;
    }

    public String getUrl()
    {
        return url;
    }

    public Integer getWidth()
    {
        return width;
    }
    
    public static class Builder
    {
        private Option<Integer> duration = none();
        private Option<Integer> height = none();
        private String url;
        private Option<Integer> width = none();
        
        public Builder(URI url)
        {
            this.url = url.toString();
        }
        
        public Builder(String urlString)
        {
            this.url = urlString;
        }
        
        public MediaLinkRepresentation build()
        {
            return new MediaLinkRepresentation(this);
        }
        
        public Builder duration(Option<Integer> duration)
        {
            this.duration = checkNotNull(duration, "duration");
            return this;
        }
        
        public Builder height(Option<Integer> height)
        {
            this.height = checkNotNull(height, "height");
            return this;
        }
        
        public Builder width(Option<Integer> width)
        {
            this.width = checkNotNull(width, "width");
            return this;
        }
    }
}
