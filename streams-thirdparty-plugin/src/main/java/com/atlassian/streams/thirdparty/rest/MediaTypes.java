package com.atlassian.streams.thirdparty.rest;

/**
 * Media types used in the third party Activity Streams provider plugin.
 *
 * @since v5.0
 */
public class MediaTypes
{
    public static final String STREAMS_THIRDPARTY_JSON = "application/vnd.atl.streams.thirdparty+json";
}
