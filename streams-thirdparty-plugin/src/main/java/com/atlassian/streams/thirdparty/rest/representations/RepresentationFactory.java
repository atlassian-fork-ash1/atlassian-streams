package com.atlassian.streams.thirdparty.rest.representations;

import com.atlassian.streams.api.UserProfile;
import com.atlassian.streams.thirdparty.api.Activity;
import com.atlassian.streams.thirdparty.api.ActivityObject;
import com.atlassian.streams.thirdparty.api.ActivityQuery;
import com.atlassian.streams.thirdparty.api.Application;
import com.atlassian.streams.thirdparty.api.Image;

import com.google.common.base.Function;

/**
 * Factory for creating REST API representation objects out of Java API objects.
 */
public interface RepresentationFactory
{
    ActivityCollectionRepresentation createActivityCollectionRepresentation(Iterable<Activity> activities, ActivityQuery query);

    ActivityRepresentation createActivityRepresentation(Activity activity);

    Function<Activity, ActivityRepresentation> toActivityRepresentation();
    
    ActivityObjectRepresentation createActivityObjectRepresentation(ActivityObject object);

    ActivityObjectRepresentation createActivityObjectRepresentation(Application application);

    ActivityObjectRepresentation createActivityObjectRepresentation(UserProfile userProfile);

    MediaLinkRepresentation createMediaLinkRepresentation(Image image);
    
    Function<Image, MediaLinkRepresentation> toMediaLinkRepresentation();
}
