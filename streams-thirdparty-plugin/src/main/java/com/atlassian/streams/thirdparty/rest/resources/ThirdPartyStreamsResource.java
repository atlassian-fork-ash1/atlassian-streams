package com.atlassian.streams.thirdparty.rest.resources;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;

import com.atlassian.sal.api.user.UserManager;
import com.atlassian.streams.thirdparty.api.Activity;
import com.atlassian.streams.thirdparty.api.ActivityService;
import com.atlassian.streams.thirdparty.rest.representations.RepresentationFactory;

import static com.atlassian.streams.thirdparty.rest.MediaTypes.STREAMS_THIRDPARTY_JSON;
import static com.google.common.base.Preconditions.checkNotNull;
import static javax.ws.rs.core.Response.Status.FORBIDDEN;
import static javax.ws.rs.core.Response.Status.INTERNAL_SERVER_ERROR;
import static javax.ws.rs.core.Response.Status.NOT_FOUND;

@Path("/{activityId}")
public class ThirdPartyStreamsResource
{
    private final ActivityService activityService;
    private final RepresentationFactory factory;
    private final UserManager userManager;

    public ThirdPartyStreamsResource(ActivityService activityService, RepresentationFactory factory, UserManager userManager)
    {
        this.activityService = checkNotNull(activityService, "activityService");
        this.factory = checkNotNull(factory, "factory");
        this.userManager = checkNotNull(userManager, "userManager");
    }

    @GET
    public Response get(@PathParam("activityId") long activityId)
    {
        for (Activity activity : activityService.getActivity(activityId))
        {
            return Response.ok(factory.createActivityRepresentation(activity)).type(STREAMS_THIRDPARTY_JSON).build();
        }
        return Response.status(NOT_FOUND).build();
    }

    @DELETE
    public Response delete(@PathParam("activityId") long activityId)
    {
        String user = userManager.getRemoteUsername();
        if (!userManager.isAdmin(user) || !userManager.isSystemAdmin(user))
        {
            return Response.status(FORBIDDEN).build();
        }
        for (Activity activity : activityService.getActivity(activityId))
        {
            if (activityService.delete(activityId))
            {
                return Response.ok(factory.createActivityRepresentation(activity)).type(STREAMS_THIRDPARTY_JSON).build();
            }
            else
            {
                return Response.status(INTERNAL_SERVER_ERROR).build();
            }
        }
        return Response.status(NOT_FOUND).build();
    }
}
