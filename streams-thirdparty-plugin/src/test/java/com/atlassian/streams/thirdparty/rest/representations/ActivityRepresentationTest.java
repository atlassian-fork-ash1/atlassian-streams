package com.atlassian.streams.thirdparty.rest.representations;

import java.net.URI;
import java.util.Date;

import com.atlassian.sal.api.user.UserKey;
import com.atlassian.sal.api.user.UserProfile;
import com.atlassian.streams.api.Html;
import com.atlassian.streams.api.common.Either;
import com.atlassian.streams.thirdparty.api.Activity;
import com.atlassian.streams.thirdparty.api.ValidationErrors;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;

import static com.atlassian.streams.api.Html.html;
import static com.atlassian.streams.api.common.Option.some;
import static com.atlassian.streams.thirdparty.TestData.DEFAULT_ID;
import static com.atlassian.streams.thirdparty.TestData.INVALID_URI_STRING;
import static com.atlassian.streams.thirdparty.TestData.assertNotValidationError;
import static com.atlassian.streams.thirdparty.TestData.assertValidationError;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ActivityRepresentationTest
{
    private static final String APP_NAME = "appname";
    private static final String APP_ID = "http://app";
    private ActivityObjectRepresentation actor;
    private ActivityObjectRepresentation generator;
    private Date published;
    private ActivityRepresentation rep;
    private UserProfile userProfile;
    
    @Before
    public void setUp() throws Exception
    {
        actor = ActivityObjectRepresentation.builder().idString(some("user")).build();
        generator = ActivityObjectRepresentation.builder().idString(some(APP_ID)).displayName(some(APP_NAME)).build();
        published = new Date();
        userProfile = mock(UserProfile.class);
        when(userProfile.getEmail()).thenReturn("mrrobot@evilcorp.com");
        when(userProfile.getFullName()).thenReturn("Eliot Alderson");
        when(userProfile.getProfilePageUri()).thenReturn(URI.create("http://mysuperawsomeprofilepageprovider.com/view/mrrobot"));
        when(userProfile.getProfilePictureUri()).thenReturn(URI.create("http://mysuperawesomeprofilepictureprovider.com/view/fsociety.gif"));
        when(userProfile.getUserKey()).thenReturn(new UserKey("eliotAlderson"));
    }
    
    @Test
    public void toActivityCopiesAppDisplayName()
    {
        rep = ActivityRepresentation.builder(actor, generator).id(some(DEFAULT_ID)).build();
        
        assertThat(assertNotValidationError(rep.toActivity(userProfile)).getApplication().getDisplayName(), equalTo(APP_NAME));
    }

    @Test
    public void toActivityCopiesAppId()
    {
        rep = ActivityRepresentation.builder(actor, generator).id(some(DEFAULT_ID)).build();
        
        assertThat(assertNotValidationError(rep.toActivity(userProfile)).getApplication().getId(), equalTo(URI.create(APP_ID)));
    }

    @Test
    public void toActivityRejectsNullAppId()
    {
        rep = ActivityRepresentation.builder(actor,
                                             ActivityObjectRepresentation.builder().displayName(some(APP_NAME)).build())
                                             .id(some(DEFAULT_ID)).build();
        
        assertValidationError(rep.toActivity(userProfile));
    }

    @Test
    public void toActivityRejectsBlankAppDisplayName()
    {
        rep = ActivityRepresentation.builder(actor,
                                             ActivityObjectRepresentation.builder().displayName(some("   \t\n "))
                                                 .idString(some(APP_ID)).build())
                                             .id(some(DEFAULT_ID)).build();
        
        assertValidationError(rep.toActivity(userProfile));
    }

    @Test
    public void toActivityRejectsNullAppDisplayName()
    {
        rep = ActivityRepresentation.builder(actor,
                                             ActivityObjectRepresentation.builder().displayName(some(APP_NAME)).build())
                                             .id(some(DEFAULT_ID)).build();
        
        assertValidationError(rep.toActivity(userProfile));
    }

    @Test
    public void toActivityCopiesContent()
    {
        Html content = html("Khan tent");
        rep = ActivityRepresentation.builder(actor, generator).content(some(content)).id(some(DEFAULT_ID)).build();
        
        assertThat(assertNotValidationError(rep.toActivity(userProfile)).getContent(), equalTo(some(content)));
    }

    @Test
    public void toActivityCopiesId()
    {
        rep = ActivityRepresentation.builder(actor, generator).id(some(DEFAULT_ID)).build();
        
        assertThat(assertNotValidationError(rep.toActivity(userProfile)).getId(), equalTo(some(DEFAULT_ID)));
    }

    @Test
    public void toActivityRejectsInvalidId()
    {
        rep = ActivityRepresentation.builder(actor, generator).idString(some(INVALID_URI_STRING)).build();
        
        assertValidationError(rep.toActivity(userProfile));
    }

    @Test
    public void toActivityCopiesPostedDate()
    {
        rep = ActivityRepresentation.builder(actor, generator).published(some(published)).id(some(DEFAULT_ID)).build();
        
        assertThat(assertNotValidationError(rep.toActivity(userProfile)).getPostedDate(), equalTo(new DateTime(published)));
    }

    @SuppressWarnings("unchecked")
    @Test
    public void toActivityDefaultsPostedDateToNowIfOmitted()
    {
        DateTime now = new DateTime();
        rep = ActivityRepresentation.builder(actor, generator).id(some(DEFAULT_ID)).build();
        
        assertThat(assertNotValidationError(rep.toActivity(userProfile)).getPostedDate(), greaterThanOrEqualTo(now));
    }

    @Test
    public void toActivityCopiesTitle()
    {
        Html title = html("Thai tle");
        rep = ActivityRepresentation.builder(actor, generator).title(some(title)).id(some(DEFAULT_ID)).build();
        
        assertThat(assertNotValidationError(rep.toActivity(userProfile)).getTitle(), equalTo(some(title)));
    }
    
    @Test
    public void toActivityCopiesUrl()
    {
        rep = ActivityRepresentation.builder(actor, generator).url(some(DEFAULT_ID)).build();
        
        assertThat(assertNotValidationError(rep.toActivity(userProfile)).getUrl(), equalTo(some(DEFAULT_ID)));
    }

    @Test
    public void toActivityRejectsInvalidUrl()
    {
        rep = ActivityRepresentation.builder(actor, generator).urlString(some(INVALID_URI_STRING)).build();
        
        assertValidationError(rep.toActivity(userProfile));
    }
    
    @Test
    public void toActivityCopiesVerb()
    {
        String verb = "noun";
        rep = ActivityRepresentation.builder(actor, generator).verbString(some(verb)).id(some(DEFAULT_ID)).build();
        
        assertThat(assertNotValidationError(rep.toActivity(userProfile)).getVerb(), equalTo(some(URI.create(verb))));
    }

    @Test
    public void toActivityRejectsInvalidVerb()
    {
        rep = ActivityRepresentation.builder(actor, generator).verbString(some(INVALID_URI_STRING)).id(some(DEFAULT_ID)).build();
        
        assertValidationError(rep.toActivity(userProfile));
    }

    @Test
    public void toActivityReturnsCallerAsAnActor() throws Exception
    {
        // having
        rep = ActivityRepresentation.builder(actor, generator).url(some(DEFAULT_ID)).build();

        // when
        final Either<ValidationErrors, Activity> errOrActivity = rep.toActivity(userProfile);

        // then
        assertThat(errOrActivity.right().get().getUser().getFullName(), is(userProfile.getFullName()));
    }
}
