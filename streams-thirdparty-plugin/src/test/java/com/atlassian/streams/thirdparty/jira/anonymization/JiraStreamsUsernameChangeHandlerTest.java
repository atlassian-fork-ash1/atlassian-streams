package com.atlassian.streams.thirdparty.jira.anonymization;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.jira.bc.ServiceResult;
import com.atlassian.jira.task.context.Context;
import com.atlassian.jira.user.anonymize.UserPropertyChangeParameter;
import com.atlassian.streams.spi.StreamsI18nResolver;
import com.atlassian.streams.thirdparty.ao.ActivityEntity;
import net.java.ao.EntityStreamCallback;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Optional;

import static com.atlassian.jira.user.anonymize.AffectedEntityType.ANONYMIZE;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.AdditionalAnswers.answer;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.hamcrest.MockitoHamcrest.argThat;

@RunWith(MockitoJUnitRunner.StrictStubs.class)
public class JiraStreamsUsernameChangeHandlerTest {

    @Mock
    private ActiveObjects ao;
    @Mock
    private StreamsI18nResolver i18nResolver;
    @Mock
    private Context context;
    @Mock
    private Context.Task task;
    @Mock
    private ActivityEntity entity1;
    @Mock
    private ActivityEntity entity2;
    @Mock
    private ActivityEntity entity3;

    private UserPropertyChangeParameter userPropertyChangeParameter;

    @InjectMocks
    private JiraStreamsUsernameChangeHandler handler;

    @Before
    public void setUp() {
        setAoGet(entity1, 10L);
        setAoGet(entity2, 20L);
        setAoGet(entity3, 30L);

        when(context.start(null)).thenReturn(task);
        when(i18nResolver.getText(anyString(), anyString())).thenAnswer(answer(
                (String key, String param) -> key + "(" + param + ")"
        ));
        userPropertyChangeParameter = new UserPropertyChangeParameter("fromUsername", "toUsername", context);
    }

    @Test
    public void whenThereAreNoEntitiesContainingUsernameEmptyAffectedEntitiesShouldBeReturned() {
        setAoCount(0, 0);

        assertThat(handler.getAffectedEntities(userPropertyChangeParameter), empty());
    }

    @Test
    public void whenThereAreEntitiesContainingUsernameOneElementAffectedEntitiesShouldBeReturned() {
        setAoCount(1, 2);

        assertThat(
                handler.getAffectedEntities(userPropertyChangeParameter),
                contains(
                        allOf(
                                hasProperty("type", equalTo(ANONYMIZE)),
                                hasProperty("numberOfOccurrences", equalTo(Optional.of(3L))),
                                hasProperty("descriptionKey", equalTo("streams.jira.username_change_handler.entry")),
                                hasProperty("link", equalTo(Optional.empty()))
                        )
                )
        );
    }

    @Test
    public void shouldUpdateEntities() {
        setAoStream(new ActivityEntity[]{entity1}, new ActivityEntity[]{entity2, entity3});

        final ServiceResult result = handler.update(userPropertyChangeParameter);

        assertThat(result.isValid(), is(true));
        InOrder inOrder = inOrder(entity1, entity2, entity3);
        inOrder.verify(entity1).setPoster("toUsername");
        inOrder.verify(entity1).save();
        inOrder.verify(entity2).setUsername("toUsername");
        inOrder.verify(entity2).save();
        inOrder.verify(entity3).setUsername("toUsername");
        inOrder.verify(entity3).save();
        inOrder.verifyNoMoreInteractions();
    }

    @Test
    public void shouldUpdateEntitiesEvenWhenExceptionOccuredDuringUpdate() {
        setAoStream(new ActivityEntity[]{entity1}, new ActivityEntity[]{entity2, entity3});
        doThrow(new RuntimeException("some exception")).when(entity2).save();

        final ServiceResult result = handler.update(userPropertyChangeParameter);

        assertThat(result.isValid(), is(false));
        assertThat(result.getErrorCollection().getErrorMessages(), contains("streams.jira.change.handler.processing.exception(some exception)"));
        InOrder inOrder = inOrder(entity1, entity2, entity3);
        inOrder.verify(entity1).setPoster("toUsername");
        inOrder.verify(entity1).save();
        inOrder.verify(entity2).setUsername("toUsername");
        inOrder.verify(entity2).save();
        inOrder.verify(entity3).setUsername("toUsername");
        inOrder.verify(entity3).save();
        inOrder.verifyNoMoreInteractions();
    }

    @Test
    public void shouldUpdateEntitiesEvenWhenExceptionOccuredDuringGet() {
        setAoStream(new ActivityEntity[]{entity1}, new ActivityEntity[]{entity2, entity3});
        doThrow(new RuntimeException("some exception")).when(ao).get(eq(ActivityEntity.class), eq(20L));

        final ServiceResult result = handler.update(userPropertyChangeParameter);

        assertThat(result.isValid(), is(false));
        assertThat(result.getErrorCollection().getErrorMessages(), contains("streams.jira.change.handler.processing.exception(some exception)"));
        InOrder inOrder = inOrder(entity1, entity2, entity3);
        inOrder.verify(entity1).setPoster("toUsername");
        inOrder.verify(entity1).save();
        inOrder.verify(entity3).setUsername("toUsername");
        inOrder.verify(entity3).save();
        inOrder.verifyNoMoreInteractions();
    }

    @Test
    public void shouldUpdateEntitiesEvenWhenSomeGetReturnsNull() {
        setAoStream(new ActivityEntity[]{entity1}, new ActivityEntity[]{entity2, entity3});
        doReturn(null).when(ao).get(eq(ActivityEntity.class), eq(20L));

        final ServiceResult result = handler.update(userPropertyChangeParameter);

        assertThat(result.isValid(), is(true));
        InOrder inOrder = inOrder(entity1, entity2, entity3);
        inOrder.verify(entity1).setPoster("toUsername");
        inOrder.verify(entity1).save();
        inOrder.verify(entity3).setUsername("toUsername");
        inOrder.verify(entity3).save();
        inOrder.verifyNoMoreInteractions();
    }

    @Test
    public void shouldUpdateContextProgress() {
        setAoCount(1, 2);
        setAoStream(new ActivityEntity[]{entity1}, new ActivityEntity[]{entity2, entity3});
        final int numberOfTasks = handler.getNumberOfTasks(userPropertyChangeParameter);

        handler.update(userPropertyChangeParameter);

        verify(task, times(numberOfTasks)).complete();
    }

    private void setAoCount(int posterCount, int usernameCount) {
        doReturn(posterCount).when(ao).count(any(),
                argThat(hasProperty("whereClause", equalTo("POSTER = ?")))
        );
        doReturn(usernameCount).when(ao).count(any(),
                argThat(hasProperty("whereClause", equalTo("USERNAME = ?")))
        );
    }

    private void setAoStream(ActivityEntity[] withPosters, ActivityEntity[] withUsernames) {
        doAnswer(invocation -> {
            for (final ActivityEntity withPoster : withPosters) {
                ((EntityStreamCallback<ActivityEntity, Long>) invocation.getArguments()[2]).onRowRead(withPoster);
            }
            return null;
        }).when(ao).stream(eq(ActivityEntity.class),
                argThat(hasProperty("whereClause", equalTo("POSTER = ?"))),
                any());

        doAnswer(invocation -> {
            for (final ActivityEntity withUsername : withUsernames) {
                ((EntityStreamCallback<ActivityEntity, Long>) invocation.getArguments()[2]).onRowRead(withUsername);
            }
            return null;
        }).when(ao).stream(eq(ActivityEntity.class),
                argThat(hasProperty("whereClause", equalTo("USERNAME = ?"))),
                any());
    }

    private void setAoGet(ActivityEntity entity, long id) {
        when(entity.getActivityId()).thenReturn(id);
        doReturn(entity).when(ao).get(eq(ActivityEntity.class), eq(id));
    }
}
