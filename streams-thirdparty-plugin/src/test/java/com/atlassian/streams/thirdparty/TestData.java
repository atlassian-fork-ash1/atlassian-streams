package com.atlassian.streams.thirdparty;

import java.net.URI;

import com.atlassian.streams.api.common.Either;
import com.atlassian.streams.thirdparty.api.ValidationErrors;

import org.junit.Assert;

public abstract class TestData
{
    public static final URI DEFAULT_ID = URI.create("http://id");
    public static final String USERNAME = "username";
    public static final String USER_FULL_NAME = "Luzer";
    public static final URI PROFILE_URI = URI.create("http://my/page");
    public static final URI PICTURE_URI = URI.create("http://my/face");
    public static final URI RELATIVE_URI = URI.create("not/absolute");
    public static final String INVALID_URI_STRING = "this::isn't a uri";

    public static String makeString(int length)
    {
        StringBuilder buf = new StringBuilder(length);
        for (int i = 0; i < length; i++)
        {
            buf.append("x");
        }
        return buf.toString();
    }
    
    public static URI makeUri(int length)
    {
        return URI.create(makeString(length));
    }
    
    public static URI makeAbsoluteUri(int length)
    {
        return URI.create("http://" + makeString(length - 7));
    }
    
    public static void assertValidationError(Either<ValidationErrors, ?> result)
    {
        if (result.isRight())
        {
            Assert.fail("expected ValidationErrors, got " + result.right().get());
        }
    }
    
    public static <T> T assertNotValidationError(Either<ValidationErrors, T> result)
    {
        if (result.isLeft())
        {
            Assert.fail("unexpected ValidationError: " + result.left().get());
        }
        return result.right().get();
    }
}
