package com.atlassian.streams.testing;

import java.net.URI;

import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.streams.api.UserProfile;
import com.atlassian.streams.spi.UserProfileAccessor;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.hamcrest.TypeSafeDiagnosingMatcher;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import static com.atlassian.streams.api.common.Option.option;
import static com.atlassian.streams.api.common.Option.some;
import static com.atlassian.streams.api.common.uri.Uris.encode;
import static com.google.common.base.Objects.equal;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;

public abstract class AbstractUserProfileAccessorTestSuite
{
    private @Mock ApplicationProperties applicationProperties;

    protected UserProfileAccessor userProfileAccessor;

    abstract protected String getProfilePathTemplate();

    abstract protected String getProfilePicturePathTemplate();

    protected ApplicationProperties getApplicationProperties()
    {
        return applicationProperties;
    }

    @Before
    public void prepareApplicationProperties()
    {
        when(applicationProperties.getBaseUrl()).thenReturn("http://localhost/streams");
    }

    @Test
    public void assertThatUserProfileWithUsernameAsFullnameIsReturnedWhenUserDoesNotExist()
    {
        URI baseUri = URI.create("http://localhost/streams");
        assertThat(userProfileAccessor.getUserProfile(baseUri, "idonotexist"),
                is(sameUsername(new UserProfile.Builder("idonotexist").build())));
    }

    @Test
    public void assertThatWhenUserExistsUserProfileIsReturned()
    {
        URI baseUri = URI.create("http://localhost/streams");
        assertThat(userProfileAccessor.getUserProfile(baseUri, "user"),
                is(equalTo(
                        new UserProfile.Builder("user")
                                .fullName("User")
                                .email(some("u@c.com"))
                                .profilePageUri(option(getProfileUri("user")))
                                .profilePictureUri(option(getProfilePictureUri(getProfilePicParameter("user"))))
                                .build())));
    }

    @Test
    public void assertThatUsernameIsUriEncodedInProfileUri()
    {
        URI baseUri = URI.create("http://localhost/streams");
        assertThat(userProfileAccessor.getUserProfile(baseUri, "user 2").getProfilePageUri(),
                is(equalTo(some(getProfileUri("user 2")))));
    }

    @Test
    public void assertThatUsernameIsUriEncodedInProfilePictureUri()
    {
        URI baseUri = URI.create("http://localhost/streams");
        assertThat(userProfileAccessor.getUserProfile(baseUri, "user 2").getProfilePictureUri(),
                is(equalTo(option(getProfilePictureUri(getProfilePicParameter("user 2"))))));
    }

    @Test
    public void assertThatFullNameIsNotEncoded()
    {
        URI baseUri = URI.create("http://localhost/streams");
        assertThat(userProfileAccessor.getUserProfile(baseUri, "user3").getFullName(),
                is(equalTo("User <3&'>")));
    }

    private URI getProfileUri(String username)
    {
        return URI.create(applicationProperties.getBaseUrl() + getProfilePathTemplate().replace("{username}", encode(username)));
    }

    private URI getProfilePictureUri(String parameter)
    {
        if (getProfilePicturePathTemplate() != null)
        {
            return URI.create(getProfilePicApplicationBaseUrl() + getProfilePicturePathTemplate().replace("{profilePictureParameter}", encode(parameter)));
        }
        else
        {
            return null;
        }
    }

    /**
     * Get the parameter used for profile picture URIs. By default is just the username.
     * @param username the username
     * @return the username
     */
    protected String getProfilePicParameter(String username)
    {
        return username;
    }

    /**
     * Returns the application base url to be used for profile pictures.
     * @return the application base url to be used for profile pictures.
     */
    protected String getProfilePicApplicationBaseUrl()
    {
        return applicationProperties.getBaseUrl();
    }

    private static Matcher<? super UserProfile> sameUsername(final UserProfile expected)
    {
        return new TypeSafeDiagnosingMatcher<UserProfile>()
        {

            @Override
            protected boolean matchesSafely(UserProfile actual, Description mismatchDescription)
            {
                if (actual == null)
                {
                    mismatchDescription.appendValue(null);
                    return false;
                }
                if (!equal(actual.getUsername(), expected.getUsername()))
                {
                    mismatchDescription.appendText("userName=").appendValue(actual.getUsername());
                    return false;
                }
                return true;
            }

            public void describeTo(Description description)
            {
                description.appendText("username=").appendValue(expected.getUsername());
            }
        };
    }

    private static Matcher<? super UserProfile> equalTo(final UserProfile expected)
    {
        return new TypeSafeDiagnosingMatcher<UserProfile>()
        {
            @Override
            protected boolean matchesSafely(UserProfile actual, Description mismatchDescription)
            {
                boolean matches = true;
                if (expected == actual)
                {
                    return matches;
                }
                if (actual == null)
                {
                    mismatchDescription.appendValue(null);
                    return false;
                }
                if (!equal(actual.getUsername(), expected.getUsername()))
                {
                    mismatchDescription.appendText("username=").appendValue(actual.getUsername());
                    matches = false;
                }
                if (!equal(actual.getFullName(), expected.getFullName()))
                {
                    if (!matches)
                    {
                        mismatchDescription.appendText(", ");
                    }
                    mismatchDescription.appendText("fullName=").appendValue(actual.getFullName());
                    matches = false;
                }
                if (!equal(actual.getEmail(), expected.getEmail()))
                {
                    if (!matches)
                    {
                        mismatchDescription.appendText(", ");
                    }
                    mismatchDescription.appendText("email=").appendValue(actual.getEmail());
                    matches = false;
                }
                if (!equal(actual.getProfilePageUri(), expected.getProfilePageUri()))
                {
                    if (!matches)
                    {
                        mismatchDescription.appendText(", ");
                    }
                    mismatchDescription.appendText("profilePageUri=").appendValue(actual.getProfilePageUri());
                    matches = false;
                }
                if (!equal(actual.getProfilePictureUri(), expected.getProfilePictureUri()))
                {
                    if (!matches)
                    {
                        mismatchDescription.appendText(", ");
                    }
                    mismatchDescription.appendText("profilePictureUri=").appendValue(actual.getProfilePictureUri());
                    matches = false;
                }
                return matches;
            }

            public void describeTo(Description description)
            {
                description.appendText("username=").appendValue(expected.getUsername())
                        .appendText(", fullName=").appendValue(expected.getFullName())
                        .appendText(", email=").appendValue(expected.getEmail())
                        .appendText(", profilePageUri=").appendValue(expected.getProfilePageUri())
                        .appendText(", profilePictureUri=").appendValue(expected.getProfilePictureUri());
            }
        };
    }

    private static <T> Matcher<? super T> equalTo(T expected)
    {
        return Matchers.equalTo(expected);
    }
}
