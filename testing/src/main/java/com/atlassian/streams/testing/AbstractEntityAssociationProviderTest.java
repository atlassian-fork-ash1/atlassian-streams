package com.atlassian.streams.testing;

import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.UrlMode;
import com.atlassian.streams.spi.EntityIdentifier;
import com.atlassian.streams.spi.StandardStreamsFilterOption;
import com.atlassian.streams.spi.StreamsEntityAssociationProvider;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import java.net.URI;

import static com.atlassian.streams.api.common.Option.none;
import static com.atlassian.streams.api.common.Option.some;
import static com.atlassian.streams.spi.EntityIdentifierMatchers.hasNoEntityIdentifiers;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Mockito.when;

public abstract class AbstractEntityAssociationProviderTest
{
    static final String BASE_URL = "http://localhost:3990/streams";
    private static final String UNKNOWN_BASE_URL = "http://foreignhost";
    protected static final String PROJECT_ENTITY_KEY = "PROJECT";
    private static final URI UNKNOWN_ENTITY_TYPE = URI.create("http://whatever");
    
    protected StreamsEntityAssociationProvider provider;

    @Mock
    protected ApplicationProperties applicationProperties;

    @Before
    public void setupBase()
    {
        provider = createProvider();
        when(applicationProperties.getBaseUrl()).thenReturn(BASE_URL);
        when(applicationProperties.getBaseUrl(UrlMode.CANONICAL)).thenReturn(BASE_URL);
    }
    
    protected abstract StreamsEntityAssociationProvider createProvider();
    
    protected abstract String getProjectUriPath(String key);
    
    protected abstract URI getProjectEntityType();
    
    protected abstract void setProjectExists(String key, boolean exists);

    protected abstract void setProjectViewPermission(String key, boolean permitted);
    
    protected abstract void setProjectEditPermission(String key, boolean permitted);
    
    protected URI getProjectUri(String key)
    {
        return URI.create(BASE_URL + getProjectUriPath(key));
    }
    
    protected EntityIdentifier getProjectEntityIdentifier(String key)
    {
        return new EntityIdentifier(getProjectEntityType(),
                                    key,
                                    getProjectUri(key));
    }
    
    @Test
    public void assertThatProjectUriReturnsProjectEntityIdentifier()
    {
        setProjectExists(PROJECT_ENTITY_KEY, true);
        assertHasOnlyProjectKey(provider.getEntityIdentifiers(getProjectUri(PROJECT_ENTITY_KEY)), PROJECT_ENTITY_KEY);
    }

    @Test
    public void assertThatProjectUriWithRequestParametersProjectEntityIdentifier()
    {
        setProjectExists(PROJECT_ENTITY_KEY, true);
        assertHasOnlyProjectKey(provider.getEntityIdentifiers(URI.create(getProjectUri(PROJECT_ENTITY_KEY).toString() + "?req_param=req_value")),
                                PROJECT_ENTITY_KEY);
    }

    @Test
    public void assertThatProjectUriWithHashReturnsProjectEntityIdentifier()
    {
        setProjectExists(PROJECT_ENTITY_KEY, true);
        assertHasOnlyProjectKey(provider.getEntityIdentifiers(URI.create(getProjectUri(PROJECT_ENTITY_KEY).toString() + "#myhash")),
                                PROJECT_ENTITY_KEY);
    }

    @Test
    public void assertThatProjectKeyWithoutAbsoluteUriReturnsProjectEntityIdentifier()
    {
        setProjectExists(PROJECT_ENTITY_KEY, true);
        assertHasOnlyProjectKey(provider.getEntityIdentifiers(URI.create(PROJECT_ENTITY_KEY)),
                                PROJECT_ENTITY_KEY);
    }

    @Test
    public void assertThatProjectUriForUnknownKeyReturnsNoIdentifier()
    {
        setProjectExists(PROJECT_ENTITY_KEY, false);
        assertThat(provider.getEntityIdentifiers(getProjectUri(PROJECT_ENTITY_KEY)), hasNoEntityIdentifiers());
    }
    
    @Test
    public void assertThatNonLocalUriReturnsNoEntityIdentifiers()
    {
        assertThat(provider.getEntityIdentifiers(URI.create(UNKNOWN_BASE_URL)), hasNoEntityIdentifiers());
    }

    @Test
    public void assertThatLocalUriForNonEntityReturnsNoEntityIdentifiers()
    {
        assertThat(provider.getEntityIdentifiers(URI.create(BASE_URL + "/some/other/url")), hasNoEntityIdentifiers());
    }
    
    @Test
    public void assertThatProjectIdentifierReturnsProjectUri()
    {
        setProjectExists(PROJECT_ENTITY_KEY, true);
        assertThat(provider.getEntityURI(getProjectEntityIdentifier(PROJECT_ENTITY_KEY)),
                   equalTo(some(getProjectUri(PROJECT_ENTITY_KEY))));
    }
    
    @Test
    public void assertThatProjectIdentifierReturnsProjectFilterKey()
    {
        assertThat(provider.getFilterKey(getProjectEntityIdentifier(PROJECT_ENTITY_KEY)),
                   equalTo(some(StandardStreamsFilterOption.PROJECT_KEY)));
    }
    
    @Test
    public void assertThatIdentifierOfUnknownTypeReturnsNoFilterKey()
    {
        assertThat(provider.getFilterKey(new EntityIdentifier(UNKNOWN_ENTITY_TYPE, PROJECT_ENTITY_KEY, URI.create(UNKNOWN_BASE_URL))),
                   equalTo(none(String.class)));
    }
    
    @Test
    public void assertThatUserViewPermissionIsTrueWhenAppropriate()
    {
        setProjectViewPermission(PROJECT_ENTITY_KEY, true);
        assertThat(provider.getCurrentUserViewPermission(getProjectEntityIdentifier(PROJECT_ENTITY_KEY)),
                   equalTo(some(true)));
    }

    @Test
    public void assertThatUserViewPermissionIsFalseWhenAppropriate()
    {
        setProjectViewPermission(PROJECT_ENTITY_KEY, false);
        assertThat(provider.getCurrentUserViewPermission(getProjectEntityIdentifier(PROJECT_ENTITY_KEY)),
                   equalTo(some(false)));
    }
    
    @Test
    public void assertThatUserViewPermissionIsUnknownWhenProjectDoesNotExist()
    {
        setProjectExists(PROJECT_ENTITY_KEY, false);
        assertThat(provider.getCurrentUserViewPermission(getProjectEntityIdentifier(PROJECT_ENTITY_KEY)),
                   equalTo(none(Boolean.class)));
    }

    @Test
    public void assertThatUserEditPermissionIsTrueWhenAppropriate()
    {
        setProjectEditPermission(PROJECT_ENTITY_KEY, true);
        assertThat(provider.getCurrentUserEditPermission(getProjectEntityIdentifier(PROJECT_ENTITY_KEY)),
                   equalTo(some(true)));
    }
    
    @Test
    public void assertThatUserEditPermissionIsFalseWhenAppropriate()
    {
        setProjectEditPermission(PROJECT_ENTITY_KEY, false);
        assertThat(provider.getCurrentUserEditPermission(getProjectEntityIdentifier(PROJECT_ENTITY_KEY)),
                   equalTo(some(false)));
    }
    
    @Test
    public void assertThatUserEditPermissionIsUnknownWhenProjectDoesNotExist()
    {
        setProjectExists(PROJECT_ENTITY_KEY, false);
        assertThat(provider.getCurrentUserEditPermission(getProjectEntityIdentifier(PROJECT_ENTITY_KEY)),
                   equalTo(none(Boolean.class)));
    }
    
    private void assertHasOnlyProjectKey(Iterable<EntityIdentifier> identifiers, String key)
    {
        assertThat(identifiers, Matchers.contains(getProjectEntityIdentifier(key)));
    }
}
