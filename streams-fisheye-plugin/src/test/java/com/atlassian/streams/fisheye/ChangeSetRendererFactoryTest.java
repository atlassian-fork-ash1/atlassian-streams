package com.atlassian.streams.fisheye;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class ChangeSetRendererFactoryTest {
    @Test
    public void testCommitMessageRenderer() {
        Map<String,String> issues = new HashMap<>();
        issues.put("BC-123", "yyy");
        issues.put("ABC-1", "zzz");
        issues.put("ABC-123", "xxx");

        ChangeSetRendererFactory.CommitMessageRenderer r = new ChangeSetRendererFactory.CommitMessageRenderer(issues);
        assertEquals("<a href='xxx'>ABC-123</a>", r.render("ABC-123"));
        assertEquals("CR-ABC-123", r.render("CR-ABC-123"));
        assertEquals("<a href='xxx'>ABC-123</a> <a href='xxx'>ABC-123</a>", r.render("ABC-123 ABC-123"));
        assertEquals("<a href='zzz'>ABC-1</a>", r.render("ABC-1"));
        assertEquals("<a href='yyy'>BC-123</a>", r.render("BC-123"));
        assertEquals("<a href='zzz'>ABC-1</a>: blah", r.render("ABC-1: blah"));
        assertEquals("&lt;b&gt;XSS&lt;/b&gt;", r.render("<b>XSS</b>"));
    }
}
