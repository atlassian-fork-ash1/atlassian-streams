package com.atlassian.streams.fisheye;

import com.atlassian.streams.spi.StreamsKeyProvider.StreamsKey;

import com.cenqua.fisheye.config.RepositoryManager;
import com.cenqua.fisheye.rep.RepositoryHandle;
import com.google.common.base.Function;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Iterables.transform;

public class ProjectKeys
{
    private final RepositoryManager repositoryManager;

    private ProjectKeys(RepositoryManager repositoryManager)
    {
        this.repositoryManager = checkNotNull(repositoryManager, "repositoryManager");
    }

    public Iterable<StreamsKey> get()
    {
        return transform(repositoryManager.getHandles(), handlesToStreamsKey);
    }
    
    private static final Function<RepositoryHandle, StreamsKey> handlesToStreamsKey = new Function<RepositoryHandle, StreamsKey>()
    {
        public StreamsKey apply(RepositoryHandle handle)
        {
            final String description = handle.getCfg().getRepositoryTypeConfig().getDescription();
            return new StreamsKey(handle.getName(), description == null ? "" : description);
        }
    };

}
