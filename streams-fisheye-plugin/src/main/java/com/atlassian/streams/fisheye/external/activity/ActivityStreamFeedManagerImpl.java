package com.atlassian.streams.fisheye.external.activity;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.fisheye.activity.ExternalActivityItem;
import com.atlassian.fisheye.activity.ExternalActivityItemSearchParams;
import com.atlassian.sal.api.net.Request;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.google.common.base.Preconditions.checkNotNull;

public class ActivityStreamFeedManagerImpl implements ActivityStreamFeedManager
{
    private static final Logger log = LoggerFactory.getLogger(ActivityStreamFeedManagerImpl.class);
    private static final int CONNECTION_TIMEOUT = 1000;
    private static final int SO_TIMEOUT = 10000;

    private final RemoteStreamsFeedUriBuilder remoteStreamsFeedUriBuilder;
    private final ExternalActivityItemFactory itemFactory;

    public ActivityStreamFeedManagerImpl(RemoteStreamsFeedUriBuilder remoteStreamsFeedUriBuilder,
                                         ExternalActivityItemFactory itemFactory)
    {
        this.remoteStreamsFeedUriBuilder = checkNotNull(remoteStreamsFeedUriBuilder, "remoteStreamsFeedUriBuilder");
        this.itemFactory = checkNotNull(itemFactory, "itemFactory");
    }

    public Iterable<ExternalActivityItem> getItems(ApplicationLink appLink, ExternalActivityItemSearchParams params) throws Exception
    {
        final String uri = remoteStreamsFeedUriBuilder.buildUri(appLink, params).toASCIIString();

        try
        {
            final Request<?, ?> request = appLink.createAuthenticatedRequestFactory().createRequest(Request.MethodType.GET, uri);
            request.setConnectionTimeout(CONNECTION_TIMEOUT);
            request.setSoTimeout(SO_TIMEOUT);
            return itemFactory.getItems(request.execute());
        }
        catch (Exception e)
        {
            log.warn("Cannot fetch remote feed from: " + uri);
            throw e;
        }
    }
}
