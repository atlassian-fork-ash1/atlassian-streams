package com.atlassian.streams.bamboo;

import java.net.URI;
import java.util.Date;
import java.util.Iterator;
import java.util.Set;

import com.atlassian.bamboo.author.Author;
import com.atlassian.bamboo.build.Job;
import com.atlassian.bamboo.builder.BuildState;
import com.atlassian.bamboo.core.BambooObject;
import com.atlassian.bamboo.plan.PlanKeys;
import com.atlassian.bamboo.plan.cache.ImmutableJob;
import com.atlassian.bamboo.project.Project;
import com.atlassian.bamboo.project.ProjectManager;
import com.atlassian.bamboo.resultsummary.BuildResultsSummaryCriteria;
import com.atlassian.bamboo.resultsummary.ResultsSummary;
import com.atlassian.bamboo.resultsummary.ResultsSummaryCriteria;
import com.atlassian.bamboo.resultsummary.ResultsSummaryManager;
import com.atlassian.bamboo.utils.collection.PartialList;
import com.atlassian.bamboo.v2.build.trigger.ManualBuildTriggerReason;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.streams.api.ActivityObjectType;
import com.atlassian.streams.api.ActivityRequest;
import com.atlassian.streams.api.ActivityVerb;
import com.atlassian.streams.api.StreamsEntry;
import com.atlassian.streams.api.StreamsFeed;
import com.atlassian.streams.api.common.Option;
import com.atlassian.streams.api.common.Pair;
import com.atlassian.streams.spi.CancellableTask;
import com.atlassian.streams.spi.CancelledException;
import com.atlassian.streams.spi.Evictor;
import com.atlassian.streams.spi.Filters;
import com.atlassian.streams.spi.StreamsActivityProvider;

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

import org.hibernate.LazyInitializationException;
import org.hibernate.ObjectNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;

import static com.atlassian.streams.api.ActivityObjectTypes.comment;
import static com.atlassian.streams.api.ActivityVerbs.post;
import static com.atlassian.streams.api.StreamsEntry.byPostedDate;
import static com.atlassian.streams.api.StreamsFilterType.Operator.IS;
import static com.atlassian.streams.api.StreamsFilterType.Operator.NOT;
import static com.atlassian.streams.api.common.Iterables.flatMap;
import static com.atlassian.streams.api.common.Iterables.take;
import static com.atlassian.streams.api.common.Option.none;
import static com.atlassian.streams.api.common.Option.option;
import static com.atlassian.streams.api.common.Option.some;
import static com.atlassian.streams.api.common.Options.catOptions;
import static com.atlassian.streams.api.common.Pair.pair;
import static com.atlassian.streams.bamboo.BambooActivityVerbs.fail;
import static com.atlassian.streams.bamboo.BambooActivityVerbs.pass;
import static com.atlassian.streams.spi.Filters.entriesInActivities;
import static com.atlassian.streams.spi.Filters.getAllValues;
import static com.atlassian.streams.spi.Filters.getIsValues;
import static com.atlassian.streams.spi.Filters.getProjectKeys;
import static com.atlassian.streams.spi.Filters.inActivities;
import static com.atlassian.streams.spi.Filters.inIssueKeys;
import static com.atlassian.streams.spi.Filters.inProjectKeys;
import static com.atlassian.streams.spi.Filters.inSupportedActivities;
import static com.atlassian.streams.spi.Filters.inUsers;
import static com.atlassian.streams.spi.Iterables.filterOrEvict;
import static com.atlassian.streams.spi.StandardStreamsFilterOption.ACTIVITY_KEY;
import static com.atlassian.streams.spi.StandardStreamsFilterOption.ISSUE_KEY;
import static com.atlassian.streams.spi.StandardStreamsFilterOption.PROJECT_KEY;
import static com.atlassian.streams.spi.StandardStreamsFilterOption.USER;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Predicates.and;
import static com.google.common.base.Predicates.not;
import static com.google.common.base.Predicates.notNull;
import static com.google.common.base.Predicates.or;
import static com.google.common.collect.ImmutableList.copyOf;
import static com.google.common.collect.Iterables.all;
import static com.google.common.collect.Iterables.any;
import static com.google.common.collect.Iterables.concat;
import static com.google.common.collect.Iterables.contains;
import static com.google.common.collect.Iterables.filter;
import static com.google.common.collect.Iterables.getOnlyElement;
import static com.google.common.collect.Iterables.isEmpty;
import static com.google.common.collect.Iterables.size;
import static com.google.common.collect.Iterables.transform;
import static com.google.common.collect.Sets.intersection;

public class BambooStreamsActivityProvider implements StreamsActivityProvider
{
    private static final Logger log = LoggerFactory.getLogger(BambooStreamsActivityProvider.class);

    public static final String PROVIDER_KEY = "builds";
    public static final String BUILD_TRIGGER_REL = "http://streams.atlassian.com/syndication/build-trigger";

    private final ProjectManager projectManager;
    private final BambooEntryFactory entryFactory;
    private final I18nResolver i18nResolver;
    private final ResultsSummaryManager resultsSummaryManager;
    private final BambooAliasResultsSearcher authorSearch;
    private final Evictor<BambooObject> evictor;
    private static final int MAXIMUM_SAFE_RESULTS = 1000;

    public BambooStreamsActivityProvider(final ResultsSummaryManager resultsSummaryManager,
        final ProjectManager projectManager, final BambooEntryFactory entryFactory,
        final BambooAliasResultsSearcher authorSearch, final I18nResolver i18nResolver,
        final Evictor<BambooObject> evictor)
    {
        this.resultsSummaryManager = checkNotNull(resultsSummaryManager, "resultsSummaryManager");
        this.projectManager = checkNotNull(projectManager, "projectManager");
        this.entryFactory = checkNotNull(entryFactory, "entryFactory");
        this.i18nResolver = checkNotNull(i18nResolver, "i18nResolver");
        this.evictor = checkNotNull(evictor, "evictor");
        this.authorSearch = checkNotNull(authorSearch, "authorSearch");
    }

    /**
     * Get the activity feed for the given request
     * 
     * @param activityRequest The request
     * @return The ATOM feed
     */
    public CancellableTask<StreamsFeed> getActivityFeed(final ActivityRequest activityRequest)
    {
        return new CancellableTask<StreamsFeed>()
        {
            volatile boolean cancelled = false;
            final ImmutableSet<String> accountAliases = authorSearch.getLinkedAuthorsByUserNames(Filters.getAuthors(activityRequest));

            @Override
            public StreamsFeed call() throws Exception
            {
                Iterable<? extends ResultsSummary> results = getResultSummaries(activityRequest);

                if (cancelled)
                {
                    throw new CancelledException();
                }
                Iterable<? extends ResultsSummary> filteredResults = copyOf(filterOrEvict(evictor,
                                                                                          results,
                                                                                          and(byAuthors(activityRequest,
                                                                                                        accountAliases),
                                                                                              withIssueKeys(activityRequest))));
                results = null;

                // filter search results transform to feed entries
                final Iterable<Option<StreamsEntry>> buildEntries = transform(filteredResults,
                        summaryToEntry(activityRequest.getContextUri()));

                // we must filter by verb and itemKey, and then sort and limit the number of results before building the
                // feed
                Iterable<StreamsEntry> filteredEntries = filter(catOptions(buildEntries),
                                                                entriesInActivities(activityRequest));
                Iterable<StreamsEntry> sortedEntries = byPostedDate().sortedCopy(filteredEntries);

                return buildFeed(activityRequest, take(activityRequest.getMaxResults(), sortedEntries));
            }

            @Override
            public Result cancel()
            {
                cancelled = true;
                return Result.CANCELLED;
            }
        };
    }

    private Iterable<? extends ResultsSummary> getResultSummaries(ActivityRequest request)
    {
        Iterable<? extends ResultsSummary> resultsSummaryFromPlans = getResultsSummaryFromPlans(request);

        return copyOf(filterOrEvict(evictor, resultsSummaryFromPlans, byProjects(request)));
    }

    private Iterable<? extends ResultsSummary> getResultsSummaryFromPlans(ActivityRequest request)
    {
        // Optimization for searching by user (only) until ResultsSummaryCriteriaBuilder supports "authors" (BAM-8319)
        if (hasOnlyAuthorsFilter(request))
        {
            Iterable<String> userNames = Filters.getAuthors(request);
            PartialList<ResultsSummary> results = authorSearch.getResultsByUserNames(userNames, request.getMaxResults());
            return copyOf(filterOrEvict(evictor, results.getList(), isJobResultSummary));
        }

        ResultsSummaryCriteria resultsSummaryCriteria = new BuildResultsSummaryCriteria();

        // Apply date filters
        for (Date minDate : Filters.getMinDate(request))
        {
            resultsSummaryCriteria.setFromDate(minDate);
        }
        for (Date maxDate : Filters.getMaxDate(request))
        {
            resultsSummaryCriteria.setToDate(maxDate);
        }

        // Apply JIRA "IS" issue filters
        Iterable<String> issueKeys = getAllValues(IS, request.getStandardFilters().get(ISSUE_KEY.getKey()));
        resultsSummaryCriteria.setMatchesJiraIssues(ImmutableList.copyOf(issueKeys));

        resultsSummaryCriteria.setProjectKeys(getProjectKeys(request));

        Iterable<BuildState> buildStates = flatMap(inSupportedActivities(BambooFilterOptionProvider.activities).apply(request),
                                                   toBuildState);

        if (size(buildStates) == 1)
        {
            resultsSummaryCriteria.setBuildState(getOnlyElement(buildStates));
        }

        // If all the filters are supported by the ResultsSummaryManager, we can limit max results here
        if (!hasNonNativeFilters(request))
        {
            resultsSummaryCriteria.setMaxRowCount(request.getMaxResults());
        }
        else
        {
            log.info("Activity request '" + request.getUri() + "' has non-native filters");
        }

        // We can't do user filters; there's nothing in the API

        if (resultsSummaryCriteria.getMaxRowCount() > MAXIMUM_SAFE_RESULTS
            || resultsSummaryCriteria.getMaxRowCount() == 0)
        {
            //We can't let the user DoS the server
            resultsSummaryCriteria.setMaxRowCount(MAXIMUM_SAFE_RESULTS);
        }

        return resultsSummaryManager.getResultSummaries(resultsSummaryCriteria);
    }

    private static Function<Pair<ActivityObjectType, ActivityVerb>, Iterable<BuildState>> toBuildState = new Function<Pair<ActivityObjectType,ActivityVerb>, Iterable<BuildState>>()
    {
        public Iterable<BuildState> apply(Pair<ActivityObjectType, ActivityVerb> p)
        {
            if (p.second().equals(fail()))
            {
                return some(BuildState.FAILED);
            }
            else if (p.second().equals(pass()))
            {
                return some(BuildState.SUCCESS);
            }
            return none();
        }
    };

    private Function<ResultsSummary, Option<StreamsEntry>> summaryToEntry(final URI baseUri)
    {
        return new Function<ResultsSummary, Option<StreamsEntry>>()
        {
            @Override
            public Option<StreamsEntry> apply(@Nullable ResultsSummary results) {
                try
                {
                    return some(entryFactory.buildEntry(baseUri, results));
                }
                catch (Exception e)
                {
                    log.warn("Error creating streams entry", e);
                    return none(StreamsEntry.class);
                }
            }
        };
    }

    /*
     * When we fetch ResultSummaries Hibernate returns proxies. It's all good until RS gets
     * removed from DB by another thread. If this happens we get ObjectNotFoundException.
     * In order not to blow the activity streams page we catch the exception here.
     *
     * If the cause is different than ObjectNotFound we log it.
     */
    private static class WithoutHibernateErrors implements Predicate<ResultsSummary>
    {
        final private Predicate<ResultsSummary> predicate;

        private WithoutHibernateErrors(final Predicate<ResultsSummary> predicate)
        {
            this.predicate = predicate;
        }

        @Override
        public boolean apply(@Nullable final ResultsSummary input)
        {
            try
            {
                return predicate.apply(input);
            }
            catch (LazyInitializationException e)
            {
                if (! (e.getCause() instanceof ObjectNotFoundException))
                {
                    log.warn("Exception reading BRS data", e);
                }
                return false;
            }
        }
    }

    private static Predicate<ResultsSummary> withIssueKeys(final ActivityRequest request)
    {
        return new WithoutHibernateErrors(new WithIssueKeys(request));
    }

    private static final class WithIssueKeys implements Predicate<ResultsSummary>
    {
        private final Predicate<String> withIssueKeys;

        public WithIssueKeys(ActivityRequest request)
        {
            this.withIssueKeys = inIssueKeys(request);
        }

        public boolean apply(ResultsSummary result)
        {
            Set<String> issueKeys = result.getJiraIssueKeys();

            //any() automatically returns false if the iterable is empty, thus killing
            //comparisons involving empty sets
            if (isEmpty(issueKeys))
            {
                issueKeys = ImmutableSet.of("");
            }

            return any(issueKeys, withIssueKeys);
        }
    }

    private static Predicate<ResultsSummary> byProjects(final ActivityRequest request)
    {
        return new WithoutHibernateErrors(new ByProjects(request));
    }

    private static final class ByProjects implements Predicate<ResultsSummary>
    {
        private final Predicate<String> inProjectKeys;

        public ByProjects(ActivityRequest request)
        {
            this.inProjectKeys = inProjectKeys(request);
        }

        public boolean apply(ResultsSummary result)
        {
            String buildKey = result.getBuildKey();
            String projectKey = buildKey.substring(0, buildKey.indexOf('-'));
            return inProjectKeys.apply(projectKey);
        }
    }

    private static Predicate<ResultsSummary> byAuthors(final ActivityRequest request, Iterable<String> accountAliases)
    {
        return new WithoutHibernateErrors(new ByAuthors(request, accountAliases));
    }

    static private Predicate<String> inAuthors(final Iterable<String> aliases)
    {
        return new Predicate<String>()
        {
            @Override
            public boolean apply(String input)
            {
                return contains(aliases, input);
            }
        };
    }

    private static final class ByAuthors implements Predicate<ResultsSummary>
    {
        private final Predicate<String> byAuthors;
        private final boolean hasNotUserFilters;
        private final boolean hasIsUserFilters;

        public ByAuthors(ActivityRequest request, Iterable<String> authorAliases)
        {
            if (null != authorAliases)
            {
                this.byAuthors = Predicates.or(inUsers(request), inAuthors(authorAliases));
            }
            else
            {
                this.byAuthors = inUsers(request);
            }
            this.hasNotUserFilters = !isEmpty(getAllValues(NOT, request.getStandardFilters().get(USER.getKey())));
            this.hasIsUserFilters = !isEmpty(getIsValues(request.getStandardFilters().get(USER.getKey())));
        }

        public boolean apply(ResultsSummary result)
        {
            // because it is possible that no committers exist, also check if any author has triggered the manual build
            Iterable<String> allResultsSummaryAuthors = transform(result.getUniqueAuthors(), toAuthorUsername);

            for (String manualBuildAuthor : getManualBuildAuthor(result))
            {
                allResultsSummaryAuthors = concat(allResultsSummaryAuthors, ImmutableList.of(manualBuildAuthor));
            }
            if (isEmpty(allResultsSummaryAuthors))
            {
                //any() automatically returns false if the iterable is empty, thus killing
                //comparisons involving empty lists and NOT user filters
                allResultsSummaryAuthors = ImmutableList.of("");
            }

            if (!(hasNotUserFilters || hasIsUserFilters))
            {
                //if no user filters exist, return true. don't call any() below as it returns false for anonymous builds.
                return true;
            }
            else if (hasNotUserFilters)
            {
                //if checking NOT values, *all* authors must match
                return all(allResultsSummaryAuthors, byAuthors);
            }
            else
            {
                // check if *any* author is a committer to or triggered the build
                return any(allResultsSummaryAuthors, byAuthors);
            }
        }
    }

    private static Option<String> getManualBuildAuthor(ResultsSummary buildSummary)
    {
        if (buildSummary.getTriggerReason() instanceof ManualBuildTriggerReason)
        {
            ManualBuildTriggerReason triggerReason = (ManualBuildTriggerReason) buildSummary.getTriggerReason();
            return option(triggerReason.getUserName());
        }
        return none();
    }

    private StreamsFeed buildFeed(final ActivityRequest activityRequest, final Iterable<StreamsEntry> entries)
    {
        Iterable<String> projectKeys = getIsValues(activityRequest.getStandardFilters().get(PROJECT_KEY));
        return new StreamsFeed(i18nResolver.getText("streams.bamboo.title", getProjectNames(projectKeys)),
                               entries,
                               some(i18nResolver.getText("streams.bamboo.subtitle")));
    }

    private String getProjectNames(final Iterable<String> projectKeys)
    {
        if (isEmpty(projectKeys))
        {
            return i18nResolver.getText("streams.bamboo.emptyprojectlist.name");
        }
        Iterable<Project> projects = filter(transform(filter(projectKeys, notNull()), toProject), notNull());
        final StringBuilder result = new StringBuilder();
        for (final Iterator<Project> iter = projects.iterator(); iter.hasNext();)
        {
            final Project project = iter.next();
            if (result.length() > 0)
            {
                result.append(iter.hasNext() ? ", " : " " + i18nResolver.getText("streams.bamboo.conjunction.and") + " ");
            }
            result.append(project.getName());
        }
        return result.toString();
    }

    private final Function<String, Project> toProject = new Function<String, Project>()
    {
        public Project apply(final String projectKey)
        {
            return projectManager.getProjectByKey(projectKey);
        }
    };

    private static final Function<Author, String> toAuthorUsername = new Function<Author, String>()
    {
        public String apply(final Author author)
        {
            return author.getName();
        }
    };

    private boolean hasNonNativeFilters(ActivityRequest request)
    {
        return or(ImmutableList.of(hasNottedProjectFilters,
                                   hasMultipleActivityFilters,
                                   hasUnsupportedActivityFilters,
                                   hasNottedActivityFilters,
                                   hasNonNativeStandardFilters)).apply(request);
    }

    private boolean hasOnlyAuthorsFilter(ActivityRequest request)
    {
        return and(hasAuthorFilters,
                   hasSingleStandardFilter,
                   not(hasProviderFilters)).apply(request);
    }

    private static final Predicate<ActivityRequest> hasUnsupportedActivityFilters = new Predicate<ActivityRequest>()
    {
        public boolean apply(ActivityRequest activityRequest)
        {
            Iterable<Pair<ActivityObjectType, ActivityVerb>> unsupported = ImmutableList.of(pair(comment(), post()));
            return !isEmpty(getIsValues(activityRequest.getProviderFilters().get(ACTIVITY_KEY))) && any(unsupported, inActivities(activityRequest));
        }
    };

    private static final Predicate<ActivityRequest> hasMultipleActivityFilters = new Predicate<ActivityRequest>()
    {
        public boolean apply(ActivityRequest activityRequest)
        {
            return size(getIsValues(activityRequest.getProviderFilters().get(ACTIVITY_KEY))) > 1;
        }
    };

    private static final Predicate<ActivityRequest> hasNottedActivityFilters = new Predicate<ActivityRequest>()
    {
        public boolean apply(ActivityRequest activityRequest)
        {
            return !isEmpty(getAllValues(NOT, activityRequest.getProviderFilters().get(ACTIVITY_KEY)));
        }
    };

    private static final Predicate<ActivityRequest> hasNottedProjectFilters = new Predicate<ActivityRequest>()
    {
        public boolean apply(ActivityRequest activityRequest)
        {
            return !isEmpty(getAllValues(NOT, activityRequest.getStandardFilters().get(PROJECT_KEY)));
        }
    };

    private static final Predicate<ActivityRequest> hasNonNativeStandardFilters = new Predicate<ActivityRequest>()
    {
        public boolean apply(ActivityRequest activityRequest)
        {
            return !intersection(activityRequest.getStandardFilters().keySet(),
                                 ImmutableSet.<String>of(USER.getKey(), ISSUE_KEY.getKey())).isEmpty();
        }
    };

    private static final Predicate<ActivityRequest> hasAuthorFilters = new Predicate<ActivityRequest>()
    {
        public boolean apply(ActivityRequest activityRequest)
        {
            return !isEmpty(Filters.getAuthors(activityRequest));
        }
    };

    private static final Predicate<ActivityRequest> hasProviderFilters = new Predicate<ActivityRequest>()
    {
        public boolean apply(ActivityRequest activityRequest)
        {
            return !activityRequest.getProviderFilters().isEmpty();
        }
    };

    private static final Predicate<ActivityRequest> hasSingleStandardFilter = new Predicate<ActivityRequest>()
    {
        public boolean apply(ActivityRequest activityRequest)
        {
            return activityRequest.getStandardFilters().size() == 1;
        }
    };

    private static final Predicate<ResultsSummary> isJobResultSummary = new Predicate<ResultsSummary>()
    {
        public boolean apply(ResultsSummary resultsSummary)
        {
            return PlanKeys.isJobKey(resultsSummary.getPlanKey());
        }
    };
}
