package com.atlassian.streams.bamboo;

import java.net.URI;

import com.atlassian.bamboo.plan.Plan;
import com.atlassian.bamboo.build.Job;
import com.atlassian.bamboo.plan.PlanKeys;
import com.atlassian.bamboo.plan.cache.ImmutableJob;
import com.atlassian.bamboo.plan.cache.ImmutablePlan;
import com.atlassian.bamboo.resultsummary.ResultsSummary;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.streams.api.StreamsEntry.Link;
import com.atlassian.streams.spi.ServletPath;
import com.atlassian.streams.spi.StreamsI18nResolver;

import static com.atlassian.streams.api.common.Option.none;
import static com.atlassian.streams.api.common.Option.some;
import static com.atlassian.streams.api.common.uri.Uris.encode;

import static com.atlassian.bamboo.util.Narrow.to;
import static com.atlassian.streams.bamboo.BambooStreamsActivityProvider.BUILD_TRIGGER_REL;
import static com.atlassian.streams.bamboo.BambooStreamsActivityProvider.PROVIDER_KEY;
import static com.atlassian.streams.spi.StreamsActivityProvider.ICON_LINK_REL;
import static com.atlassian.streams.spi.StreamsActivityProvider.REPLY_TO_LINK_REL;
import static com.google.common.base.Preconditions.checkNotNull;

public class UriProvider
{
    private static final String IMAGES_ICONSV3 = "/images/iconsv3/";
    private static final String ICON_FAILED = IMAGES_ICONSV3 + "plan_failed_16.png";
    private static final String ICON_SUCCEEDED = IMAGES_ICONSV3 + "plan_successful_16.png";

    private final StreamsI18nResolver i18nResolver;
    
    public UriProvider(StreamsI18nResolver i18nResolver)
    {
        this.i18nResolver = checkNotNull(i18nResolver, "i18nResolver");
    }

    public Link getReplyToLink(final URI baseUri, final ResultsSummary buildSummary)
    {
        String uri = baseUri.toASCIIString() + ServletPath.COMMENTS + "/" +
            encode(PROVIDER_KEY) + "/" + buildSummary.getBuildResultKey();
        return new Link(URI.create(uri), REPLY_TO_LINK_REL, none(String.class));
    }
    
    public Link getBuildIconLink(final URI baseUri, final ResultsSummary buildSummary)
    {
        String uri = baseUri.toASCIIString() +
                (buildSummary.isSuccessful() ?  ICON_SUCCEEDED : ICON_FAILED);
        String tooltip = buildSummary.isSuccessful() ?
                "streams.item.bamboo.tooltip.build.successful" : "streams.item.bamboo.tooltip.build.failed";
        return new Link(URI.create(uri), ICON_LINK_REL, some(i18nResolver.getText(tooltip)));
    }
    
    public Link getBuildTriggerLink(final URI baseUri, final ResultsSummary buildSummary)
    {
        String uri = baseUri.toASCIIString() + "/rest/api/latest/queue/";
        final boolean isJob = PlanKeys.isJobKey(buildSummary.getPlanKey());
        if (isJob)
        {
            uri += PlanKeys.getChainKeyFromJobKey(buildSummary.getPlanKey());
        }
        else
        {
            uri += buildSummary.getBuildKey();
        }

        return new Link(URI.create(uri), BUILD_TRIGGER_REL, none(String.class));
    }

    public URI getBuildResultUri(final URI baseUri, final ResultsSummary buildSummary)
    {
        return URI.create(baseUri.toASCIIString() + "/browse/" + buildSummary.getBuildResultKey());
    }

    public URI getBuildResultUri(final URI baseUri, final String buildResultKey)
    {
        return URI.create(baseUri.toASCIIString() + "/browse/" + buildResultKey);
    }

    public URI getBuildPlanResultUri(final URI baseUri, final ImmutablePlan plan, final ResultsSummary buildSummary)
    {
        return URI.create(baseUri.toASCIIString() + "/browse/" + plan.getKey() + "-"
                + buildSummary.getBuildNumber());
    }
}
