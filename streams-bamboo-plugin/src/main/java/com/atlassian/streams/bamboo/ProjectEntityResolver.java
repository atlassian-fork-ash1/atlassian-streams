package com.atlassian.streams.bamboo;

import com.atlassian.bamboo.project.ProjectManager;
import com.atlassian.streams.api.common.Option;
import com.atlassian.streams.spi.EntityResolver;

import static com.google.common.base.Preconditions.checkNotNull;

public class ProjectEntityResolver implements EntityResolver
{
    private final ProjectManager projectManager;

    public ProjectEntityResolver(ProjectManager projectManager)
    {
        this.projectManager = checkNotNull(projectManager, "projectManager");
    }

    public Option<Object> apply(String key)
    {
        return Option.<Object>option(projectManager.getProjectByKey(key));
    }
}
