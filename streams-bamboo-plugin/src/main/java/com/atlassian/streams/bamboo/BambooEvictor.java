package com.atlassian.streams.bamboo;

import com.atlassian.bamboo.core.BambooObject;
import com.atlassian.bamboo.index.buildresult.BuildResultsSummaryDocument;
import com.atlassian.bamboo.persistence3.PluginHibernateSessionFactory;
import com.atlassian.streams.spi.Evictor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.hibernate.HibernateException;
import org.hibernate.Session;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Evicts {@link BambooObject} instances from Bamboo's {@link Session}.
 */
public class BambooEvictor implements Evictor<BambooObject>
{
    private static final Logger log = LoggerFactory.getLogger(BambooEvictor.class);
    private final PluginHibernateSessionFactory sessionFactory;

    public BambooEvictor(final PluginHibernateSessionFactory sessionFactory)
    {
        this.sessionFactory = checkNotNull(sessionFactory, "sessionFactory");
    }

    /**
     * Evicts the given instance from the current {@link Session}.
     *
     * @param object the instance to evict.
     */
    @Override
    public Void apply(final BambooObject object)
    {
        //JPA 2.1+ (Hibernate 4+) requires IAE when non-entites are evicted 
        if (!isEntity(object))
        {
            return null;
        }
        try
        {
            sessionFactory.getSession().evict(object);
        }
        catch (final HibernateException e)
        {
            log.warn("Failed to evict the entity from the session", e);
        }
        return null;
    }

    private static boolean isEntity(final BambooObject object)
    {
        // while we can't easily find out what is an entity,
        // Lucene classes definitely aren't - and we only worry about them anyway
        return !(object instanceof BuildResultsSummaryDocument);
    }
}
