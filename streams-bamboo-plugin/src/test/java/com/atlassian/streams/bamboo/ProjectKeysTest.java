package com.atlassian.streams.bamboo;

import java.util.List;
import java.util.Set;

import com.atlassian.bamboo.project.DefaultProject;
import com.atlassian.bamboo.project.Project;
import com.atlassian.bamboo.project.ProjectManager;
import com.atlassian.streams.spi.StreamsKeyProvider.StreamsKey;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeDiagnosingMatcher;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static com.google.common.collect.Iterables.transform;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ProjectKeysTest
{
    private static Set<Project> PROJECTS = ImmutableSet.of(
        new DefaultProject("PRJ1", "Project 1", null),
        new DefaultProject("PRJ2", "Project 2", null),
        new DefaultProject("PRJ3", "Project 3", null),
        new DefaultProject("PRJ4", "Project 4", null));

    @Mock
    private ProjectManager projectManager;

    private ProjectKeys projectKeys;

    @Before
    public void createKeyProvider()
    {
        projectKeys = new ProjectKeys(projectManager);
        when(projectManager.getAllProjects()).thenReturn(PROJECTS);
    }

    @Test
    public void assertThatGetKeysReturnsAllProjectKeys()
    {
        assertThat(projectKeys.get(), containsStreamKeys(ImmutableList.of(
            new StreamsKey("PRJ1", "Project 1"),
            new StreamsKey("PRJ2", "Project 2"),
            new StreamsKey("PRJ3", "Project 3"),
            new StreamsKey("PRJ4", "Project 4"))));
    }

    private static final Function<StreamsKey, Matcher<? super StreamsKey>> keyToStreamsKeyMatcher = ProjectKeysTest::streamsKey;

    public static Matcher<? super Iterable<StreamsKey>> containsStreamKeys(Iterable<StreamsKey> keys)
    {
        List<Matcher<? super StreamsKey>> transform = ImmutableList.copyOf(transform(keys, keyToStreamsKeyMatcher));
        return contains(transform);
    }

    public static Matcher<? super StreamsKey> streamsKey(StreamsKey key)
    {
        return new StreamsKeyMatcher(key);
    }

    private final static class StreamsKeyMatcher extends TypeSafeDiagnosingMatcher<StreamsKey>
    {
        private final StreamsKey key;

        StreamsKeyMatcher(StreamsKey key)
        {
            this.key = key;
        }

        @Override
        protected boolean matchesSafely(StreamsKey streamsKey, Description mismatchDescription)
        {
            if (!streamsKey.equals(key))
            {
                mismatchDescription.appendText("was streams [").appendValue(streamsKey).appendText("]");
                return false;
            }
            return true;
        }

        public void describeTo(Description description)
        {
            description.appendText("streams [").appendValue(key).appendText("]");
        }
    }
}
