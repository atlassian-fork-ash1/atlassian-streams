package it.com.atlassian.streams.jira;

import java.io.IOException;

import com.atlassian.integrationtesting.runner.TestGroups;
import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.streams.testing.FeedClient;
import com.atlassian.streams.testing.StreamsUiTesterRunner;

import com.google.common.collect.ImmutableList;
import com.google.inject.Inject;

import org.apache.abdera.model.Feed;
import org.hamcrest.Matcher;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;

import static com.atlassian.streams.api.ActivityObjectTypes.comment;
import static com.atlassian.streams.api.ActivityVerbs.post;
import static com.atlassian.streams.api.StreamsFilterType.Operator.IS;
import static com.atlassian.streams.api.common.Pair.pair;
import static com.atlassian.streams.testing.AbstractFeedClient.local;
import static com.atlassian.streams.testing.FeedClient.issueKey;
import static com.atlassian.streams.testing.FeedClient.maxResults;
import static com.atlassian.streams.testing.Jira.activities;
import static com.atlassian.streams.testing.Jira.Data.commentedOne1;
import static com.atlassian.streams.testing.Jira.Data.createdOne1;
import static com.atlassian.streams.testing.StreamsTestGroups.JIRA;
import static com.atlassian.streams.testing.matchers.Matchers.hasEntries;
import static com.atlassian.streams.testing.matchers.Matchers.hasEntry;
import static com.atlassian.streams.testing.matchers.Matchers.withActivityObjectElement;
import static com.atlassian.streams.testing.matchers.Matchers.withContent;
import static com.atlassian.streams.testing.matchers.Matchers.withTitle;
import static com.atlassian.streams.testing.matchers.Matchers.withType;
import static com.atlassian.streams.testing.matchers.Matchers.withVerbElement;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.not;

@RunWith(StreamsUiTesterRunner.class)
@TestGroups(JIRA)
public class JiraActivityStreamTest
{
    @Inject static FeedClient client;

    @Test
    @Restore("jira/backups/base.zip")
    public void assertThatFeedContainsEntryWithCommentActivityObjectTypeAndPostVerbForAddingAComment() throws IOException
    {
        Feed feed = client.getAs("admin", local(), issueKey(IS, "ONE-1"), activities(IS, pair(comment(), post())));

        assertThat(feed.getEntries(), hasEntry(
                withVerbElement(equalTo(post().iri().toASCIIString())),
                withActivityObjectElement(withType(equalTo(comment().iri().toASCIIString())))));
    }

    @Test
    @Restore("jira/backups/apostrophes.xml")
    public void assertThatCommentEntriesDoNotStripSingleQuotes() throws IOException
    {
        Feed feed = client.getAs("admin", local(), issueKey(IS, "ONE-2"), activities(IS, pair(comment(), post())));

        assertThat(feed.getEntries(), hasEntry(
                withTitle(allOf(containsString("admin"), containsString("commented on"), containsString("ONE-2"))),
                withContent(containsString("We're adding a comment"))
        ));
    }

    @Test
    @Restore("jira/backups/filter-eleven-issues.xml")
    public void assertThatOldIssueIsStillVisibleWhenFilteringByItemKey() throws IOException
    {
        // Test for STRM-1186 - which basically filters out an item that was updated before the
        //      maxResults number of entries
        Feed feed = client.getAs("admin", local(), issueKey(IS, "ONE-1"), maxResults(10));
        assertThat(feed.getEntries(), hasEntries(commentedOne1(), createdOne1()));
    }

    @Test
    @Restore("jira/backups/issue-assigned.xml")
    public void assertThatUpdatingAssigneeCreatesAnEntryWithProfileLinkToNewAssignee() throws Exception
    {
        Feed feed = client.getAs("admin", local(), issueKey(IS, "ONE-1"));
        assertThat(feed.getEntries(), hasEntry(
            withTitle(allOf(containsString("changed the Assignee to"),
                    containsString("<a href=\"" + client.getBaseUrl() + "/secure/ViewProfile.jspa?name=user\" class=\"activity-item-user\">User &lt;&gt;</a>")))));
    }

    @Test
    @Restore("jira/backups/reporter-changed.xml")
    public void assertThatUpdatingReporterCreatesAnEntryWithProfileLinkToNewReporter() throws Exception
    {
        Feed feed = client.getAs("admin", local(), issueKey(IS, "ONE-1"));
        assertThat(feed.getEntries(), hasEntry(
            withTitle(allOf(containsString("changed the Reporter to"),
                    containsString("<a href=\"" + client.getBaseUrl() + "/secure/ViewProfile.jspa?name=user\" class=\"activity-item-user\">User &lt;&gt;</a>")))));
    }

    @Test
    @Restore("jira/backups/anonymous.xml")
    public void assertThatFeedContainsAnonymousComment()
    {
        String feed = client.getAs("admin", String.class, issueKey(IS, "ONE-1"));
        assertThat(feed, containsString("Anonymous"));
    }

    @Test
    @Restore("jira/backups/comment-with-noformat-markup.xml")
    public void assertThatIllegalDataIsStripped() throws Exception
    {
        Feed feed = client.getAs("admin", local(), issueKey(IS, "ONE-1"));
        assertThat(feed.getEntries(), hasEntry(
                withTitle(allOf(containsString("admin"), containsString("commented on"), containsString("ONE-1"))),
                withContent(allOf(containsString("required pre-revision text"), not(containsString("rror"))))
        ));
    }

    @Test
    @Ignore("File uploads with HtmlUnit don't seem to be working STRM-260")
    public void assertThatFeedContainsMultipleUpdateEntryWithAttachmentsWhenMultipleFieldsAreUpdated() throws Exception
    {
//        client.logInAs("admin");
//        try
//        {
//            editIssue(client, "ONE-1", "new summary", IMPROVEMENT, BLOCKER, "28/Oct/10", "new environment",
//                    "new description", new File(getClass().getClassLoader().getResource("streams-jira-data.xml").toURI()),
//                    null);

            Feed feed = client.getAs("admin", local(), issueKey(IS, "ONE-1"));
            assertThat(feed.getEntries(), hasEntry(
                withTitle(allOf(containsString("updated 6 fields of"), containsString("ONE-1"))),
                withContent(allOf(ImmutableList.<Matcher<? super String>>of(
                        containsString("changed the Summary to 'new summary'"),
                        containsString("changed the Issue Type to 'Improvement'"),
                        containsString("updated the Environment"),
                        containsString("attached one file"),
                        containsString("streams-jira-data.xml"),
                        containsString("changed the Priority to 'Blocker'"),
                        containsString("updated the Description"),
                        containsString("changed the Due Date to '28/Oct/10'"))))));
//        }
//        finally
//        {
//            client.logout();
//        }
    }

    @Test
    @Ignore("File uploads with HtmlUnit don't seem to be working STRM-260")
    public void assertThatFeedContainsAttachVerbForAddingAnAttachment() throws Exception
    {
//        client.logInAs("admin");
//        try
//        {
//            attachFile(client, "ONE-2", new File(getClass().getClassLoader().getResource("streams-jira-data.xml").toURI()), "I'm attaching something");

            client.getAs("admin", local(), issueKey(IS, "ONE-2"));
//        }
//        finally
//        {
//            client.logout();
//        }
    }

    @Test
    @Ignore("File uploads with HtmlUnit don't seem to be working STRM-260")
    public void assertThatFeedContainsAttachmentActivityObjectForAddingAnAttachment() throws Exception
    {
//        client.logInAs("admin");
//        try
//        {
//            attachFile(client, "ONE-2", new File(getClass().getClassLoader().getResource("streams-jira-data.xml").toURI()), "I'm attaching something");

            client.getAs("admin", local(), issueKey(IS, "ONE-2"));
//        }
//        finally
//        {
//            client.logout();
//        }
    }


    @Test
    @Ignore("File uploads with HtmlUnit don't seem to be working STRM-260")
    public void assertThatFeedContainsEntryForAddingAnAttachment() throws Exception
    {
//        client.logInAs("admin");
//        try
//        {
//            attachFile(client, "ONE-2", new File(getClass().getClassLoader().getResource("streams-jira-data.xml").toURI()), "I'm attaching something");

            Feed feed = client.getAs("admin", local(), issueKey(IS, "ONE-2"));
            assertThat(feed.getEntries(), hasEntry(
                withTitle(containsString("attached one file to")),
                withContent(allOf(containsString("I'm attaching something"), containsString("streams-jira-data.xml")))
            ));
//        }
//        finally
//        {
//            client.logout();
//        }
    }
}
