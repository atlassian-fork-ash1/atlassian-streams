package it.com.atlassian.streams.thirdparty;

import com.atlassian.integrationtesting.runner.TestGroups;
import com.atlassian.integrationtesting.runner.restore.RestoreOnce;
import com.atlassian.streams.testing.StreamsUiTesterRunner;
import com.atlassian.streams.testing.ThirdPartyClient;
import com.atlassian.streams.testing.ThirdPartyTests;
import com.atlassian.streams.thirdparty.rest.representations.ActivityObjectRepresentation;

import com.sun.jersey.api.client.ClientResponse;

import org.apache.abdera.model.Feed;
import org.junit.Test;
import org.junit.runner.RunWith;

import static com.atlassian.streams.api.Html.html;
import static com.atlassian.streams.api.StreamsFilterType.Operator.IS;
import static com.atlassian.streams.api.common.Option.some;
import static com.atlassian.streams.testing.FeedClient.issueKey;
import static com.atlassian.streams.testing.FeedClient.key;
import static com.atlassian.streams.testing.StreamsTestGroups.JIRA;
import static com.atlassian.streams.testing.matchers.JerseyClientMatchers.forbidden;
import static com.atlassian.streams.testing.matchers.Matchers.hasEntry;
import static com.atlassian.streams.testing.matchers.Matchers.withTitle;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

@RunWith(StreamsUiTesterRunner.class)
@TestGroups(JIRA)
@RestoreOnce("jira/backups/base.zip")
public class JiraThirdPartyTest extends ThirdPartyTests
{
    private static final String ANON_AVATAR_URL = "/streams/secure/useravatar?avatarId=10063";
    private static final String PROJECT_KEY_ONE = "ONE";
    private static final String ISSUE_KEY_ONE_1 = "ONE-1";
    private static final String ISSUE_KEY_TWO_1 = "TWO-1";

    @Override
    public String getValidProjectKey()
    {
        return "ONE";
    }
    
    @Override
    public String getInvalidProjectKey()
    {
        return "TWO";
    }
    
    @Test
    public void activityWithJIRAIssueKeyIsIncludedByIssueFilter()
    {
        postActivityAndAssertSuccess(activity(actorWithId(USER_NAME), generator())
                                     .title(some(html(defaultTitle)))
                                     .target(some(ActivityObjectRepresentation.builder().urlString(some(ISSUE_KEY_ONE_1)).build())));

        Feed feed = client.getAs("admin", thirdPartyModule(), issueKey(IS, ISSUE_KEY_ONE_1));

        assertThat(feed.getEntries().size(), equalTo(1));
        assertThat(feed.getEntries(), hasEntry(withTitle(equalTo(defaultTitle))));
    }
    
    @Test
    public void activityWithoutJIRAIssueKeyIsExcludedByIssueFilter()
    {
        postActivityAndAssertSuccess(activity(actorWithId(USER_NAME), generator())
                                     .title(some(html(defaultTitle))));

        Feed feed = client.getAs("admin", thirdPartyModule(), issueKey(IS, ISSUE_KEY_ONE_1));

        assertThat(feed.getEntries().size(), equalTo(0));
    }
    
    @Test
    public void activityWithWrongJIRAIssueKeyIsExcludedByIssueFilter()
    {
        postActivityAndAssertSuccess(activity(actorWithId(USER_NAME), generator())
                                     .title(some(html(defaultTitle)))
                                     .target(some(ActivityObjectRepresentation.builder().urlString(some(ISSUE_KEY_TWO_1)).build())));

        Feed feed = client.getAs("admin", thirdPartyModule(), issueKey(IS, ISSUE_KEY_ONE_1));

        assertThat(feed.getEntries().size(), equalTo(0));
    }
    
    @Test
    public void activityWithJIRAIssueKeyIsIncludedByProjectFilter()
    {
        postActivityAndAssertSuccess(activity(actorWithId(USER_NAME), generator())
                                     .title(some(html(defaultTitle)))
                                     .target(some(ActivityObjectRepresentation.builder().urlString(some(ISSUE_KEY_ONE_1)).build())));

        Feed feed = client.getAs("admin", thirdPartyModule(), key(IS, PROJECT_KEY_ONE));

        assertThat(feed.getEntries().size(), equalTo(1));
        assertThat(feed.getEntries(), hasEntry(withTitle(equalTo(defaultTitle))));
    }
    
    @Test
    public void activityWithWrongJIRAIssueKeyIsExcludedByProjectFilter()
    {
        postActivityAndAssertSuccess(activity(actorWithId(USER_NAME), generator())
                                     .title(some(html(defaultTitle)))
                                     .target(some(ActivityObjectRepresentation.builder().urlString(some(ISSUE_KEY_TWO_1)).build())));

        Feed feed = client.getAs("admin", thirdPartyModule(), key(IS, PROJECT_KEY_ONE));

        assertThat(feed.getEntries().size(), equalTo(0));
    }

    @Test
    public void deletingActivityAsUserReturnsUnauthorized()
    {
        ThirdPartyClient userClient = thirdPartyClient.as("user", "user");
        try
        {
            ClientResponse response = thirdPartyClient.postActivity(activity(actorWithId(USER_NAME), generator()).build());
            ClientResponse deleteResponse = userClient.deleteActivity(response.getLocation());
            assertThat(deleteResponse, is(forbidden()));
        }
        finally
        {
            userClient.destroy();
        }
    }
}
