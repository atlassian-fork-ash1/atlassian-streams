package it.com.atlassian.streams.jira;

import com.atlassian.integrationtesting.runner.TestGroups;
import com.atlassian.integrationtesting.runner.restore.RestoreOnce;
import com.atlassian.streams.testing.FeedClient;
import com.atlassian.streams.testing.StreamsUiTesterRunner;
import com.google.inject.Inject;
import org.apache.abdera.model.Feed;
import org.junit.Test;
import org.junit.runner.RunWith;

import static com.atlassian.streams.api.StreamsFilterType.Operator.IS;
import static com.atlassian.streams.testing.FeedClient.issueKey;
import static com.atlassian.streams.testing.StreamsTestGroups.JIRA;
import static com.atlassian.streams.testing.matchers.Matchers.hasEntry;
import static com.atlassian.streams.testing.matchers.Matchers.withTitle;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.not;

@RunWith(StreamsUiTesterRunner.class)
@TestGroups(JIRA)
@RestoreOnce("jira/backups/issue-archiving.xml")
public class ArchivingStatusCustomFieldUpdateTest
{
    @Inject private FeedClient client;

    @Test
    public void assertThatFeedContainsUpdateOfArchivingStatus() throws Exception
    {
        Feed feed = client.getAs("admin", issueKey(IS, "THREE-2"));
        assertThat(feed.getEntries(), hasEntry(withTitle(allOf(
            containsString("archived"),
            not(containsString("Archived")),
            not(containsString("updated")),
            containsString("THREE-2")
        ))));
    }

    @Test
    public void assertThatFeedContainsIssueRestoredEntry() throws Exception
    {
        Feed feed = client.getAs("admin", issueKey(IS, "THREE-2"));
        assertThat(feed.getEntries(), hasEntry(withTitle(allOf(containsString("restored"),
                not(containsString("updated")),
                not(containsString("Restored")),
                containsString("THREE-2")))));
    }

}
