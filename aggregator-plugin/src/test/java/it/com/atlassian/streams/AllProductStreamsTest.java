package it.com.atlassian.streams;

import com.atlassian.integrationtesting.runner.TestGroups;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.streams.internal.rest.representations.ConfigPreferencesRepresentation;
import com.atlassian.streams.internal.rest.representations.StreamsConfigRepresentation;
import com.atlassian.streams.testing.FeedClient;
import com.atlassian.streams.testing.RestTester;
import com.atlassian.streams.testing.StreamsUiTesterRunner;

import com.google.inject.Inject;
import com.sun.jersey.api.client.ClientResponse;

import org.apache.abdera.model.Feed;
import org.joda.time.DateTime;
import org.junit.Test;
import org.junit.runner.RunWith;

import static com.atlassian.streams.api.StreamsFilterType.Operator.BETWEEN;
import static com.atlassian.streams.spi.StandardStreamsFilterOption.ISSUE_KEY;
import static com.atlassian.streams.spi.StandardStreamsFilterOption.UPDATE_DATE;
import static com.atlassian.streams.spi.StandardStreamsFilterOption.USER;
import static com.atlassian.streams.spi.StreamsActivityProvider.ICON_LINK_REL;
import static com.atlassian.streams.testing.FeedClient.updateDate;
import static com.atlassian.streams.testing.StreamsTestGroups.APPLINKS;
import static com.atlassian.streams.testing.StreamsTestGroups.APPLINKS_STREAMS3;
import static com.atlassian.streams.testing.StreamsTestGroups.BAMBOO;
import static com.atlassian.streams.testing.StreamsTestGroups.CONFLUENCE;
import static com.atlassian.streams.testing.StreamsTestGroups.FECRU;
import static com.atlassian.streams.testing.StreamsTestGroups.JIRA;
import static com.atlassian.streams.testing.matchers.FilterRepresentationMatchers.hasOption;
import static com.atlassian.streams.testing.matchers.FilterRepresentationMatchers.hasStandardFilters;
import static com.atlassian.streams.testing.matchers.Matchers.allEntries;
import static com.atlassian.streams.testing.matchers.Matchers.hasLink;
import static com.atlassian.streams.testing.matchers.Matchers.haveLink;
import static com.atlassian.streams.testing.matchers.Matchers.inDescendingOrder;
import static com.atlassian.streams.testing.matchers.Matchers.whereHref;
import static com.atlassian.streams.testing.matchers.Matchers.whereRel;
import static javax.ws.rs.core.Response.Status.FORBIDDEN;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.everyItem;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.startsWith;
import static org.junit.Assert.assertNotNull;

@RunWith(StreamsUiTesterRunner.class)
public class AllProductStreamsTest
{
    @Inject static FeedClient client;
    @Inject static RestTester rest;
    @Inject static ApplicationProperties applicationProperties;

    @Test
    public void assertThatActivityStreamIsAvailable()
    {
        assertNotNull(client.get());
    }

    @Test
    public void assertThatFeedContainsSelfLink()
    {
        Feed feed = client.get();
        assertThat(feed.getLinks(), hasLink(whereRel(is(equalTo("self"))), whereHref(is(equalTo(client.getUri().toASCIIString())))));
    }

    @Test
    public void assertThatFeedHasAtomContentType()
    {
        ClientResponse response = client.get(ClientResponse.class);
        assertThat(response.getHeaders().getFirst("Content-Type"), startsWith("application/atom+xml"));
    }

    @Test
    public void assertThatFeedIsSortedChronologically()
    {
        Feed feed = client.getAs("admin");
        assertThat(feed.getEntries(), is(inDescendingOrder()));
    }

    @Test
    public void assertThatAllEntriesContainIconLink()
    {
        //relative links will be filtered out if used for icons
        Feed feed = client.getAs("admin");
        assertThat(feed.getEntries(), allEntries(haveLink(whereRel(is(equalTo(ICON_LINK_REL))))));
    }

    @Test
    public void assertThatStreamsConfigPreferenceContainsTimeZone()
    {
        assertThat(rest.getConfigPreferencesRepresentation().getTimeZone(), is(notNullValue()));
    }

    @Test
    public void assertThatFilterOptionsAreAvailable()
    {
        assertNotNull(rest.getStreamsConfigRepresentation());
    }

    @Test
    public void assertThatFilterOptionsAreAvailableWhenNotLoggedIn()
    {
        RestTester anonRestTester = new RestTester(applicationProperties, false);
        assertNotNull(anonRestTester.getStreamsConfigRepresentation());
    }

    @Test
    public void assertThatInvalidDateBetweenParameterDoesNotThrowAnException()
    {
        assertNotNull(client.getAs("admin", updateDate(BETWEEN, new DateTime().withDate(2000, 1, 1), null)));
    }

    @Test
    @TestGroups(excludes = {CONFLUENCE, JIRA, APPLINKS, APPLINKS_STREAMS3})
    public void assertThatNonConfluenceNonJiraFilterOptionsContainStandardFilters()
    {
        StreamsConfigRepresentation representation = rest.getStreamsConfigRepresentation();
        assertThat(representation, hasStandardFilters(hasOption(USER), hasOption(UPDATE_DATE)));
    }

    /**
     * We expect the ISSUE_KEY option only in JIRA or when applinked to a JIRA
     */
    @Test
    @TestGroups(value = {JIRA, APPLINKS, APPLINKS_STREAMS3})
    public void assertThatJiraFilterOptionsContainStandardFilters()
    {
        StreamsConfigRepresentation representation = rest.getStreamsConfigRepresentation();
        assertThat(representation, hasStandardFilters(hasOption(USER), hasOption(UPDATE_DATE), hasOption(ISSUE_KEY)));
    }

    @Test
    @TestGroups(value = CONFLUENCE)
    public void assertThatConfluenceFilterOptionsContainStandardFilters()
    {
        StreamsConfigRepresentation representation = rest.getStreamsConfigRepresentation();
        assertThat(representation, hasStandardFilters(hasOption(USER), hasOption(UPDATE_DATE)));
    }

    /**
     * JIRA and Confluence have product-specific implementations. Only Fecru and Bamboo use the default values.
     */
    @Test
    @TestGroups(value = {FECRU, BAMBOO})
    public void assertThatTheDateFormatIsCorrect()
    {
        ConfigPreferencesRepresentation prefs = rest.getConfigPreferencesRepresentation();
        assertThat(prefs.getDateFormat(), is(equalTo("d MMM yyyy")));
    }

    /**
     * JIRA and Confluence have product-specific implementations. Only Fecru and Bamboo use the default values.
     */
    @Test
    @TestGroups(value = {FECRU, BAMBOO})
    public void assertThatTheTimeFormatIsCorrect()
    {
        ConfigPreferencesRepresentation prefs = rest.getConfigPreferencesRepresentation();
        assertThat(prefs.getTimeFormat(), is(equalTo("h:mm a")));
    }

    /**
     * JIRA and Confluence have product-specific implementations. Only Fecru and Bamboo use the default values.
     */
    @Test
    @TestGroups(value = {FECRU, BAMBOO})
    public void assertThatTheDateTimeFormatIsCorrect()
    {
        ConfigPreferencesRepresentation prefs = rest.getConfigPreferencesRepresentation();
        assertThat(prefs.getDateTimeFormat(), is(equalTo("d MMM yyyy h:mm a")));
    }

    @Test
    public void assertThatI18nValueIsCorrect()
    {
        assertThat(rest.getI18nValue("portlet.activityfeed.name"), is(equalTo("Activity Stream")));
    }

    @Test
    public void assertThatI18nValueUsesParameters()
    {
        assertThat(rest.getI18nValue("stream.item.title", new String[] { "author", "action", "item" }), not(
            containsString("{0}")));
    }

    @Test
    public void assertThatI18nValuesOnlyIncludesPrefixedKeys()
    {
        assertThat(rest.getI18nValues("portlet.activityfeed").getTranslations().keySet(), everyItem(startsWith(
            "portlet.activityfeed")));
    }

    @Test
    public void assertThatUrlProxyResourceIsNotWhitelistedToAccessLocalhost()
    {
        assertThat(rest.getProxiedResponse(applicationProperties.getBaseUrl()).getStatus(), is(equalTo(FORBIDDEN.getStatusCode())));
    }

    @Test
    public void assertThatUrlProxyResourceIsNotWhitelistedToAccessGoogle()
    {
        assertThat(rest.getProxiedResponse("http://www.google.com").getStatus(), is(equalTo(FORBIDDEN.getStatusCode())));
    }
}
