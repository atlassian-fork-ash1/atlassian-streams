package it.com.atlassian.streams.jira;

import com.atlassian.integrationtesting.runner.TestGroups;
import com.atlassian.integrationtesting.runner.restore.RestoreOnce;
import com.atlassian.streams.internal.rest.representations.ConfigPreferencesRepresentation;
import com.atlassian.streams.testing.RestTester;
import com.atlassian.streams.testing.StreamsUiTesterRunner;

import com.google.inject.Inject;

import org.junit.Test;
import org.junit.runner.RunWith;

import static com.atlassian.streams.testing.StreamsTestGroups.JIRA;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

@RunWith(StreamsUiTesterRunner.class)
@TestGroups(JIRA)
@RestoreOnce("jira/backups/custom-datetime-formats.xml")
public class JiraCustomDateTimeFormatTest
{
    @Inject static RestTester restTester;

    @Test
    public void assertThatTheCustomDateFormatIsCorrect()
    {
        ConfigPreferencesRepresentation prefs = restTester.getConfigPreferencesRepresentation();
        assertThat(prefs.getDateFormat(), is(equalTo("'new date format'")));
    }

    @Test
    public void assertThatTheCustomTimeFormatIsCorrect()
    {
        ConfigPreferencesRepresentation prefs = restTester.getConfigPreferencesRepresentation();
        assertThat(prefs.getTimeFormat(), is(equalTo("'new time format'")));
    }

    @Test
    public void assertThatTheCustomDateTimeFormatIsCorrect()
    {
        ConfigPreferencesRepresentation prefs = restTester.getConfigPreferencesRepresentation();
        assertThat(prefs.getDateTimeFormat(), is(equalTo("'new datetime format'")));
    }
}
