package it.com.atlassian.streams.jira;

import com.atlassian.integrationtesting.runner.TestGroups;
import com.atlassian.integrationtesting.runner.restore.RestoreOnce;
import com.atlassian.streams.testing.FeedClient;
import com.atlassian.streams.testing.StreamsUiTesterRunner;

import com.google.inject.Inject;

import org.apache.abdera.model.Feed;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;

import static com.atlassian.streams.testing.StreamsTestGroups.JIRA;
import static com.atlassian.streams.testing.matchers.Matchers.hasEntry;
import static com.atlassian.streams.testing.matchers.Matchers.withContent;
import static com.atlassian.streams.testing.matchers.Matchers.withTitle;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.containsString;

@RunWith(StreamsUiTesterRunner.class)
@TestGroups(JIRA)
@RestoreOnce("jira/backups/i18n-french.xml")
@Ignore("To be fixed in STRM-1998")
public class JiraI18nTest
{
    private static final String ISSUE_TYPE = "Type de demande";
    private static final String PRIORITY = "Priorit&eacute;";
    private static final String BLOCKER = "Blocage";
    private static final String IMPROVEMENT = "Am&eacute;lioration";
    private static final String TRIVIAL = "Simple";
    private static final String NEW_FUNCTIONALITY = "Nouvelle fonctionnalit&eacute;";
    private static final String DUPLICATE = "Doublon";
    private static final String TWO_CHANGES = "2 champs de";

    @Inject static FeedClient client;

    @Test
    public void assertThatFeedContainsFrenchUpdateIssueTypeToImprovement() throws Exception
    {
        Feed feed = client.getAs("admin");
        assertThat(feed.getEntries(), hasEntry(withTitle(allOf(containsString(ISSUE_TYPE), containsString(IMPROVEMENT)))));
    }

    @Test
    public void assertThatFeedContainsFrenchUpdatePriorityToBlocker() throws Exception
    {
        Feed feed = client.getAs("admin");
        assertThat(feed.getEntries(), hasEntry(withTitle(allOf(containsString(PRIORITY), containsString(BLOCKER)))));
    }

    @Test
    public void assertThatFeedContainsFrenchStatusResolution() throws Exception
    {
        Feed feed = client.getAs("admin");
        assertThat(feed.getEntries(), hasEntry(withTitle(allOf(containsString("ONE-2"), containsString(DUPLICATE)))));
    }

    @Test
    public void assertThatFeedContainsFrenchMultipleIssueUpdates() throws Exception
    {
        Feed feed = client.getAs("admin");
        assertThat(feed.getEntries(), hasEntry(allOf(
            withTitle(allOf(containsString(TWO_CHANGES), containsString("ONE-2"))),
            withContent(allOf(containsString(ISSUE_TYPE), containsString(NEW_FUNCTIONALITY))),
            withContent(allOf(containsString(PRIORITY), containsString(TRIVIAL)))
        )));
    }
}
