package it.com.atlassian.streams.jira;

import com.atlassian.integrationtesting.runner.TestGroups;
import com.atlassian.integrationtesting.runner.restore.RestoreOnce;
import com.atlassian.integrationtesting.ui.UiTester;
import com.atlassian.streams.testing.FeedClient;
import com.atlassian.streams.testing.StreamsUiTesterRunner;

import com.google.inject.Inject;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;

import static com.atlassian.streams.testing.StreamsTestGroups.JIRA;
import static org.junit.Assert.assertNotNull;

@RunWith(StreamsUiTesterRunner.class)
@TestGroups(JIRA)
@RestoreOnce("jira/backups/base.zip")
public class JiraActivityStreamUiTest
{
    @Inject static FeedClient client;
    @Inject static UiTester uiTester;

    @Test
    @Ignore("Gadgets seem to be broken in JIRA at the moment and this doesn't show up properly STRM-258")
    public void assertThatActivityStreamShowsUpAsAnIssueTab() throws Exception
    {
        uiTester.logInAs("admin");
        try
        {
            gotoActivityIssueTab("ONE-1");
            assertNotNull(uiTester.getElementById("gadget-stream"));
        }
        finally
        {
            uiTester.logout();
        }
    }

    private void gotoActivityIssueTab(String issueKey)
    {
        uiTester.gotoPage("browse/" + issueKey + "?page=com.atlassian.streams.streams-jira-plugin%3Aactivity-stream-issue-tab#issue-tabs");
    }

    @Test
    @Ignore("Gadgets seem to be broken in JIRA at the moment and this doesn't show up properly STRM-258")
    public void assertThatActivityStreamShowsUpInProjectSummary() throws Exception
    {
        uiTester.logInAs("admin");
        try
        {
            browseProject("ONE");
            assertNotNull(uiTester.getElementById("gadget-stream"));
        }
        finally
        {
            uiTester.logout();
        }
    }

    private void browseProject(String projectKey)
    {
        uiTester.gotoPage("browse/" + projectKey);
    }

    @Test
    @Ignore("Gadgets seem to be broken in JIRA at the moment and this doesn't show up properly STRM-258")
    public void assertThatActivityStreamShowsUpInUserProfile() throws Exception
    {
        uiTester.logInAs("admin");
        try
        {
            gotoUserProfile("admin");
            assertNotNull(uiTester.getElementById("gadget-stream"));
        }
        finally
        {
            uiTester.logout();
        }
    }

    private void gotoUserProfile(String user)
    {
        uiTester.gotoPage("/secure/ViewProfile.jspa?name=" + user);
    }

}
