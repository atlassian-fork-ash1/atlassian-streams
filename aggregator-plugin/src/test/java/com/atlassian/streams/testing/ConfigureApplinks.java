package com.atlassian.streams.testing;

import java.util.Set;

import javax.ws.rs.core.MediaType;

import com.google.common.collect.ImmutableSet;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;
import com.sun.jersey.api.representation.Form;

import static com.atlassian.streams.testing.App.CRUCIBLE;
import static com.google.common.base.Predicates.equalTo;
import static com.google.common.base.Predicates.in;
import static com.google.common.base.Predicates.not;
import static com.google.common.base.Predicates.or;
import static com.google.common.collect.Iterables.filter;
import static java.util.Arrays.asList;
import static org.apache.commons.lang3.StringEscapeUtils.escapeXml;
import static org.apache.commons.lang3.StringUtils.capitalize;

/**
 * Utility which automates configuring applinks.  It is expected that each app will already be configured to
 * trust the others before this is done (if there was a good cross product way to setup trusted apps then we could
 * put that in here too, but I haven't been able to find any).
 */
public class ConfigureApplinks
{
    static Client client = new Client();
    
    public static void main(String[] args)
    {
        client.addFilter(new HTTPBasicAuthFilter("admin", "admin"));
        for (App app : filter(asList(App.values()), not(equalTo(CRUCIBLE))))
        {
            configureApplinks(app);
        }
    }
    
    /**
     * Assumes all other apps have been configured to "trust" requests from {@code app}. 
     */
    private static void configureApplinks(App app)
    {
        System.out.println("Configuring " + app);
        for (App other : filter(asList(App.values()), not(or(equalTo(app), in(app.excludes())))))
        {
            if (addInstanceLink(app, other))
            {
                addProjectLink(app, other);
            }
        }
    }
    
    private static boolean addInstanceLink(App from, App to)
    {
        System.out.print("  adding instance link to " + to);
        Form form = new Form();
        form.add("application", to.name());
        form.add("name", to.toString());
        form.add("url", "http://localhost:" + to.port() + "/streams");
        
        ClientResponse response = client.resource("http://localhost:" + from.port() + "/streams/plugins/servlet/applinks/instance").
            type(MediaType.APPLICATION_FORM_URLENCODED).post(ClientResponse.class, form);
        if (response.getStatus() != 200)
        {
            System.out.println(" - failed with status " + response.getStatus());
            return false;
        }
        else
        {
            return true;
        }
    }
    
    private static void addProjectLink(App from, App to)
    {
        System.out.print(" - adding project link to " + to);
        
        String xml = "<com.atlassian.applinks.core.DefaultApplicationLink>" +
                         "<name>" + to + "</name>" +
                         "<remoteKey>" + to.project() + "</remoteKey>" +
                         "<url>" + escapeXml("http://localhost:" + to.port() + "/streams") + "</url>" +
                     "</com.atlassian.applinks.core.DefaultApplicationLink>";
        ClientResponse response = client.
            resource("http://localhost:" + from.port() + "/streams/plugins/servlet/applinks/projectlinks?key=" + from.project() + "&application=" + to.name()).
            type(MediaType.APPLICATION_XML).post(ClientResponse.class, xml);
        if (response.getStatus() != 200)
        {
            System.out.println(" - failed with status " + response.getStatus());
        }
        else
        {
            System.out.println(" - success!");
        }
    }
}

enum App
{
    JIRA(2990, "ONE"), BAMBOO(6990, "ONE"), CONFLUENCE(1990, "ONE"), FISHEYE(3990, "ONE"), CRUCIBLE(3990, "CR-ONE");
    
    private final int port;
    private final String project;
    
    App(int port, String project)
    {
        this.port = port;
        this.project = project;
    }
    
    int port()
    {
        return port;
    }
    
    String project()
    {
        return project;
    }
    
    Set<App> excludes()
    {
        switch (this)
        {
            case FISHEYE:
                return ImmutableSet.of(CRUCIBLE);
            case CRUCIBLE:
                return ImmutableSet.of(FISHEYE);
            default:
                return ImmutableSet.of();
        }
    }
    
    @Override
    public String toString()
    {
        return this == JIRA ? name() : capitalize(name().toLowerCase());
    }
}
