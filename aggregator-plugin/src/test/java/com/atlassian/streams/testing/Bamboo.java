package com.atlassian.streams.testing;

import java.util.List;

import com.atlassian.streams.api.ActivityObjectType;
import com.atlassian.streams.api.ActivityVerb;
import com.atlassian.streams.api.StreamsFilterType.Operator;
import com.atlassian.streams.api.common.Pair;
import com.atlassian.streams.testing.AbstractFeedClient.Parameter;

import com.google.common.collect.ImmutableList;

import org.apache.abdera.model.Entry;
import org.hamcrest.Matcher;
import org.joda.time.DateTime;
import org.joda.time.Months;

import static com.atlassian.streams.bamboo.BambooStreamsActivityProvider.PROVIDER_KEY;
import static com.atlassian.streams.testing.matchers.Matchers.withTitle;
import static com.google.common.collect.Iterables.concat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.not;

public class Bamboo
{
    public static final String BAMBOO_PROVIDER = "builds";

    public static Parameter activities(Operator op, Iterable<Pair<ActivityObjectType, ActivityVerb>> activities)
    {
        return FeedClient.activities(PROVIDER_KEY, op, activities);
    }

    public static Parameter activities(Operator op, Pair<ActivityObjectType, ActivityVerb>... activities)
    {
        return activities(op, ImmutableList.copyOf(activities));
    }

    public static Parameter activities(Operator op, Pair<ActivityObjectType, ActivityVerb> activity)
    {
        return activities(op, ImmutableList.of(activity));
    }

    public static Parameter activities(Operator op, Pair<ActivityObjectType, ActivityVerb> a1, Pair<ActivityObjectType, ActivityVerb> a2)
    {
        return activities(op, ImmutableList.of(a1, a2));
    }

    /**
     * Provides matchers for entries that will appear in the activity stream when started with the 
     * streams-bamboo-home.zip or bamboo-streams-applinks-test-resources.zip as the home directory.
     */
    public static final class Data
    {
    
        public static List<Matcher<? super Entry>> allBuildEntries()
        {
            return ImmutableList.copyOf(concat(buildEntriesForProjectAA(1), buildEntriesForProjectAP(1)));
        }
    
        public static List<Matcher<? super Entry>> allBuildEntriesForProjectAA()
        {
            return buildEntriesForProjectAA(1);
        }
    
        public static List<Matcher<? super Entry>> buildEntriesForProjectAA(int startBuildNumber)
        {
            return buildEntriesForProjectAA(startBuildNumber, ProjectAAResult.NUM_BUILDS);
        }
    
        public static List<Matcher<? super Entry>> buildEntriesForProjectAA(int startBuildNumber, int endBuildNumber)
        {
            ImmutableList.Builder<Matcher<? super Entry>> allEntries = ImmutableList.builder();
            for (int i = endBuildNumber; i >= startBuildNumber; i--)
            {
                allEntries.add(buildEntryForProjectAA(i));
            }
            return allEntries.build();
        }
    
        public static Matcher<? super Entry> buildEntryForProjectAA(int buildNumber)
        {
            ImmutableList.Builder<Matcher<? super String>> title = ImmutableList.builder();
            title.add(containsString("Atlassian Anarchy &rsaquo; Sounds like a plan"));
            title.add(containsString("Default Job"));
            title.add(containsString("AA-SLAP-" + buildNumber));
            title.add(containsString("AA-SLAP-JOB1-" + buildNumber));
    
            if (buildNumber > 1)
            {
                // STRM-477 removed broken english text fragment
                title.add(not(containsString("Updated by")));
                title.add(not(containsString("testadmin")));
            }
            title.add(containsString(
                ProjectAAResult.FOR_BUILD[buildNumber-1] == ProjectAAResult.SUCCESS ? "was successful" : "failed"));
    
            return withTitle(allOf(title.build()));
        }
    
        public static enum ProjectAAResult
        {
            SUCCESS, FAIL;
    
            public static final int NUM_BUILDS = 57;
            public static final ProjectAAResult[] FOR_BUILD = new ProjectAAResult[NUM_BUILDS];
    
            // Build 2 was completed at 2009-07-29 16:33:58
            public static final DateTime BUILD_2 = new DateTime()
                .withYear(2009).withMonthOfYear(Months.SEVEN.getMonths()).withDayOfMonth(29)
                .withHourOfDay(16).withMinuteOfHour(33).withSecondOfMinute(58).withMillisOfSecond(0);
     
            // Build 41 was completed at 2009-07-29 16:48:55
            public static final DateTime BUILD_41 = new DateTime()
                .withYear(2009).withMonthOfYear(Months.SEVEN.getMonths()).withDayOfMonth(29)
                .withHourOfDay(16).withMinuteOfHour(48).withSecondOfMinute(55).withMillisOfSecond(0);
     
            // Build 43 was completed at 2009-07-29 16:49:41
            public static final DateTime BUILD_43 = new DateTime()
                .withYear(2009).withMonthOfYear(Months.SEVEN.getMonths()).withDayOfMonth(29)
                .withHourOfDay(16).withMinuteOfHour(49).withSecondOfMinute(41).withMillisOfSecond(0);
     
            // Build 46 was completed at 2009-07-29 16:54:40
            public static final DateTime BUILD_56 = new DateTime()
                .withYear(2009).withMonthOfYear(Months.SEVEN.getMonths()).withDayOfMonth(29)
                .withHourOfDay(16).withMinuteOfHour(54).withSecondOfMinute(40).withMillisOfSecond(0);
     
            static
            {
                // calculate the build results. the build results follow a general pattern, with a few minor exceptional
                // cases, that being the 8th build and the 57th build
                // it goes (1 S) (2 F) (3 F) ... (8 S) (9 F) (10 S) (11 S) ... (32 S) (33 F) (34 F) ... (56 S) (57 S)
                for (int i = 0; i < 57; i++)
                {
                    if (i < 7)
                    {
                        if (isThird(i+1))
                        {
                            FOR_BUILD[i] = ProjectAAResult.SUCCESS;
                        }
                        else
                        {
                            FOR_BUILD[i] = ProjectAAResult.FAIL;
                        }
                    }
                    else if (i == 7)
                    {
                        FOR_BUILD[i] = ProjectAAResult.SUCCESS;
                    }
                    else if (i < 31)
                    {
                        if (isThird(i+1-8))
                        {
                            FOR_BUILD[i] = ProjectAAResult.FAIL;
                        }
                        else
                        {
                            FOR_BUILD[i] = ProjectAAResult.SUCCESS;
                        }
                    }
                    else if (i < 56)
                    {
                        if (isThird(i+1-31))
                        {
                            FOR_BUILD[i] = ProjectAAResult.SUCCESS;
                        }
                        else
                        {
                            FOR_BUILD[i] = ProjectAAResult.FAIL;
                        }
                    }
                    else
                    {
                        FOR_BUILD[i] = ProjectAAResult.SUCCESS;
                    }
                }
    
            }
        }
    
        public static List<Matcher<? super Entry>> allBuildEntriesForProjectAP()
        {
            return buildEntriesForProjectAP(1);
        }
    
        public static List<Matcher<? super Entry>> buildEntriesForProjectAP(int startBuildNumber)
        {
            return buildEntriesForProjectAP(startBuildNumber, ProjectAPResult.NUM_BUILDS);
        }
    
        public static List<Matcher<? super Entry>> buildEntriesForProjectAP(int startBuildNumber, int endBuildNumber)
        {
            ImmutableList.Builder<Matcher<? super Entry>> allEntries = ImmutableList.builder();
            for (int i = endBuildNumber; i >= startBuildNumber; i--)
            {
                allEntries.add(buildEntryForProjectAP(i));
            }
            return allEntries.build();
        }
    
        public static Matcher<? super Entry> buildEntryForProjectAP(int buildNumber)
        {
            ImmutableList.Builder<Matcher<? super String>> title = ImmutableList.builder();
            title.add(containsString("Another Project &rsaquo; Yet another plan"));
            title.add(containsString("Default Job"));
            title.add(containsString("AP-YET-" + buildNumber));
            title.add(containsString("AP-YET-JOB1-" + buildNumber));
    
            if (buildNumber > 1)
            {
                // STRM-477 removed broken english text fragment
                title.add(not(containsString("updated by")));
                title.add(not(containsString("testadmin")));
            }
            title.add(containsString(
                ProjectAPResult.FOR_BUILD[buildNumber-1] == ProjectAPResult.SUCCESS ? "was successful" : "failed"));
    
            return withTitle(allOf(title.build()));
        }
    
        public static enum ProjectAPResult
        {
            SUCCESS, FAIL;
    
            public static final int NUM_BUILDS = 6;
            public static final ProjectAPResult[] FOR_BUILD = new ProjectAPResult[NUM_BUILDS];
    
            static
            {
                // calculate the build results. the build results follow a general pattern
                // it goes (1 S) (2 F) (3 F) ... (8 S) (9 F) (10 S) (11 S) ... (32 S) (33 F) (34 F) ... (56 S) (57 S)
                for (int i = 0; i < NUM_BUILDS; i++)
                {
                    if (i < 6)
                    {
                        if (isThird(i+1))
                        {
                            FOR_BUILD[i] = ProjectAPResult.SUCCESS;
                        }
                        else
                        {
                            FOR_BUILD[i] = ProjectAPResult.FAIL;
                        }
                    }
                }
            }
        }
    
        private static boolean isThird(int n)
        {
            if (n == 1)
            {
                return true;
            }
            else if (n < 1)
            {
                return false;
            }
            else
            {
                return isThird(n - 3);
            }
        }
    }
}
