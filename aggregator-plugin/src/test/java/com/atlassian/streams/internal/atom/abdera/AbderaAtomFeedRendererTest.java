package com.atlassian.streams.internal.atom.abdera;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.net.URI;

import javax.xml.parsers.DocumentBuilderFactory;

import com.atlassian.plugin.webresource.UrlMode;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.streams.api.FeedContentSanitizer;
import com.atlassian.streams.api.StreamsEntry;
import com.atlassian.streams.api.UserProfile;
import com.atlassian.streams.api.common.ImmutableNonEmptyList;
import com.atlassian.streams.api.common.uri.Uri;
import com.atlassian.streams.internal.feed.ActivitySourceTimeOutFeedHeader;
import com.atlassian.streams.internal.feed.AuthRequiredFeedHeader;
import com.atlassian.streams.internal.feed.DefaultFeedRendererContext;
import com.atlassian.streams.internal.feed.FeedContentSanitizerImpl;
import com.atlassian.streams.internal.feed.FeedEntry;
import com.atlassian.streams.internal.feed.FeedHeader;
import com.atlassian.streams.internal.feed.FeedModel;
import com.atlassian.streams.internal.feed.FeedRendererContext;
import com.atlassian.streams.spi.FormatPreferenceProvider;

import com.atlassian.streams.testing.StreamsTestData;
import com.google.common.collect.ImmutableList;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import static com.atlassian.streams.api.common.Option.none;
import static com.atlassian.streams.api.common.Option.some;
import static com.atlassian.streams.spi.StreamsActivityProvider.ICON_LINK_REL;
import static com.atlassian.streams.spi.StreamsActivityProvider.REPLY_TO_LINK_REL;
import static com.atlassian.streams.testing.StreamsTestData.AGGREGATED_TITLE;
import static com.atlassian.streams.testing.StreamsTestData.BASE_URI;
import static com.atlassian.streams.testing.StreamsTestData.DST_DATE;
import static com.atlassian.streams.testing.StreamsTestData.ENTRY_ALTERNATE_URL;
import static com.atlassian.streams.testing.StreamsTestData.ENTRY_APP_NAME;
import static com.atlassian.streams.testing.StreamsTestData.ENTRY_CONTENT;
import static com.atlassian.streams.testing.StreamsTestData.ENTRY_DATE;
import static com.atlassian.streams.testing.StreamsTestData.ENTRY_DATE_STR;
import static com.atlassian.streams.testing.StreamsTestData.ENTRY_OBJECT_KEY;
import static com.atlassian.streams.testing.StreamsTestData.ENTRY_OBJECT_NAME;
import static com.atlassian.streams.testing.StreamsTestData.ENTRY_SUMMARY;
import static com.atlassian.streams.testing.StreamsTestData.ENTRY_TITLE;
import static com.atlassian.streams.testing.StreamsTestData.ENTRY_URI;
import static com.atlassian.streams.testing.StreamsTestData.ENTRY_USER_NAME;
import static com.atlassian.streams.testing.StreamsTestData.FEED_AUTHOR;
import static com.atlassian.streams.testing.StreamsTestData.FEED_DATE;
import static com.atlassian.streams.testing.StreamsTestData.FEED_DATE_STR;
import static com.atlassian.streams.testing.StreamsTestData.FEED_SUBTITLE;
import static com.atlassian.streams.testing.StreamsTestData.FEED_TITLE;
import static com.atlassian.streams.testing.StreamsTestData.FEED_URI;
import static com.atlassian.streams.testing.StreamsTestData.LA_TIMEZONE;
import static com.atlassian.streams.testing.StreamsTestData.LA_TIMEZONE_DST_OFFSET;
import static com.atlassian.streams.testing.StreamsTestData.LA_TIMEZONE_OFFSET;
import static com.atlassian.streams.testing.StreamsTestData.NON_DST_DATE;
import static com.atlassian.streams.testing.StreamsTestData.NY_TIMEZONE;
import static com.atlassian.streams.testing.StreamsTestData.NY_TIMEZONE_OFFSET;
import static com.atlassian.streams.testing.StreamsTestData.TYPE_TEST_URL;
import static com.atlassian.streams.testing.StreamsTestData.USER_AVATAR_URL;
import static com.atlassian.streams.testing.StreamsTestData.USER_PHOTO_URL;
import static com.atlassian.streams.testing.StreamsTestData.USER_PROFILE_URL;
import static com.atlassian.streams.testing.StreamsTestData.VERB_CHILD_URL;
import static com.atlassian.streams.testing.StreamsTestData.VERB_TEST_URL;
import static com.atlassian.streams.testing.StreamsTestData.childVerb;
import static com.atlassian.streams.testing.StreamsTestData.newActivityObject;
import static com.atlassian.streams.testing.StreamsTestData.newEntryParams;
import static com.atlassian.streams.testing.StreamsTestData.newEntryParamsWithoutObject;
import static com.atlassian.streams.testing.StreamsTestData.newRenderer;
import static com.google.inject.internal.Iterables.getOnlyElement;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.anyOf;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.xml.HasXPath.hasXPath;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AbderaAtomFeedRendererTest
{
    private static final UserProfile SOMEONE = new UserProfile.Builder(ENTRY_USER_NAME)
            .profilePageUri(some(URI.create(USER_PROFILE_URL)))
            .profilePictureUri(some(URI.create(USER_AVATAR_URL)))
            .build();

    @Mock I18nResolver i18nResolver;
    @Mock ApplicationProperties applicationProperties;
    @Mock WebResourceManager webResourceManager;
    @Mock FormatPreferenceProvider formatPreferenceProvider;
    FeedContentSanitizer sanitizer = new FeedContentSanitizerImpl();
    FeedRendererContext feedRendererContext;

    AbderaAtomFeedRenderer encoding;

    @Before
    public void createEncoding()
    {
        when(formatPreferenceProvider.getUserTimeZone()).thenReturn(LA_TIMEZONE);
        encoding = new AbderaAtomFeedRenderer(formatPreferenceProvider, sanitizer);

        when(i18nResolver.getText(DefaultFeedRendererContext.DEFAULT_FEED_TITLE)).thenReturn(AGGREGATED_TITLE);
        when(i18nResolver.getText(DefaultFeedRendererContext.DEFAULT_FEED_AUTHOR)).thenReturn(FEED_AUTHOR);

        feedRendererContext = new DefaultFeedRendererContext(i18nResolver, webResourceManager);
    }

    @Test
    public void contentTypeIsAtomXml()
    {
        assertThat(encoding.getContentType(), equalTo("application/atom+xml"));
    }

    @Test
    public void outputFeedContainsBasicProperties()
    {
        FeedModel feed = getBasicFeedProperties().build();

        Document doc = getEncodedFeedXml(feed);

        assertThat(doc, hasXPath("/feed/id", equalTo(FEED_URI.toString())));
        assertThat(doc, hasXPath("/feed/title[@type='text']", equalTo(FEED_TITLE)));
        assertThat(doc, hasXPath("/feed/subtitle[@type='text']", equalTo(FEED_SUBTITLE)));
        assertThat(doc, hasXPath("/feed/link[@rel='self']/@href", equalTo(FEED_URI.toString())));
        assertThat(doc, hasXPath("/feed/updated", equalTo(FEED_DATE_STR)));
    }

    @Test
    public void outputFeedContainsCorrectTimeZoneOffsetProperty()
    {
        FeedModel feed = getBasicFeedProperties().build();
        Document doc = getEncodedFeedXml(feed);

        assertThat(doc, hasXPath("/feed/timezone-offset", anyOf(equalTo(LA_TIMEZONE_OFFSET),
                equalTo(LA_TIMEZONE_DST_OFFSET))));
    }

    @Test
    public void outputFeedContainsExactlyOneTimeZoneOffsetProperty()
    {
        FeedModel feed = getBasicFeedProperties().build();
        Document doc = getEncodedFeedXml(feed);

        assertThat(doc, hasXPath("count(/feed/timezone-offset)", equalTo("1")));
    }

    @Test
    public void outputFeedUsesDefaultTitleIfFeedHadNoTitle()
    {
        FeedModel feed = getBasicFeedProperties().title(none(String.class)).build();

        Document doc = getEncodedFeedXml(feed);

        assertThat(doc, hasXPath("/feed/title[@type='text']", equalTo(AGGREGATED_TITLE)));
    }

    @Test
    public void outputFeedUsesDefaultAuthorIfFeedHadNoEntries()
    {
        FeedModel feed = getBasicFeedProperties().build();

        Document doc = getEncodedFeedXml(feed);

        assertThat(doc, hasXPath("/feed/author", equalTo(FEED_AUTHOR)));
    }

    @Test
    public void outputFeedContainsLink()
    {
        String rel = "timeout";
        Uri href = Uri.parse("http://foo/bar");
        FeedModel feed = getBasicFeedProperties()
                .addLink(rel, href)
                .build();

        Document doc = getEncodedFeedXml(feed);

        assertThat(doc, hasXPath("/feed/link[@rel='timeout']/@href", equalTo(href.toString())));
    }

    @Test
    public void outputFeedCanContainMultipleLinksWithSameRel()
    {
        String rel = "timeout";
        Uri href1 = Uri.parse("http://foo/bar");
        Uri href2 = Uri.parse("http://foo/baz");
        FeedModel feed = getBasicFeedProperties()
                .addLink(rel, href1)
                .addLink(rel, href2)
                .build();

        Document doc = getEncodedFeedXml(feed);

        assertThat(doc, hasXPath("/feed/link[@rel='timeout'][@href='" + href1 + "']"));
        assertThat(doc, hasXPath("/feed/link[@rel='timeout'][@href='" + href2 + "']"));
    }

    @Test
    public void outputFeedContainsAuthRequestHeader()
    {
        String appId = "appid",
                appName = "appname",
                appUri = "http://foo.com/bar",
                authUri = "http://foo.com/auth";
        FeedHeader header = new AuthRequiredFeedHeader(appId, appName, URI.create(appUri), URI.create(authUri));

        FeedModel feed = getBasicFeedProperties()
                .addHeaders(ImmutableList.of(header))
                .build();

        Document doc = getEncodedFeedXml(feed);

        assertThat(doc, hasXPath("/feed/authorisation-message/application-id", equalTo(appId)));
        assertThat(doc, hasXPath("/feed/authorisation-message/application-name", equalTo(appName)));
        assertThat(doc, hasXPath("/feed/authorisation-message/application-uri", equalTo(appUri)));
        assertThat(doc, hasXPath("/feed/authorisation-message/authorisation-uri", equalTo(authUri)));
    }

    @Test
    public void outputFeedContainsTimeoutName()
    {
        String appName = "app<b>name";
        FeedHeader header = new ActivitySourceTimeOutFeedHeader(appName);

        FeedModel feed = getBasicFeedProperties()
                .addHeaders(ImmutableList.of(header))
                .build();

        Document doc = getEncodedFeedXml(feed);

        assertThat(doc, hasXPath("/feed/timed-out-source-list/activity-source", equalTo(appName)));
    }

    @Test
    public void outputFeedContainsEntry()
    {
        StreamsEntry streamsEntry = new StreamsEntry(newEntryParams()
                .renderer(newRenderer(URI.create(ENTRY_URI), ENTRY_TITLE, some(ENTRY_CONTENT), some(ENTRY_SUMMARY)))
                .postedDate(ENTRY_DATE), i18nResolver);
        FeedEntry feedEntry = FeedEntry.fromStreamsEntry(streamsEntry);

        FeedModel feed = getBasicFeedProperties()
                .addEntries(ImmutableList.of(feedEntry))
                .build();

        Document doc = getEncodedFeedXml(feed);

        // Just check for the presence of the entry and its id & title attributes -
        // the rest of the entry attributes will be verified in separate tests
        assertThat(doc, hasXPath("/feed/entry[1]/id", equalTo(ENTRY_URI)));
        assertThat(doc, hasXPath("/feed/entry[1]/title[@type='html']", equalTo(ENTRY_TITLE)));
    }

    @Test
    public void assertThatEntryHasTitle()
    {
        StreamsEntry entry = new StreamsEntry(newEntryParams()
                .renderer(newRenderer(URI.create(ENTRY_URI), ENTRY_TITLE, ENTRY_CONTENT)), i18nResolver);
        Node node = getEncodedEntryXml(entry);

        assertThat(node, hasXPath("title[@type='html']", equalTo(ENTRY_TITLE)));
    }

    @Test
    public void assertThatIllegalCharactersStripped()
    {
        String summaryWithBadChar = "sum\u0002mary";
        String summaryStripped = "summary";
        StreamsEntry entry = new StreamsEntry(newEntryParams()
                .renderer(newRenderer(URI.create(ENTRY_URI), ENTRY_TITLE, some(ENTRY_CONTENT),
                        some(summaryWithBadChar))), i18nResolver);
        Node node = getEncodedEntryXml(entry);

        assertThat(node, hasXPath("summary", equalTo(summaryStripped)));
    }

    @Test
    public void assertThatSummaryIsSanitized()
    {
        String summaryWithBadChar = "<span style='background-image: url(\"javascript: alert('xss3');\");;'>summary<script>alert('xss');</script></span>";
        String summaryStripped = "<span style=\"\">summary</span>";
        StreamsEntry entry = new StreamsEntry(newEntryParams()
                .renderer(newRenderer(URI.create(ENTRY_URI), ENTRY_TITLE, some(ENTRY_CONTENT),
                        some(summaryWithBadChar))), i18nResolver);
        Node node = getEncodedEntryXml(entry);

        assertThat(node, hasXPath("summary", equalTo(summaryStripped)));
    }
    /**
     * Test to make sure records without descriptions (e.g. issue creation) do not result in SyndEntries with a
     * description of 'null'.
     */
    @Test
    public void assertThatBlankDescriptionNotIncluded()
    {
        StreamsEntry entry = new StreamsEntry(newEntryParams()
                .renderer(newRenderer(URI.create(ENTRY_URI), ENTRY_TITLE, some(ENTRY_CONTENT),
                        some(ENTRY_SUMMARY))), i18nResolver);
        Node node = getEncodedEntryXml(entry);

        // look for the css class for activity item descriptions
        assertThat(node, hasXPath("summary"));
        assertThat(node, hasXPath("summary", not(containsString("activity-item-description"))));
    }

    @Test
    public void assertThatStreamsGeneratorUriIsCreated()
    {
        StreamsEntry entry = new StreamsEntry(newEntryParams(), i18nResolver);
        Node node = getEncodedEntryXml(entry);

        assertThat(node, hasXPath("generator/@uri", equalTo(ENTRY_URI)));
    }

    @Test
    public void assertThatAuthorHasUsrUsernameElement()
    {
        StreamsEntry entry = new StreamsEntry(newEntryParams().authors(ImmutableNonEmptyList.of(SOMEONE)), i18nResolver);
        Node node = getEncodedEntryXml(entry);

        assertThat(node, hasXPath("author/username", equalTo(ENTRY_USER_NAME)));
    }

    @Test
    public void assertThatAuthorHasProfilePictureElement()
    {
        StreamsEntry entry = new StreamsEntry(newEntryParams().authors(ImmutableNonEmptyList.of(SOMEONE)), i18nResolver);
        Node node = getEncodedEntryXml(entry);

        assertThat(node, hasXPath("author/link[@rel='photo'][1]"));
        assertThat(node, hasXPath("author/link[@rel='photo'][2]"));
    }

    @Test
    public void assertThatAnonBambooEntryHasSpecialProfilePictureLink()
    {
        when(webResourceManager.getStaticPluginResource("com.atlassian.streams:streamsWebResources", "images/bamboo-logo-48.png", UrlMode.ABSOLUTE))
                .thenReturn(USER_PHOTO_URL);
        when(webResourceManager.getStaticPluginResource("com.atlassian.streams:streamsWebResources", "images/bamboo-logo-16.png", UrlMode.ABSOLUTE))
                .thenReturn(USER_PHOTO_URL);

        StreamsEntry entry = new StreamsEntry(newEntryParams().applicationType("com.atlassian.bamboo"), i18nResolver);
        Node node = getEncodedEntryXml(entry);

        assertThat(node, hasXPath("author/link[@rel='photo'][1]/@href",
                equalTo(USER_PHOTO_URL)));
    }

    @Test
    public void assertThatBambooEntryHasSpecialProfilePictureLink()
    {
        when(webResourceManager.getStaticPluginResource("com.atlassian.streams:streamsWebResources", "images/bamboo-logo-48.png", UrlMode.ABSOLUTE))
                .thenReturn(USER_PHOTO_URL);
        when(webResourceManager.getStaticPluginResource("com.atlassian.streams:streamsWebResources", "images/bamboo-logo-16.png", UrlMode.ABSOLUTE))
                .thenReturn(USER_PHOTO_URL);

        StreamsEntry entry = new StreamsEntry(newEntryParams().applicationType("com.atlassian.bamboo"), i18nResolver);
        Node node = getEncodedEntryXml(entry);

        assertThat(node, hasXPath("author/link[@rel='photo'][1]/@href",
                equalTo(USER_PHOTO_URL)));
    }

    @Test
    public void assertThatEntryHasPublishedAndUpdatedDate()
    {
        StreamsEntry entry = new StreamsEntry(newEntryParams().postedDate(ENTRY_DATE), i18nResolver);
        Node node = getEncodedEntryXml(entry);

        assertThat(node, hasXPath("published", equalTo(ENTRY_DATE_STR)));
        assertThat(node, hasXPath("updated", equalTo(ENTRY_DATE_STR)));
    }

    @Test
    public void assertThatEntryWithNonDstUpdateDateHasNonDstTimeZoneOffset()
    {
        StreamsEntry entry = new StreamsEntry(newEntryParams().postedDate(NON_DST_DATE), i18nResolver);
        Node node = getEncodedEntryXml(entry);

        assertThat(node, hasXPath("timezone-offset", equalTo(LA_TIMEZONE_OFFSET)));
    }

    @Test
    public void assertThatEntryWithDstUpdateDateHasDstTimeZoneOffset()
    {
        StreamsEntry entry = new StreamsEntry(newEntryParams().postedDate(DST_DATE), i18nResolver);
        Node node = getEncodedEntryXml(entry);

        assertThat(node, hasXPath("timezone-offset", equalTo(LA_TIMEZONE_DST_OFFSET)));
    }

    @Test
    public void assertThatTimeZoneOffsetIsRecomputedForAggregatedEntry() throws Exception
    {
        // Entry is originally generated & encoded by a machine in the LA time zone
        StreamsEntry entry1 = new StreamsEntry(newEntryParams().postedDate(NON_DST_DATE), i18nResolver);
        FeedModel feed1 = getBasicFeedProperties()
                .addEntries(ImmutableList.of(FeedEntry.fromStreamsEntry(entry1)))
                .build();
        InputStream encodedInput = getEncodedFeed(feed1);

        AbderaAtomFeedParser parser = new AbderaAtomFeedParser();
        FeedModel feed2 = parser.readFeed(new InputStreamReader(encodedInput));
        FeedEntry aggregatedEntry = getOnlyElement(feed2.getEntries());

        // Now the aggregated entry is being re-output by a machine in a different time zone
        when(formatPreferenceProvider.getUserTimeZone()).thenReturn(NY_TIMEZONE);
        Node node = getEncodedEntryXml(aggregatedEntry);

        assertThat(node, hasXPath("timezone-offset", equalTo(NY_TIMEZONE_OFFSET)));
    }

    @Test
    public void assertThatEntryHasInReplyToElement()
    {
        String replyToUri ="http://www.example.com/2";
        StreamsEntry entry = new StreamsEntry(newEntryParams().inReplyTo(some(URI.create(replyToUri))), i18nResolver);
        Node node = getEncodedEntryXml(entry);

        assertThat(node, hasXPath("in-reply-to/@ref", equalTo(replyToUri)));
    }

    @Test
    public void assertThatEntryHasNoInReplyToElementWhenStreamsDoesNotIncludeInReplyTo()
    {
        StreamsEntry entry = new StreamsEntry(newEntryParams(), i18nResolver);
        Node node = getEncodedEntryXml(entry);

        assertThat(node, not(hasXPath("in-reply-to")));
    }

    @Test
    public void assertThatEntryHasCorrectAtlassianApplicationElement()
    {
        StreamsEntry entry = new StreamsEntry(newEntryParams(), i18nResolver);
        Node node = getEncodedEntryXml(entry);

        assertThat(node, hasXPath("application", equalTo(ENTRY_APP_NAME)));
    }

    public void assertThatEntryHasReplyToLinkIfItExists()
    {
        String linkUri = "http://somewhere.com/SOME-1";
        StreamsEntry entry = new StreamsEntry(
                newEntryParams().addLink(URI.create(linkUri), REPLY_TO_LINK_REL, none(String.class)), i18nResolver);
        Node node = getEncodedEntryXml(entry);

        assertThat(node, hasXPath("link[rel='" + REPLY_TO_LINK_REL + "']/@href",
                equalTo(linkUri)));
    }

    @Test
    public void assertThatEntryHasNoReplyToLinkIfItDoesNotExist()
    {
        StreamsEntry entry = new StreamsEntry(newEntryParams(), i18nResolver);
        Node node = getEncodedEntryXml(entry);

        assertThat(node, not(hasXPath("link[rel='" + REPLY_TO_LINK_REL + "']/@href")));
    }

    @Test
    public void assertThatEntryHasIconLinkIfItExists()
    {
        String iconUri = "http://somewhere.com/icon.jpg";
        StreamsEntry entry = new StreamsEntry(
                newEntryParams().addLink(URI.create(iconUri), ICON_LINK_REL, none(String.class)), i18nResolver);
        Node node = getEncodedEntryXml(entry);

        assertThat(node, hasXPath("link[@rel='" + ICON_LINK_REL + "']/@href",
                equalTo(iconUri)));
    }

    @Test
    public void assertThatEntryHasNoIconLinkIfItDoesNotExist()
    {
        StreamsEntry entry = new StreamsEntry(newEntryParams(), i18nResolver);
        Node node = getEncodedEntryXml(entry);

        assertThat(node, not(hasXPath("link[@rel='" + ICON_LINK_REL + "']/@href")));
    }

    @Test
    public void assertThatEntryHasNoIconLinkIfRelative()
    {
        StreamsEntry entry = new StreamsEntry(
                newEntryParams().addLink(URI.create("icon.jpg"), ICON_LINK_REL, none(String.class)), i18nResolver);
        Node node = getEncodedEntryXml(entry);

        assertThat(node, not(hasXPath("link[@rel='" + ICON_LINK_REL + "']/@href")));
    }

    @Test
    public void assertThatEntryHasVerbElement()
    {
        StreamsEntry entry = new StreamsEntry(newEntryParams(), i18nResolver);
        Node node = getEncodedEntryXml(entry);

        assertThat(node, hasXPath("verb", equalTo(VERB_TEST_URL)));
    }

    @Test
    public void assertThatEntryHasMultipleVerbElementsIfVerbHasParents()
    {
        StreamsEntry entry = new StreamsEntry(newEntryParams().verb(childVerb()), i18nResolver);
        Node node = getEncodedEntryXml(entry);

        assertThat(node, hasXPath("verb[1]", equalTo(VERB_TEST_URL)));
        assertThat(node, hasXPath("verb[2]", equalTo(VERB_CHILD_URL)));
    }

    @Test
    public void assertThatEntryHasActivityObjectElement()
    {
        StreamsEntry entry = new StreamsEntry(newEntryParams(), i18nResolver);
        Node node = getEncodedEntryXml(entry);

        assertThat(node, hasXPath("object"));
    }

    @Test
    public void assertThatAbderaActivityObjectHasTheCorrectId()
    {
        StreamsEntry entry = new StreamsEntry(newEntryParams(), i18nResolver);
        Node node = getEncodedEntryXml(entry);

        assertThat(node, hasXPath("object/id", equalTo(ENTRY_OBJECT_KEY)));
    }

    @Test
    public void assertThatActivityObjectIdCanBeOmitted()
    {
        StreamsEntry entry = new StreamsEntry(newEntryParams(new DateTime(),
                newActivityObject(ENTRY_OBJECT_KEY, none(String.class), some(URI.create(ENTRY_URI)))),
                i18nResolver);
        Node node = getEncodedEntryXml(entry);

        assertThat(node, hasXPath("object"));
        assertThat(node, not(hasXPath("object/id")));
    }

    @Test
    public void assertThatAbderaActivityObjectHasTheCorrectTitle()
    {
        StreamsEntry entry = new StreamsEntry(newEntryParams(), i18nResolver);
        Node node = getEncodedEntryXml(entry);

        assertThat(node, hasXPath("object/title", equalTo(ENTRY_OBJECT_NAME)));
    }

    @Test
    public void assertThatAbderaActivityObjectHasTheCorrectAlternateLink()
    {
        StreamsEntry entry = new StreamsEntry(newEntryParams(), i18nResolver);
        Node node = getEncodedEntryXml(entry);

        assertThat(node, hasXPath("object/link/@href", equalTo(ENTRY_ALTERNATE_URL)));
    }

    @Test
    public void assertThatActivityObjectAlternateLinkCanBeOmitted()
    {
        StreamsEntry entry = new StreamsEntry(newEntryParams(new DateTime(),
                newActivityObject(ENTRY_OBJECT_KEY, some(ENTRY_OBJECT_KEY), none(URI.class))),
                i18nResolver);
        Node node = getEncodedEntryXml(entry);

        assertThat(node, hasXPath("object"));
        assertThat(node, not(hasXPath("object/link")));
    }

    @Test
    public void assertThatAbderaActivityObjectHasTheCorrectObjectType()
    {
        StreamsEntry entry = new StreamsEntry(newEntryParams(), i18nResolver);
        Node node = getEncodedEntryXml(entry);

        assertThat(node, hasXPath("object/object-type", equalTo(TYPE_TEST_URL)));
    }

    @Test
    public void assertThatEntryHasMultipleActivityObjectElementsIfMoreThanOneActivityObjectIsSupplied()
    {
        StreamsEntry entry = new StreamsEntry(newEntryParams().addActivityObject(newActivityObject("Two")), i18nResolver);

        Node node = getEncodedEntryXml(entry);

        assertThat(node, hasXPath("object[1]/id", equalTo(ENTRY_OBJECT_KEY)));
        assertThat(node, hasXPath("object[2]/id", equalTo("Two")));
    }

    @Test
    public void assertThatActivityObjectCanBeOmitted()
    {
        StreamsEntry entry = new StreamsEntry(newEntryParamsWithoutObject(), i18nResolver);

        Node node = getEncodedEntryXml(entry);

        assertThat(node, not(hasXPath("object")));
    }

    private InputStream getEncodedFeed(FeedModel feed)
    {
        try
        {
            StringWriter sw = new StringWriter();
            encoding.writeFeed(URI.create(ENTRY_URI), feed, sw, feedRendererContext);
            return new ByteArrayInputStream(sw.toString().getBytes());
        }
        catch (Exception e)
        {
            throw new IllegalStateException(e);
        }
    }

    private Document getEncodedFeedXml(FeedModel feed)
    {
        try
        {
            return DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(getEncodedFeed(feed));
        }
        catch (Exception e)
        {
            throw new IllegalStateException(e);
        }
    }

    private FeedModel.Builder getBasicFeedProperties() {
        return FeedModel.builder(FEED_URI)
                .title(some(FEED_TITLE))
                .subtitle(some(FEED_SUBTITLE))
                .updated(some(FEED_DATE));
    }

    private Node getEncodedEntryXml(StreamsEntry entry)
    {
        return getEncodedEntryXml(FeedEntry.fromStreamsEntry(entry));
    }

    private Node getEncodedEntryXml(FeedEntry entry)
    {
        FeedModel feed = getBasicFeedProperties()
                .addEntries(ImmutableList.of(entry))
                .build();
        Document doc = getEncodedFeedXml(feed);
        NodeList list = doc.getElementsByTagName("entry");
        assertThat(list.getLength(), equalTo(1));
        return list.item(0);
    }
}
