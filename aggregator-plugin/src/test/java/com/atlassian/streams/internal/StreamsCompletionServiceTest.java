package com.atlassian.streams.internal;

import com.atlassian.failurecache.util.date.Clock;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.sal.api.executor.ThreadLocalDelegateExecutorFactory;
import com.atlassian.streams.api.common.Either;
import com.google.common.collect.Lists;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collections;
import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static com.atlassian.streams.internal.ActivityProvider.Error.other;
import static com.atlassian.streams.internal.ActivityProvider.Error.unauthorized;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Test for {@link StreamsCompletionService}
 */
@RunWith(MockitoJUnitRunner.class)
public class StreamsCompletionServiceTest
{
    @Mock
    private ThreadLocalDelegateExecutorFactory threadLocalDelegateExecutorFactory;
    @Mock
    private PluginEventManager pluginEventManager;
    @Mock
    private Clock clock;
    @Mock
    private ActivityProvider activityProvider;
    @InjectMocks
    private StreamsCompletionService streamsCompletionService;

    @Test
    public void testActivityProviderFailureRegistered()
    {
        doReturn(Executors.newSingleThreadExecutor()).when(threadLocalDelegateExecutorFactory).createExecutorService(any(ExecutorService.class));

        when(clock.getCurrentDate()).thenReturn(new Date());


        ActivityProviderCallable<Either<ActivityProvider.Error, Long>> activityProviderCallable = new TestActivityProviderErrorCallable(
                activityProvider, 0, other(activityProvider));

        assertTrue("Activity provider is reachable to start with", streamsCompletionService.reachable().apply(activityProvider));

        streamsCompletionService.execute(Collections.singleton(activityProviderCallable));

        assertFalse("Activity provider has been marked as unreachable", streamsCompletionService.reachable().apply(activityProvider));
    }

    @Test
    public void testUnauthorizedActivityProviderFailuresNotRegistered()
    {
        when(threadLocalDelegateExecutorFactory.createExecutorService(any(ExecutorService.class))).thenReturn(Executors.newSingleThreadExecutor());

        StreamsCompletionService streamsCompletionService = new StreamsCompletionService(threadLocalDelegateExecutorFactory, pluginEventManager, clock);

        ActivityProviderCallable<Either<ActivityProvider.Error, Long>> activityProviderCallable = new TestActivityProviderErrorCallable(
                activityProvider, 0, unauthorized(activityProvider));

        assertTrue("Activity provider is reachable to start with", streamsCompletionService.reachable().apply(activityProvider));

        streamsCompletionService.execute(Collections.singleton(activityProviderCallable));

        assertTrue("Activity provider should be reachable after \"Unauthorized\" error", streamsCompletionService.reachable().apply(activityProvider));
    }

    @Test
    public void testActivityProviderTimeoutRegistered()
    {
        when(threadLocalDelegateExecutorFactory.createExecutorService(any(ExecutorService.class))).thenReturn(Executors.newSingleThreadExecutor());

        when(clock.getCurrentDate()).thenReturn(new Date());

        StreamsCompletionService streamsCompletionService = new StreamsCompletionService(threadLocalDelegateExecutorFactory, pluginEventManager, clock);

        ActivityProviderCallable<Either<ActivityProvider.Error, Long>> activityProviderCallable = new TestActivityProviderCallable(activityProvider, 100);

        assertTrue("Activity provider is reachable to start with", streamsCompletionService.reachable().apply(activityProvider));

        streamsCompletionService.execute(Collections.singleton(activityProviderCallable), 1, TimeUnit.MILLISECONDS);

        assertFalse("Activity provider has been marked as unreachable", streamsCompletionService.reachable().apply(activityProvider));
    }

    @Test
    public void testActivityProviderSuccessRegistered()
    {
        when(threadLocalDelegateExecutorFactory.createExecutorService(any(ExecutorService.class))).thenReturn(Executors.newSingleThreadExecutor());

        when(clock.getCurrentDate()).thenReturn(new Date());

        StreamsCompletionService streamsCompletionService = new StreamsCompletionService(threadLocalDelegateExecutorFactory, pluginEventManager, clock);

        ActivityProviderCallable<Either<ActivityProvider.Error, Long>> badActivityProviderCallable = new TestActivityProviderErrorCallable(activityProvider, 0, other(this.activityProvider));
        ActivityProviderCallable<Either<ActivityProvider.Error, Long>> goodActivityProviderCallable = new TestActivityProviderCallable(activityProvider, 0);

        assertTrue("Activity provider is reachable to start with", streamsCompletionService.reachable().apply(activityProvider));

        streamsCompletionService.execute(Collections.singleton(badActivityProviderCallable));

        assertFalse("Activity provider has been marked as unreachable", streamsCompletionService.reachable().apply(activityProvider));

        streamsCompletionService.execute(Collections.singleton(goodActivityProviderCallable));

        assertTrue("Activity provider is reachable again", streamsCompletionService.reachable().apply(activityProvider));
    }

    @Test
    public void testOnlySlowActivityProvidersAreCancelled()
    {
        when(threadLocalDelegateExecutorFactory.createExecutorService(any(ExecutorService.class))).thenReturn(Executors.newFixedThreadPool(5));

        when(clock.getCurrentDate()).thenReturn(new Date());

        StreamsCompletionService streamsCompletionService = new StreamsCompletionService(threadLocalDelegateExecutorFactory, pluginEventManager, clock);

        ActivityProvider slowActivityProvider = mock(ActivityProvider.class);

        ActivityProviderCallable<Either<ActivityProvider.Error, Long>> firstActivityProviderCallable = new TestActivityProviderCallable(activityProvider, 50);
        ActivityProviderCallable<Either<ActivityProvider.Error, Long>> secondActivityProviderCallable = new TestActivityProviderCallable(activityProvider, 10);
        ActivityProviderCallable<Either<ActivityProvider.Error, Long>> slowActivityProviderCallable = new TestActivityProviderCallable(slowActivityProvider, 110);


        assertTrue("Activity provider is reachable to start with", streamsCompletionService.reachable().apply(activityProvider));

        Iterable<Either<ActivityProvider.Error, Long>> executed = streamsCompletionService.execute(Lists.newArrayList(firstActivityProviderCallable, secondActivityProviderCallable, slowActivityProviderCallable), 100, TimeUnit.MILLISECONDS);

        Matcher<Iterable<? extends Either<ActivityProvider.Error, Long>>> expectedResults =
                containsInAnyOrder(right(10L), right(50L), left(ActivityProvider.Error.Type.TIMEOUT, slowActivityProvider));
        assertThat(executed, expectedResults);
        assertThat(streamsCompletionService.reachable().apply(activityProvider), is(true));
        assertThat(streamsCompletionService.reachable().apply(slowActivityProvider), is(false));
    }

    private Matcher<Either<ActivityProvider.Error, Long>> right(final long value)
    {
        return new TypeSafeMatcher<Either<ActivityProvider.Error, Long>>()
        {
            @Override
            protected boolean matchesSafely(final Either<ActivityProvider.Error, Long> item)
            {
                return item.isRight() && item.right().get() == value;
            }

            @Override
            public void describeTo(final Description description)
            {
                description.appendText(Either.right(value).toString());
            }
        };
    }

    private Matcher<Either<ActivityProvider.Error, Long>> left(final ActivityProvider.Error.Type error, final ActivityProvider provider)
    {
        return new TypeSafeMatcher<Either<ActivityProvider.Error, Long>>()
        {
            @Override
            protected boolean matchesSafely(final Either<ActivityProvider.Error, Long> item)
            {
                return item.isLeft() && item.left().get().getType() == error
                        && item.left().get().getActivityProvider().isDefined()
                        && item.left().get().getActivityProvider().get() == provider;
            }

            @Override
            public void describeTo(final Description description)
            {
                description.appendText(Either.left(error).toString());
                description.appendText(" <Expected provider:" + provider + ">");
            }
        };
    }

    private class TestActivityProviderErrorCallable implements ActivityProviderCallable<Either<ActivityProvider.Error, Long>>
    {
        private final ActivityProvider activityProvider;
        private final long sleepTime;
        private final ActivityProvider.Error error;

        public TestActivityProviderErrorCallable(final ActivityProvider activityProvider, final long sleepTime, final ActivityProvider.Error error) {
            this.activityProvider = activityProvider;
            this.sleepTime = sleepTime;
            this.error = error;
        }

        @Override
        public ActivityProvider getActivityProvider()
        {
            return activityProvider;
        }

        @Override
        public Either<ActivityProvider.Error, Long> call() throws Exception
        {
            Thread.sleep(sleepTime);
            return Either.left(error);
        }
    }

    private class TestActivityProviderCallable implements ActivityProviderCallable<Either<ActivityProvider.Error, Long>>
    {
        private final ActivityProvider activityProvider;
        private final long sleepTime;

        public TestActivityProviderCallable(final ActivityProvider activityProvider, final long sleepTime) {
            this.activityProvider = activityProvider;
            this.sleepTime = sleepTime;
        }

        @Override
        public ActivityProvider getActivityProvider()
        {
            return activityProvider;
        }

        @Override
        public Either<ActivityProvider.Error, Long> call() throws Exception
        {
            Thread.sleep(sleepTime);
            return Either.right(sleepTime);
        }
    }
}
