package com.atlassian.streams.internal;

import com.atlassian.streams.api.common.Either;
import com.google.common.base.Function;

import java.util.concurrent.Callable;

import static com.atlassian.streams.api.common.Either.getRights;
import static com.atlassian.streams.api.common.Either.right;
import static com.atlassian.streams.internal.ActivityProvider.Error;
import static com.atlassian.streams.internal.ActivityProviders.localOnly;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Iterables.contains;
import static com.google.common.collect.Iterables.transform;

public class ProjectKeyValidator
{
    private final ActivityProviders activityProviders;
    private final StreamsCompletionService completionService;

    ProjectKeyValidator(ActivityProviders activityProviders, StreamsCompletionService completionService)
    {
        this.activityProviders = checkNotNull(activityProviders, "activityProviders");
        this.completionService = checkNotNull(completionService, "completionService");
    }

    public boolean allKeysAreValid(Iterable<String> keys, boolean local)
    {
        Iterable<ActivityProvider> providers = activityProviders.get(localOnly(local), completionService.reachable());
        Iterable<ActivityProviderCallable<Either<Error, Boolean>>> callables = transform(providers, toKeysValidatorCallable(keys));
        //ignore any timeouts or other exceptions
        return contains(getRights(completionService.execute(callables)), true);
    }
    
    private Function<ActivityProvider, ActivityProviderCallable<Either<Error, Boolean>>> toKeysValidatorCallable(final Iterable<String> keys)
    {
        return new Function<ActivityProvider, ActivityProviderCallable<Either<Error, Boolean>>>()
        {
            public ActivityProviderCallable<Either<Error, Boolean>> apply(final ActivityProvider provider)
            {
                return new ActivityProviderCallable<Either<Error, Boolean>>()
                {
                    public Either<Error, Boolean> call() throws Exception
                    {
                        return right(provider.allKeysAreValid(keys));
                    }

                    @Override
                    public ActivityProvider getActivityProvider()
                    {
                        return provider;
                    }
                };
            }
        };
    };
}
