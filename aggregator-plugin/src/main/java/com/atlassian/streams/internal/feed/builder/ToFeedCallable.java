package com.atlassian.streams.internal.feed.builder;

import com.atlassian.streams.api.common.Either;
import com.atlassian.streams.api.common.Pair;
import com.atlassian.streams.api.common.uri.Uri;
import com.atlassian.streams.internal.ActivityProvider;
import com.atlassian.streams.internal.ActivityProviderCancellableTask;
import com.atlassian.streams.internal.ActivityRequestImpl;
import com.atlassian.streams.internal.HttpParameters;
import com.atlassian.streams.internal.NoMatchingRemoteKeysException;
import com.atlassian.streams.internal.feed.FeedModel;
import com.atlassian.streams.spi.CancellableTask;
import com.google.common.base.Function;
import com.google.common.base.Joiner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URI;

import static com.atlassian.streams.api.common.Either.left;
import static com.atlassian.streams.internal.ActivityProvider.Error.other;

public class ToFeedCallable implements Function<ActivityProvider, ActivityProviderCancellableTask<Either<ActivityProvider.Error, FeedModel>>> {
    private static final Logger log = LoggerFactory.getLogger(ToFeedCallable.class);
    private final Pair<Uri, HttpParameters> feedParameters;
    @SuppressWarnings("unused")
    private final String requestLanguages;
    private final URI contextUri;

    public ToFeedCallable(Pair<Uri, HttpParameters> feedParameters, URI contextUri, String requestLanguages) {
        this.feedParameters = feedParameters;
        this.contextUri = contextUri;
        this.requestLanguages = requestLanguages;
    }

    public ActivityProviderCancellableTask<Either<ActivityProvider.Error, FeedModel>> apply(final ActivityProvider provider) {
        ActivityRequestImpl.Builder builder = ActivityRequestImpl.builder(feedParameters.first()).contextUri(contextUri);
        // Set the Accept-Language header for only the remote (AppLinked) activity requests.
        // We don't want to override the request-language setting for the aggregating plugin.
        if (feedParameters.second().useAcceptLanguage()) {
            // STRM-1217: Disabled Accept-Language support (STRM-785) due to STRM-1125 and STRM-1126.
            // builder.requestLanguages(requestLanguages);
        }
        ActivityRequestImpl request = builder.build(feedParameters.second(), provider);
        final CancellableTask<Either<ActivityProvider.Error, FeedModel>> task = provider.getActivityFeed(request);

        return new ActivityProviderCancellableTask<Either<ActivityProvider.Error, FeedModel>>() {
            public Either<ActivityProvider.Error, FeedModel> call() {
                try {
                    return task.call();
                } catch (NoMatchingRemoteKeysException nmrke) {
                    log.info("No keys from " + provider.getName() + " matched " + Joiner.on(",").join(nmrke.getKeys()), nmrke);
                    // In this case, we don't want this to be added to the failing list.
                } catch (Exception e) {
                    log.error("Error fetching feed", e);
                    // Not sure on this one. We may get an error because of bad JQL for example, which shouldn't cause it to be added to failing list.
                }
                return left(other());
            }

            @Override
            public Result cancel() {
                return task.cancel();
            }

            @Override
            public ActivityProvider getActivityProvider() {
                return provider;
            }
        };
    }
}