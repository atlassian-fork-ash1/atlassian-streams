package com.atlassian.streams.internal;

import java.util.Set;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkService;
import com.atlassian.applinks.api.EntityLinkService;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.atlassian.streams.api.builder.StreamsFeedUriBuilderFactory;
import com.atlassian.streams.internal.applinks.ApplicationLinkServiceExtensions;
import com.atlassian.streams.internal.feed.FeedParser;
import com.atlassian.streams.spi.EntityResolver;
import com.atlassian.streams.spi.StreamsI18nResolver;
import com.atlassian.streams.spi.StreamsLocaleProvider;

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.base.Supplier;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Iterables.filter;
import static com.google.common.collect.Iterables.transform;

public class AppLinksActivityProviders implements Supplier<Iterable<ActivityProvider>>
{
    private final ApplicationLinkService appLinkService;
    private final EntityLinkService entityLinkService;
    private final Iterable<EntityResolver> entityResolvers;
    private final FeedParser feedParser;
    private final StreamsFeedUriBuilderFactory streamsFeedUriBuilderFactory;
    private final StreamsLocaleProvider streamsLocaleProvider;
    private final TransactionTemplate transactionTemplate;
    private final ApplicationLinkServiceExtensions appLinkServiceExtensions;
    private final StreamsI18nResolver i18nResolver;
    
    AppLinksActivityProviders(ApplicationLinkService appLinkService,
            EntityLinkService entityLinkService,
            Set<EntityResolver> entityResolvers,
            FeedParser feedParser,
            StreamsFeedUriBuilderFactory streamsFeedUriBuilderFactory,
            StreamsLocaleProvider streamsLocaleProvider,
            TransactionTemplate transactionTemplate,
            ApplicationLinkServiceExtensions appLinkServiceExtensions,
            StreamsI18nResolver i18nResolver)
    {
        this.appLinkService = checkNotNull(appLinkService, "appLinkService");
        this.entityLinkService = checkNotNull(entityLinkService, "entityLinkService");
        this.entityResolvers = checkNotNull(entityResolvers, "entityResolvers");
        this.feedParser = checkNotNull(feedParser, "feedParser");
        this.streamsFeedUriBuilderFactory = checkNotNull(streamsFeedUriBuilderFactory, "streamsFeedUriBuilderFactory");
        this.streamsLocaleProvider = checkNotNull(streamsLocaleProvider, "streamsLocaleProvider");
        this.transactionTemplate = checkNotNull(transactionTemplate, "transactionTemplate");
        this.appLinkServiceExtensions = checkNotNull(appLinkServiceExtensions, "appLinkServiceExtensions");
        this.i18nResolver = checkNotNull(i18nResolver, "i18nResolver");
    }

    public Iterable<ActivityProvider> get()
    {
        return transform(filter(appLinkService.getApplicationLinks(), removeOptOuts()), toAppLinkProvider());
    }

    static Predicate<? super ApplicationLink> removeOptOuts()
    {
        return new Predicate<ApplicationLink>()
        {
            @Override
            public boolean apply(ApplicationLink link)
            {
                String value = (String) link.getProperty("IS_ACTIVITY_ITEM_PROVIDER");
                return value == null || Boolean.parseBoolean(value);
            }
        };
    }

    private Function<ApplicationLink, ActivityProvider> toAppLinkProvider()
    {
        return new Function<ApplicationLink, ActivityProvider>()
        {
            public ActivityProvider apply(ApplicationLink appLink)
            {
                return new AppLinksActivityProvider(appLink, entityLinkService, entityResolvers, feedParser, streamsFeedUriBuilderFactory, streamsLocaleProvider, transactionTemplate, appLinkServiceExtensions, i18nResolver);
            }
        };
    }
}
