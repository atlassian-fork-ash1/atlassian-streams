package com.atlassian.streams.internal.completion;

import com.atlassian.streams.api.common.Either;
import com.atlassian.streams.internal.ActivityProvider;
import com.atlassian.streams.internal.ActivityProviderCallable;
import com.google.common.base.Function;
import com.google.common.base.Supplier;
import io.atlassian.util.concurrent.Timeout;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import static com.atlassian.streams.api.common.Either.left;
import static com.atlassian.streams.internal.ActivityProvider.Error.other;
import static com.atlassian.streams.internal.ActivityProvider.Error.timeout;
import static com.google.common.base.Suppliers.memoize;
import static com.google.common.collect.ImmutableList.copyOf;
import static com.google.common.collect.Iterables.transform;
import static io.atlassian.util.concurrent.Timeout.getNanosTimeout;
import static java.util.Objects.requireNonNull;

public class Completer {
    private static final Logger logger = LoggerFactory.getLogger(Completer.class);
    private final Executor executor;
    private final ExecutorCompletionServiceFactory completionServiceFactory;

    public Completer(final Executor executor, final ExecutorCompletionServiceFactory completionServiceFactory) {
        this.executor = requireNonNull(executor, "executor");
        this.completionServiceFactory = requireNonNull(completionServiceFactory, "completionServiceFactory");
    }

    public <T> Iterable<Either<ActivityProvider.Error, T>> invokeAll(final Iterable<? extends ActivityProviderCallable<Either<ActivityProvider.Error, T>>> callables, final long time, final TimeUnit unit) {
        return invokeAll(callables, getNanosTimeout(time, unit));
    }

    public <T> Iterable<Either<ActivityProvider.Error, T>> invokeAll(Iterable<? extends ActivityProviderCallable<Either<ActivityProvider.Error, T>>> jobs) {
        return invokeAll(jobs, null);
    }

    private <T> Iterable<Either<ActivityProvider.Error, T>> invokeAll(Iterable<? extends ActivityProviderCallable<Either<ActivityProvider.Error, T>>> callables, @Nullable Timeout nanosTimeout) {
        // we must copy the resulting Iterable<Supplier> so
        // each iteration doesn't resubmit the jobs
        final Iterable<Supplier<Either<ActivityProvider.Error, T>>> lazyAsyncSuppliers = copyOf(transform(callables, new CompletionFunction<T>(completionServiceFactory.<T>create().apply(executor), nanosTimeout)));
        return transform(lazyAsyncSuppliers, Completer.<Either<ActivityProvider.Error, T>>fromSupplier());
    }

    /**
     * Extension point if a custom CompletionService is required
     */
    public interface ExecutorCompletionServiceFactory {
        <T> Function<Executor, CompletionService<Either<ActivityProvider.Error, T>>> create();
    }

    private static class CompletionFunction<T>
            implements Function<ActivityProviderCallable<Either<ActivityProvider.Error, T>>, Supplier<Either<ActivityProvider.Error, T>>> {
        private final CompletionService<Either<ActivityProvider.Error, T>> completionService;

        private Timeout nanosTimeout;

        CompletionFunction(final CompletionService<Either<ActivityProvider.Error, T>> completionService, @Nullable Timeout nanosTimeout) {
            this.completionService = completionService;
            this.nanosTimeout = nanosTimeout;
        }

        public Supplier<Either<ActivityProvider.Error, T>> apply(final ActivityProviderCallable<Either<ActivityProvider.Error, T>> task) {
            final Future<Either<ActivityProvider.Error, T>> future = completionService.submit(task);
            return memoize(new Supplier<Either<ActivityProvider.Error, T>>() {
                @Override
                public Either<ActivityProvider.Error, T> get() {

                    try {
                        if (nanosTimeout == null) {
                            return future.get();
                        } else {
                            return future.get(nanosTimeout.getTime(), nanosTimeout.getUnit());
                        }
                    } catch (InterruptedException e) {
                        logger.debug("Handling a non-timeout exception", e);
                        return left(timeout(task.getActivityProvider()));
                    } catch (ExecutionException e) {
                        logger.debug("Handling a non-timeout exception", e);
                        return left(other(task.getActivityProvider()));
                    } catch (TimeoutException e) {
                        logger.debug("Handling a timeout", e.getMessage());
                        //This is a special case were we timed out waiting on task but it is still running
                        // so we have to cancel it
                        future.cancel(true);
                        return left(timeout(task.getActivityProvider()));
                    }
                }
            });
        }
    }

    static <T> Function<Supplier<? extends T>, T> fromSupplier() {
        return new Function<Supplier<? extends T>, T>() {
            public T apply(final Supplier<? extends T> supplier) {
                return supplier.get();
            }
        };
    }
}