package com.atlassian.streams.internal.rest.representations;

import java.util.Collection;

import com.atlassian.streams.spi.StreamsKeyProvider.StreamsKey;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Collections2.transform;

public class StreamsKeysRepresentation
{
    @JsonProperty private final Collection<StreamsKeyEntry> keys;

    @JsonCreator
    public StreamsKeysRepresentation(@JsonProperty("keys") Collection<StreamsKeyEntry> keys)
    {
        this.keys = ImmutableList.copyOf(keys);
    }

    public StreamsKeysRepresentation(Iterable<StreamsKey> streamsKeys)
    {
        this.keys = transform(ImmutableList.copyOf(streamsKeys), toValueMapEnties);
    }

    public Collection<StreamsKeyEntry> getKeys()
    {
        return keys;
    }

    public static class StreamsKeyEntry
    {
        @JsonProperty private final String key;
        @JsonProperty private final String label;

        @JsonCreator
        public StreamsKeyEntry(@JsonProperty("key") String key,
                @JsonProperty("label") String label)
        {
            this.key = checkNotNull(key, "Streams key entry is null for label: " + label);
            this.label = checkNotNull(label, "Label is null for key: " + key);
        }

        public String getKey()
        {
            return key;
        }

        public String getLabel()
        {
            return label;
        }
    }

    private static final Function<StreamsKey, StreamsKeyEntry> toValueMapEnties = new Function<StreamsKey, StreamsKeyEntry>()
    {
        public StreamsKeyEntry apply(StreamsKey streamsKey)
        {
            return new StreamsKeyEntry(streamsKey.getKey(), streamsKey.getLabel());
        }
    };
}
