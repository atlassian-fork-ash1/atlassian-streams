package com.atlassian.streams.internal.atom.abdera;

import org.apache.abdera.util.AbstractExtensionFactory;

import static com.atlassian.streams.internal.atom.abdera.AtomConstants.ACTIVITY_NS;
import static com.atlassian.streams.internal.atom.abdera.AtomConstants.ACTIVITY_OBJECT;
import static com.atlassian.streams.internal.atom.abdera.AtomConstants.ACTIVITY_TARGET;
import static com.atlassian.streams.internal.atom.abdera.AtomConstants.ATLASSIAN_NS;
import static com.atlassian.streams.internal.atom.abdera.AtomConstants.ATLASSIAN_AUTHORISATION_MESSAGE;

public class ActivityStreamsExtensionFactory extends AbstractExtensionFactory
{
    public ActivityStreamsExtensionFactory()
    {
        super(ACTIVITY_NS, ATLASSIAN_NS);

        addImpl(ACTIVITY_OBJECT, ActivityObject.class);
        addImpl(ACTIVITY_TARGET, ActivityObject.class);
        addImpl(ATLASSIAN_AUTHORISATION_MESSAGE, AuthorisationMessage.class);
    }
}
