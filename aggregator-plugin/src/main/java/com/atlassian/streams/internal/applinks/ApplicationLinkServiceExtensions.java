package com.atlassian.streams.internal.applinks;

import java.net.URI;

import com.atlassian.applinks.api.ApplicationLink;

/**
 * This interface contains methods needed to support the revised OAuth Homogenous UX spec, which
 * are currently being implemented in Streams but will eventually be implemented in the UAL plugin.
 * The implementations are likely to be entirely different, since Streams does not have access to
 * UAL internals that will make these tasks easier.
 */
public interface ApplicationLinkServiceExtensions
{
    /**
     * Returns true if credentials are configured for this applink.
     */
    boolean isAuthorised(ApplicationLink appLink);
    
    /**
     * Returns the URI for an administrative page within the application that will let
     * the user change or revoke access to this applink.  For OAuth links, this is the
     * OAuth Access Tokens page.  Note that such a URI may not be available for all
     * applinks or all applications, in which case it will return null.
     * @return a URI or null
     */
    URI getUserAdminUri(ApplicationLink appLink);
    
    /**
     * Returns the URI of a callback servlet that will handle post-processing of
     * completed authorisation requests.
     */
    URI getAuthCallbackUri(ApplicationLink appLink);
}
