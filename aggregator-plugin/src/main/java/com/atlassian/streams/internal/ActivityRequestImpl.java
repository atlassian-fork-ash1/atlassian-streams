package com.atlassian.streams.internal;

import com.atlassian.streams.api.ActivityRequest;
import com.atlassian.streams.api.StreamsFilterType;
import com.atlassian.streams.api.StreamsFilterType.Operator;
import com.atlassian.streams.api.common.Option;
import com.atlassian.streams.api.common.Pair;
import com.atlassian.streams.api.common.uri.Uri;
import com.google.common.base.Function;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Multimap;
import com.google.common.collect.Multimaps;

import java.net.URI;

import static com.google.common.collect.Iterables.concat;
import static com.google.common.collect.Iterables.transform;

public class ActivityRequestImpl implements ActivityRequest
{
    public static final int DEFAULT_MAX_RESULTS = 10;
    public static final int DEFAULT_TIMEOUT = 10000;

    private final Uri uri;
    private final URI contextUri;
    private final Option<String> key;
    private final Iterable<String> providers;
    private final Multimap<String, Pair<StreamsFilterType.Operator, Iterable<String>>> standardFilters;
    private final Multimap<String, Pair<StreamsFilterType.Operator, Iterable<String>>> providerFilters;
    private final String requestLanguages;

    private final int maxResults;
    private final int timeout;

    public ActivityRequestImpl(Builder builder)
    {
        this.uri = builder.uri;
        this.contextUri = builder.contextUri;
        this.key = builder.key;
        this.providers = builder.providers;
        this.standardFilters = Multimaps.unmodifiableMultimap(ArrayListMultimap.create(builder.standardFilters));
        this.providerFilters = Multimaps.unmodifiableMultimap(ArrayListMultimap.create(builder.providerFilters));
        this.requestLanguages = builder.requestLanguages;

        this.maxResults = builder.maxResults;
        this.timeout = builder.timeout;
    }

    public Uri getUri()
    {
        return uri;
    }

    @Override
    public URI getContextUri()
    {
        return contextUri;
    }

    Option<String> getKey()
    {
        return key;
    }

    Iterable<String> getProviders()
    {
        return providers;
    }

    public int getMaxResults()
    {
        return maxResults;
    }

    public int getTimeout()
    {
        return timeout;
    }

    public String getRequestLanguages()
    {
        return requestLanguages;
    }

    public Multimap<String, Pair<Operator, Iterable<String>>> getProviderFilters()
    {
        return providerFilters;
    }

    public Multimap<String, Pair<Operator, Iterable<String>>> getStandardFilters()
    {
        return standardFilters;
    }

    public static Builder builder(Uri uri)
    {
        return new Builder(uri);
    }

    public static final class Builder
    {
        private final Uri uri;
        public URI contextUri;
        private Option<String> key;
        private Iterable<String> providers = ImmutableList.of();
        private Multimap<String, Pair<Operator, Iterable<String>>> standardFilters = ArrayListMultimap.create();
        private Multimap<String, Pair<Operator, Iterable<String>>> providerFilters = ArrayListMultimap.create();
        private String requestLanguages;

        private int maxResults = DEFAULT_MAX_RESULTS;
        private int timeout = DEFAULT_TIMEOUT;

        private Builder(Uri uri)
        {
            this.uri = uri;
        }

        public Builder maxResults(int maxResults)
        {
            if (maxResults > 0)
            {
                this.maxResults = maxResults;
            }
            return this;
        }

        public Builder contextUri(URI contextUri)
        {
            this.contextUri = contextUri;
            return this;
        }

        public Builder timeout(int timeout)
        {
            if (timeout > 0)
            {
                this.timeout = timeout;
            }
            return this;
        }

        public Builder standardFilters(Multimap<String, Pair<Operator, Iterable<String>>> filters)
        {
            this.standardFilters = filters;
            return this;
        }

        public Builder providerFilters(Multimap<String, Pair<Operator, Iterable<String>>> filters)
        {
            this.providerFilters = filters;
            return this;
        }

        public Builder requestLanguages(String requestLanguages)
        {
            this.requestLanguages = requestLanguages;
            return this;
        }

        public ActivityRequest build()
        {
            return new ActivityRequestImpl(this);
        }

        public ActivityRequestImpl build(HttpParameters parameters, ActivityProvider provider)
        {
            this.maxResults = parameters.parseMaxResults(DEFAULT_MAX_RESULTS);
            this.standardFilters = parameters.parseStandardFilters();
            this.key = parameters.getProviderKey(provider);
            this.providerFilters = parameters.getProviderFilter(this.providerFilters, provider);

            this.providers  = concat(transform(parameters.getProviders(), splitOnSpace));

            return new ActivityRequestImpl(this);
        }
    }

    private static final Function<String, Iterable<String>> splitOnSpace = new Function<String, Iterable<String>>()
    {
        public Iterable<String> apply(String delimitedProviders)
        {
            ImmutableList.Builder<String> builder = ImmutableList.<String>builder();

            //delimitedProviders is in the form of "builds@Bamboo reviews@Fisheye source@Fisheye"
            Iterable<String> providers = ImmutableList.copyOf(delimitedProviders.split(" "));
            for (String provider : providers)
            {
                builder.add(provider);
            }

            return builder.build();
        }
    };

}
