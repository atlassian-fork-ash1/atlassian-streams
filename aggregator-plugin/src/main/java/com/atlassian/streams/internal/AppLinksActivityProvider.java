package com.atlassian.streams.internal;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkRequest;
import com.atlassian.applinks.api.ApplicationLinkRequestFactory;
import com.atlassian.applinks.api.ApplicationLinkResponseHandler;
import com.atlassian.applinks.api.CredentialsRequiredException;
import com.atlassian.applinks.api.EntityLink;
import com.atlassian.applinks.api.EntityLinkService;
import com.atlassian.applinks.api.auth.Anonymous;
import com.atlassian.sal.api.net.Request;
import com.atlassian.sal.api.net.Request.MethodType;
import com.atlassian.sal.api.net.Response;
import com.atlassian.sal.api.net.ResponseException;
import com.atlassian.sal.api.transaction.TransactionCallback;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.atlassian.streams.api.StreamsException;
import com.atlassian.streams.api.StreamsFilterType.Operator;
import com.atlassian.streams.api.builder.StreamsFeedUriBuilder;
import com.atlassian.streams.api.builder.StreamsFeedUriBuilderFactory;
import com.atlassian.streams.api.common.Either;
import com.atlassian.streams.api.common.Option;
import com.atlassian.streams.api.common.Pair;
import com.atlassian.streams.api.common.uri.Uri;
import com.atlassian.streams.api.common.uri.Uris;
import com.atlassian.streams.internal.applinks.ApplicationLinkServiceExtensions;
import com.atlassian.streams.internal.feed.AuthRequiredFeedHeader;
import com.atlassian.streams.internal.feed.FeedHeader;
import com.atlassian.streams.internal.feed.FeedModel;
import com.atlassian.streams.internal.feed.FeedParser;
import com.atlassian.streams.internal.rest.MediaTypes;
import com.atlassian.streams.internal.rest.representations.JsonProvider;
import com.atlassian.streams.internal.rest.representations.ProviderFilterRepresentation;
import com.atlassian.streams.internal.rest.representations.StreamsConfigRepresentation;
import com.atlassian.streams.internal.rest.representations.StreamsKeysRepresentation;
import com.atlassian.streams.internal.rest.representations.StreamsKeysRepresentation.StreamsKeyEntry;
import com.atlassian.streams.spi.CancellableTask;
import com.atlassian.streams.spi.EntityResolver;
import com.atlassian.streams.spi.StreamsI18nResolver;
import com.atlassian.streams.spi.StreamsLocaleProvider;
import com.google.common.base.Function;
import com.google.common.base.Joiner;
import com.google.common.base.Predicate;
import com.google.common.base.Supplier;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.lang.annotation.Annotation;
import java.net.SocketTimeoutException;
import java.net.URI;
import java.text.ParseException;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import static com.atlassian.streams.api.StreamsFilterType.Operator.IS;
import static com.atlassian.streams.api.StreamsFilterType.Operator.NOT;
import static com.atlassian.streams.api.common.Either.left;
import static com.atlassian.streams.api.common.Either.right;
import static com.atlassian.streams.api.common.Iterables.revMap;
import static com.atlassian.streams.api.common.Option.option;
import static com.atlassian.streams.api.common.Option.some;
import static com.atlassian.streams.api.common.Options.catOptions;
import static com.atlassian.streams.internal.ActivityProvider.Error.credentialsRequired;
import static com.atlassian.streams.internal.ActivityProvider.Error.other;
import static com.atlassian.streams.internal.ActivityProvider.Error.timeout;
import static com.atlassian.streams.internal.ActivityProvider.Error.unauthorized;
import static com.atlassian.streams.spi.Filters.getAllValues;
import static com.atlassian.streams.spi.Filters.getIsValues;
import static com.atlassian.streams.spi.StandardStreamsFilterOption.PROJECT_KEY;
import static com.atlassian.streams.spi.StandardStreamsFilterOption.STANDARD_FILTERS_PROVIDER_KEY;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Predicates.not;
import static com.google.common.collect.Iterables.concat;
import static com.google.common.collect.Iterables.filter;
import static com.google.common.collect.Iterables.isEmpty;
import static com.google.common.collect.Iterables.transform;
import static java.util.Collections.emptySet;
import static javax.ws.rs.core.HttpHeaders.CONTENT_TYPE;
import static javax.ws.rs.core.Response.Status.OK;
import static org.apache.commons.io.IOUtils.copy;

public class AppLinksActivityProvider implements ActivityProvider
{
    public static final String PROVIDER_KEY_SEPARATOR = "@";

    private static final Logger log = LoggerFactory.getLogger(AppLinksActivityProvider.class);
    private static final int CONNECTION_TIMEOUT = 1000;
    private static final int SO_TIMEOUT = 5000;

    private final ApplicationLink appLink;
    private final EntityLinkService entityLinkService;
    private final Iterable<EntityResolver> entityResolvers;
    private final StreamsFeedUriBuilderFactory streamsFeedUriBuilderFactory;
    private final JsonProvider jsonProvider = new JsonProvider();
    private final StreamsLocaleProvider streamsLocaleProvider;
    private final FeedParser feedParser;
    private final ApplicationLinkServiceExtensions appLinkServiceExtensions;
    private final TransactionTemplate transactionTemplate;
    private final StreamsI18nResolver i18nResolver;

    public AppLinksActivityProvider(ApplicationLink appLink,
            EntityLinkService entityLinkService,
            Iterable<EntityResolver> entityResolvers,
            FeedParser feedParser,
            StreamsFeedUriBuilderFactory streamsFeedUriBuilderFactory,
            StreamsLocaleProvider streamsLocaleProvider,
            TransactionTemplate transactionTemplate,
            ApplicationLinkServiceExtensions appLinkServiceExtensions,
            StreamsI18nResolver i18nResolver)
    {
        this.appLink = checkNotNull(appLink, "appLink");
        this.entityLinkService = checkNotNull(entityLinkService, "entityLinkService");
        this.entityResolvers = checkNotNull(entityResolvers, "entityResolvers");
        this.feedParser = checkNotNull(feedParser, "feedParser");
        this.streamsFeedUriBuilderFactory = checkNotNull(streamsFeedUriBuilderFactory, "streamsFeedUriBuilderFactory");
        this.streamsLocaleProvider = checkNotNull(streamsLocaleProvider, "streamsLocaleProvider");
        this.transactionTemplate = checkNotNull(transactionTemplate, "transactionTemplate");
        this.appLinkServiceExtensions = checkNotNull(appLinkServiceExtensions, "appLinkServiceExtensions");
        this.i18nResolver = checkNotNull(i18nResolver, "i18nResolver");
    }

    /**
     * Matches the key if it ends with "@KEY" where KEY is the value returned from {@link #getKey()}
     */
    @Override
    public boolean matches(String key)
    {
        return key.endsWith(PROVIDER_KEY_SEPARATOR + getKey());
    }

    @Override
    public String getName()
    {
        return appLink.getName();
    }

    public String getKey()
    {
        //must use applink id instead of name as UAL name can change and is non-unique
        return appLink.getId().get();
    }

    public String getBaseUrl()
    {
        return appLink.getDisplayUrl().toString();
    }

    public String getType()
    {
        return i18nResolver.getText(appLink.getType().getI18nKey());
    }

    public CancellableTask<Either<Error, FeedModel>> getActivityFeed(final ActivityRequestImpl request) throws StreamsException
    {
        final ActivityProvider activityProvider = this;
        return new CancellableTask<Either<Error,FeedModel>>()
        {
            @Override
            public Either<Error, FeedModel> call() {
                URI uri = getBuilder(request).getServletUri();
                Function<Option<CredentialsRequiredException>, Either<Error, FeedModel>> retryHandler =
                    new RetryFeedAsAnonymousIfCredentialsRequiredHandler(uri, request.getTimeout(), activityProvider);

                return fetch(defaultAuthenticatedRequestFactory,
                        uri.toASCIIString(),
                        request.getTimeout(),
                        new FeedResponseHandler(uri, retryHandler),
                        retryHandler);
            }

            @Override
            public Result cancel()
            {
                return Result.INTERRUPT;
            }
        };
    }

    private Either<Error, FeedModel> addAuthRequestToFeed(Either<Error, FeedModel> result, URI feedUri)
    {
        URI callbackUri = appLinkServiceExtensions.getAuthCallbackUri(appLink);
        URI authUri = appLink.createAuthenticatedRequestFactory().getAuthorisationURI(callbackUri);

        FeedHeader header = new AuthRequiredFeedHeader(appLink.getId().toString(),
                                                       appLink.getName(),
                                                       appLink.getDisplayUrl(),
                                                       authUri);

        FeedModel.Builder newFeed;
        if (result.isLeft())
        {
            // If what we got was just a credentials-required error, then we can return a feed that
            // contains nothing but a warning.  But if it was any other kind of error, we should
            // just return the error.
            if (result.left().get().getType() == Error.Type.CREDENTIALS_REQUIRED)
            {
                newFeed = new FeedModel.Builder(Uri.fromJavaUri(feedUri));
            }
            else
            {
                return result;
            }
        }
        else
        {
            newFeed = new FeedModel.Builder(result.right().get());
        }
        return right(newFeed.addHeaders(ImmutableList.of(header)).build());
    }

    public Either<Error, Iterable<ProviderFilterRepresentation>> getFilters(boolean addApplinkName)
    {
        String url = "/rest/activity-stream/1.0/config?local=true";
        return fetch(anonymousRequestFactory,
                url,
                SO_TIMEOUT,
                new GetFiltersHandler(url, this, addApplinkName),
                new ReturnErrorIfCredentialsRequiredHandler<Iterable<ProviderFilterRepresentation>>(this));
    }

    public StreamsKeysRepresentation getKeys()
    {
        return new StreamsKeysRepresentation(ImmutableList.<StreamsKeyEntry>of());
    }

    @Override
    public boolean allKeysAreValid(Iterable<String> keys)
    {
        // title and numofentries parameters don't matter.  they just need to satisfy the conditions in StreamsValidationResource
        String url = "/rest/activity-stream/1.0/validate?local=true&keys=" +
                     Uris.encode(Joiner.on(',').join(keys)) + "&title=test&numofentries=1";
        return fetch(anonymousRequestFactory,
                     url,
                     SO_TIMEOUT,
                     new KeyValidationHandler(),
                     new ReturnErrorIfCredentialsRequiredHandler<Boolean>(this))
                     .right().toOption().getOrElse(false);
    }

    public Request<?, Response> createRequest(String url, MethodType methodType) throws CredentialsRequiredException
    {
        Request<?, Response> request = defaultAuthenticatedRequestFactory.get().createRequest(methodType, url);
        request.setConnectionTimeout(CONNECTION_TIMEOUT);
        return request;
    }

    public Request<?, Response> createAnonymousRequest(String url, MethodType methodType) throws CredentialsRequiredException
    {
        Request<?, Response> request = anonymousRequestFactory.get().createRequest(methodType, url);
        request.setConnectionTimeout(CONNECTION_TIMEOUT);
        return request;
    }

    private StreamsFeedUriBuilder getBuilder(ActivityRequestImpl request)
    {
        // If we have keys, we get a builder with corresponding remote keys, otherwise, we get the app instances base builder
        StreamsFeedUriBuilder builder = createUriBuilder(request);

        final Iterable<String> localIsKeys = getIsValues(request.getStandardFilters().get(PROJECT_KEY));
        final Iterable<String> remoteIsKeys = concat(transform(localIsKeys, toRemoteKey));

        // STRM-597: If no remote keys match any of the 'is' values, then we should not do the request,
        // because if we do it'll return all content
        if (isEmpty(remoteIsKeys) && !isEmpty(localIsKeys))
        {
            throw new NoMatchingRemoteKeysException(localIsKeys);
        }
        else if (!isEmpty(remoteIsKeys))
        {
            builder.addStandardFilter(PROJECT_KEY, IS, remoteIsKeys);
        }

        final Iterable<String> localNotKeys = getAllValues(NOT, request.getStandardFilters().get(PROJECT_KEY));
        final Iterable<String> remoteNotKeys = concat(transform(localNotKeys, toRemoteKey));

        if (!isEmpty(remoteNotKeys))
        {
            builder.addStandardFilter(PROJECT_KEY, NOT, remoteNotKeys);
        }
        // STRM-2339 : ask the server to return an empty set if we are not authorized there
        builder.addAuthOnly(true);

        return builder;
    }

    private final Function<String, Iterable<String>> toRemoteKey = new Function<String, Iterable<String>>()
    {
        @Override
        public Iterable<String> apply(String key)
        {
            ImmutableSet.Builder<String> result = ImmutableSet.<String>builder();
            for (EntityLink entityLink: getEntityLinks(key))
            {
                if (entityLink.getApplicationLink().equals((appLink)))
                {
                    result.add(entityLink.getKey());
                }
            }
            return result.build();
        }
    };

    private Iterable<EntityLink> getEntityLinks(String key)
    {
        return concat(transform(catOptions(revMap(entityResolvers, key)), getEntityLinks()));
    }

    private Function<Object, Iterable<EntityLink>> getEntityLinks = new Function<Object, Iterable<EntityLink>>()
    {
        //must wrap in a transaction as Bamboo complains about looking up projects while not in a session
        public Iterable<EntityLink> apply(final Object entity)
        {
            return transactionTemplate.execute(new TransactionCallback<Iterable<EntityLink>>()
            {
                public Iterable<EntityLink> doInTransaction()
                {
                    return entityLinkService.getEntityLinks(entity);
                }
            });
        }
    };

    private Function<Object, Iterable<EntityLink>> getEntityLinks()
    {
        return getEntityLinks;
    }

    private StreamsFeedUriBuilder createUriBuilder(final ActivityRequestImpl request)
    {
        final StreamsFeedUriBuilder builder = streamsFeedUriBuilderFactory.getStreamsFeedUriBuilder("");

        // Transfer standard filters, special casing some
        for (Map.Entry<String, Pair<Operator, Iterable<String>>> param : request.getStandardFilters().entries())
        {
            final String name = param.getKey();
            if (name.equals(PROJECT_KEY))
            {
                // Ignore key params
            }
            else
            {
                // Transfer everything else
                builder.addStandardFilter(name, param.getValue());
            }
        }

        for (Map.Entry<String, Pair<Operator, Iterable<String>>> param : request.getProviderFilters().entries())
        {
            // the key will be something like {remote-key}@{applinks-name}
            for (String key : request.getKey())
            {
                builder.addProviderFilter(stripAppLinkName(key), param.getKey(), param.getValue());
            }
        }

        //each provider is in the form of "builds@Bamboo", "reviews@Fisheye", and "source@Fisheye"
        for (String provider : filter(request.getProviders(), ActivityProviders.matches(this)))
        {
            builder.addProvider(stripAppLinkName(provider));
        }

        builder.addLocalOnly(true);
        builder.addUseAcceptLang(true);
        builder.setMaxResults(request.getMaxResults());

        if (request.getTimeout() != ActivityRequestImpl.DEFAULT_TIMEOUT)
        {
            builder.setTimeout(request.getTimeout());
        }

        return builder;
    }

    private String stripAppLinkName(String key)
    {
        return key.substring(0, key.lastIndexOf(PROVIDER_KEY_SEPARATOR));
    }

    /**
     * @return a description of this ApplicationLink to use in logging
     */
    private String applinkDescription()
    {
        return appLink.getName();
    }

    private <T> Either<Error, T> fetch(
            Supplier<ApplicationLinkRequestFactory> requestFactorySupplier,
            String url,
            int timeout,
            ApplicationLinkResponseHandler<Either<Error, T>> response,
            Function<Option<CredentialsRequiredException>,
            Either<Error, T>> credentialsRequiredHandler)
    {
        ApplicationLinkRequestFactory requestFactory = requestFactorySupplier.get();

        try
        {
            final ApplicationLinkRequest request = requestFactory.createRequest(Request.MethodType.GET, url);
            request.setConnectionTimeout(CONNECTION_TIMEOUT);
            if (Sys.inDevMode())
            {
                request.setSoTimeout(Integer.MAX_VALUE);
            }
            else
            {
                //a socket timeout is set by default, so let's set it to slightly more than the request's timeout (to allow
                //the request to timeout via its implementation before this timeout occurs)
                request.setSoTimeout(timeout + SO_TIMEOUT);
            }
            request.setHeader(LocalActivityProvider.ACCEPT_LANGUAGE_KEY, acceptLanguageString());
            return request.execute(response);
        }
        catch (final ApplinkResponseException e)
        {
            if (e.getStatusCode() == 401)
            {
                if (errorMessageContainsCaseInsensitive(e.getMessage(), "full authentication"))
                {
                    return left(credentialsRequired(this));
                }
                else
                {
                    return left(unauthorized(this));
                }
            }
            return handleResponseException(url, e);
        }
        catch (ResponseException e)
        {
            return handleResponseException(url, e);
        }
        catch (CredentialsRequiredException e)
        {
            return credentialsRequiredHandler.apply(some(e));
        }
    }

    private <T> Either<Error, T> handleResponseException(final String url, final ResponseException e) {
        if (e.getCause() instanceof SocketTimeoutException)
        {
            log.debug("Timed out fetching feed for '" + applinkDescription() + "' from " + url, e);
            return left(timeout(this));
        }
        // Bamboo can return a ResponseException with text "com.atlassian.sal.api.net.ResponseException: Error 401: 'Unauthorized' received when retrieving activity"
        // searching the message text is annoying, but that's all we have from older SALs
        else if (errorMessageContainsCaseInsensitive(e.getMessage(), "401", "full authentication"))
        {
            return left(credentialsRequired(this));
        }
        else
        {
            log.info("Error fetching feed for '" + applinkDescription() + "' from " + url + ": " +  e.getMessage());
            return left(other(this));
        }
    }

    private boolean errorMessageContainsCaseInsensitive(@Nullable final String errorMessage, @Nonnull String... tokens)
    {
        if (errorMessage == null)
        {
            return false;
        }
        final String lowerCase = errorMessage.toLowerCase();
        for (final String token : tokens)
        {
            if (!lowerCase.contains(token.toLowerCase()))
            {
                return false;
            }
        }
        return true;
    }

    private Supplier<ApplicationLinkRequestFactory> defaultAuthenticatedRequestFactory = new Supplier<ApplicationLinkRequestFactory>() {

        @Override
        public ApplicationLinkRequestFactory get()
        {
            return appLink.createAuthenticatedRequestFactory();
        }
    };

    private Supplier<ApplicationLinkRequestFactory> anonymousRequestFactory = new Supplier<ApplicationLinkRequestFactory>() {

        @Override
        public ApplicationLinkRequestFactory get()
        {
            return appLink.createAuthenticatedRequestFactory(Anonymous.class);
        }
    };

    private class ReturnErrorIfCredentialsRequiredHandler<T> implements Function<Option<CredentialsRequiredException>, Either<Error, T>>
    {
        private final ActivityProvider activityProvider;

        private ReturnErrorIfCredentialsRequiredHandler(final ActivityProvider activityProvider) {
            this.activityProvider = activityProvider;
        }

        @Override
        public Either<Error, T> apply(Option<CredentialsRequiredException> e)
        {
            return left(credentialsRequired(activityProvider, e));
        }
    }

    private class RetryFeedAsAnonymousIfCredentialsRequiredHandler implements Function<Option<CredentialsRequiredException>, Either<Error, FeedModel>>
    {
        private final URI feedUri;
        private final int timeout;
        private final ActivityProvider activityProvider;

        public RetryFeedAsAnonymousIfCredentialsRequiredHandler(URI feedUri, int timeout, final ActivityProvider activityProvider)
        {
            this.feedUri = feedUri;
            this.timeout = timeout;
            this.activityProvider = activityProvider;
        }

        @Override
        public Either<Error, FeedModel> apply(Option<CredentialsRequiredException> e)
        {
            Function<Option<CredentialsRequiredException>, Either<Error, FeedModel>> noRetryHandler =
                new ReturnErrorIfCredentialsRequiredHandler<FeedModel>(activityProvider);
            return addAuthRequestToFeed(fetch(anonymousRequestFactory,
                                              feedUri.toASCIIString(),
                                              timeout,
                                              new FeedResponseHandler(feedUri, noRetryHandler),
                                              noRetryHandler),
                                        feedUri);
        }
    }

    private String acceptLanguageString()
    {
        final Locale userLocale = streamsLocaleProvider.getUserLocale();
        final Locale appLocale = streamsLocaleProvider.getApplicationLocale();
        checkNotNull(userLocale, "userLocale");
        checkNotNull(appLocale, "appLocale");
        return String.format(
                "%s, %s;q=0.9, %s;q=0.8, %s;q=0.7, *;q=0.5",
                toLanguageTag(userLocale),
                userLocale.getLanguage(),
                toLanguageTag(appLocale),
                appLocale.getLanguage());
    }

    private String toLanguageTag(Locale locale)
    {
        return locale.toString().toLowerCase().replace("_", "-");
    }

    private final class FeedResponseHandler implements ApplicationLinkResponseHandler<Either<Error, FeedModel>>
    {
        private final URI uri;
        private final Function<Option<CredentialsRequiredException>, Either<Error, FeedModel>> retryHandler;

        FeedResponseHandler(URI uri,
                            Function<Option<CredentialsRequiredException>, Either<Error, FeedModel>> retryHandler)
        {
            this.uri = uri;
            this.retryHandler = retryHandler;
        }

        public Either<Error, FeedModel> handle(final Response response) throws ResponseException
        {
            if (!response.isSuccessful())
            {
                throw new ApplinkResponseException(
                    "Error " + response.getStatusCode() + ": '" + response.getStatusText() +
                    "' received when retrieving activity from '" + appLink.getRpcUrl() + uri + "'", response.getStatusCode());
            }
            return right(readStream(response.getResponseBodyAsStream(), getCharset(response)));
        }

        private String getCharset(final Response response)
        {
            MediaType contentType = MediaType.valueOf(response.getHeader(CONTENT_TYPE));
            return option(contentType.getParameters().get("charset")).getOrElse("ISO-8859-1");
        }

        private FeedModel readStream(final InputStream entity, String charset) throws ResponseException
        {
            try
            {
                // SAL will close the input stream as soon as this method returns, so in case the
                // feed parser is lazy (as Abdera is), we must read in all the data RIGHT NOW!
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                copy(entity, baos);
                Reader reader = new InputStreamReader(new ByteArrayInputStream(baos.toByteArray()), charset);

                return feedParser.readFeed(reader);
            }
            catch (ClassCastException cce)
            {
                throw new ResponseException("Response is not an atom feed");
            }
            catch (ParseException pe)
            {
                throw new ResponseException("Error reading returned atom feed from " + appLink.getRpcUrl() + uri, pe);
            }
            catch (IOException e)
            {
                throw new ResponseException("Error reading returned atom feed from " + appLink.getRpcUrl() + uri, e);
            }
        }

        public Either<Error, FeedModel> credentialsRequired(Response response) throws ResponseException
        {
            // Normally this condition will be handled before we get here, i.e. a CredentialsRequiredException
            // will be thrown.  However, if authentication was done previously on this end but the token is no
            // longer valid on the other end, UAL will call this method.
            return retryHandler.apply(Option.<CredentialsRequiredException>none());
        }
    }

    private static final class KeyValidationHandler implements ApplicationLinkResponseHandler<Either<Error, Boolean>>
    {
        public Either<Error, Boolean> handle(Response response) throws ResponseException
        {
            return right(response.getStatusCode() == OK.getStatusCode());
        }

        public Either<Error, Boolean> credentialsRequired(Response response) throws ResponseException
        {
            return right(false);
        }
    }

    private final class GetFiltersHandler implements ApplicationLinkResponseHandler<Either<Error, Iterable<ProviderFilterRepresentation>>>
    {
        private final String url;
        private final AppLinksActivityProvider provider;
        private final Boolean addApplinkName;

        public GetFiltersHandler(String url, AppLinksActivityProvider provider, boolean addApplinkName)
        {
            this.url = url;
            this.provider = provider;
            this.addApplinkName = addApplinkName;
        }

        public Either<Error, Iterable<ProviderFilterRepresentation>> handle(Response response) throws ResponseException
        {
            if (response.getStatusCode() != OK.getStatusCode())
            {
                log.warn("Unable to retrieve filter options from {}", url);
                return right((Iterable<ProviderFilterRepresentation>) ImmutableList.<ProviderFilterRepresentation>of());
            }

            try
            {
                return right(transform(
                    filter(
                        readResponseAs(response, MediaTypes.STREAMS_JSON_TYPE, StreamsConfigRepresentation.class).getFilters(),
                        not(standardFilterOptions)),
                    toAppLinksFilterRepresentation(provider, addApplinkName)));
            }
            catch (IOException e)
            {
                log.warn("Unable to retrieve filters from " + url, e);
                return right((Iterable<ProviderFilterRepresentation>) ImmutableList.<ProviderFilterRepresentation>of());
            }
        }

        private Function<ProviderFilterRepresentation, ProviderFilterRepresentation> toAppLinksFilterRepresentation(AppLinksActivityProvider provider, boolean addApplinkName)
        {
            return new ToAppLinksFilterRepresentation(provider, addApplinkName);
        }

        public Either<Error, Iterable<ProviderFilterRepresentation>> credentialsRequired(Response response) throws ResponseException
        {
            return handle(response);
        }
    }

    private static final class ToAppLinksFilterRepresentation implements Function<ProviderFilterRepresentation, ProviderFilterRepresentation>
    {
        private final AppLinksActivityProvider provider;
        private final Boolean addApplinkName;

        public ToAppLinksFilterRepresentation(AppLinksActivityProvider provider, boolean addApplinkName)
        {
            this.provider = provider;
            this.addApplinkName = addApplinkName;
        }

        @Override
        public ProviderFilterRepresentation apply(ProviderFilterRepresentation rep)
        {
            return new ProviderFilterRepresentation(rep.getKey() + PROVIDER_KEY_SEPARATOR + provider.getKey(),
                    !addApplinkName || provider.getName().equals(rep.getName()) ? rep.getName() : provider.getName() + " - " + rep.getName(),
                    provider.getName(),
                    rep.getOptions(),
                    rep.getProviderAliasOptionKey());
        }
    }

    private static final Predicate<ProviderFilterRepresentation> standardFilterOptions = new Predicate<ProviderFilterRepresentation>()
    {
        @Override
        public boolean apply(ProviderFilterRepresentation filters)
        {
            return STANDARD_FILTERS_PROVIDER_KEY.equals(filters.getKey());
        }
    };

    private <T> T readResponseAs(Response response, MediaType mediaType, Class<T> type) throws IOException, ResponseException
    {
        return type.cast(jsonProvider.readFrom(
            asClassOfObject(type),
            asClassOfObject(type),
            new Annotation[]{},
            mediaType,
            emptyHeaders(),
            response.getResponseBodyAsStream()));
    }

    ApplicationLink getApplink()
    {
        return appLink;
    }

    @SuppressWarnings("unchecked")
    private static Class<Object> asClassOfObject(Class<? extends Object> c)
    {
        return (Class<Object>) c;
    }

    private static MultivaluedMap<String, String> emptyHeaders()
    {
        return AppLinksActivityProvider.<String, String>emptyMultivaluedMap();
    }

    private static <A, B> MultivaluedMap<A, B> emptyMultivaluedMap()
    {
        return new MultivaluedMap<A, B>()
        {
            @Override
            public void add(A key, B value)
            {
            }

            @Override
            public B getFirst(A key)
            {
                return null;
            }


            @Override
            public void putSingle(A key, B value)
            {
            }

            @Override
            public void clear()
            {
            }

            @Override
            public boolean containsKey(Object arg0)
            {
                return false;
            }

            @Override
            public boolean containsValue(Object arg0)
            {
                return false;
            }

            @Override
            public Set<java.util.Map.Entry<A, List<B>>> entrySet()
            {
                return emptySet();
            }

            @Override
            public List<B> get(Object arg0)
            {
                return null;
            }

            @Override
            public boolean isEmpty()
            {
                return true;
            }

            @Override
            public Set<A> keySet()
            {
                return emptySet();
            }

            @Override
            public List<B> put(A arg0, List<B> arg1)
            {
                return null;
            }

            @Override
            public void putAll(Map<? extends A, ? extends List<B>> arg0)
            {
            }

            @Override
            public List<B> remove(Object arg0)
            {
                return null;
            }

            @Override
            public int size()
            {
                return 0;
            }

            @Override
            public Collection<List<B>> values()
            {
                return ImmutableList.of();
            }
        };
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AppLinksActivityProvider that = (AppLinksActivityProvider) o;

        if (appLink != null ? !appLink.equals(that.appLink) : that.appLink != null) return false;

        return true;
    }

    @Override
    public int hashCode()
    {
        return appLink != null ? appLink.hashCode() : 0;
    }
}
