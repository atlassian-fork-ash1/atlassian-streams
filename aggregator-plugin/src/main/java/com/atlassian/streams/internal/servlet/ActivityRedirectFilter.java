package com.atlassian.streams.internal.servlet;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

public class ActivityRedirectFilter implements Filter
{
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException
    {
        request.getRequestDispatcher("/plugins/servlet/streams").forward(request, response);
    }

    public void init(FilterConfig filterConfig) throws ServletException {}
    public void destroy() {}

}
