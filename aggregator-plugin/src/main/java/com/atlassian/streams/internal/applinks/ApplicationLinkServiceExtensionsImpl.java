package com.atlassian.streams.internal.applinks;

import java.net.URI;

import javax.ws.rs.core.UriBuilder;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkRequestFactory;
import com.atlassian.applinks.api.CredentialsRequiredException;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.net.Request.MethodType;

import static com.google.common.base.Preconditions.checkNotNull;

public class ApplicationLinkServiceExtensionsImpl implements ApplicationLinkServiceExtensions
{
    private static final String DEFAULT_TOKEN_ADMIN_PATH = "/plugins/servlet/oauth/users/access-tokens";
    private static final String CONFLUENCE_TOKEN_ADMIN_PATH = "/users/revokeoauthtokens.action";
    private static final String CONFLUENCE_TYPE_KEY = "applinks.confluence";
    private static final String COMPLETION_SERVLET_PATH = "/plugins/servlet/streams/applinks/oauth/completion";

    private final ApplicationProperties applicationProperties;
    
    public ApplicationLinkServiceExtensionsImpl(ApplicationProperties applicationProperties)
    {
        this.applicationProperties = checkNotNull(applicationProperties, "applicationProperties");
    }
    
    @Override
    public boolean isAuthorised(ApplicationLink appLink)
    {
        ApplicationLinkRequestFactory arf = appLink.createAuthenticatedRequestFactory();
        
        // To detect whether we have an access token for this applink, we have to try to
        // create a request to it.  That doesn't actually prove that the token is *valid* -
        // to do that, we would have to execute the request (preferably requesting some
        // resource that isn't expensive, but that does require authentication).  But
        // currently this status check is only going to happen after the user has clicked
        // an authenticate link, and that link will only be displayed if a prior applink
        // request has failed (which clears the token), this should be OK.
        
        try
        {
            arf.createRequest(MethodType.GET, appLink.getDisplayUrl().toString());
            // note that the URL here is irrelevant since we're not executing the request

            return true;
        }
        catch (CredentialsRequiredException e)
        {
            return false;
        }
    }
    
    @Override
    public URI getUserAdminUri(ApplicationLink appLink)
    {
        // The UAL implementation of this will probably involve adding a method to the
        // ApplicationType interface, which already provides polymorphic behavior for the
        // various apps.

        String uriPath;
        if (appLink.getType().getI18nKey().equals(CONFLUENCE_TYPE_KEY))
        {
            uriPath = CONFLUENCE_TOKEN_ADMIN_PATH;
        }
        else
        {
            uriPath = DEFAULT_TOKEN_ADMIN_PATH;
        }
        
        return UriBuilder.fromUri(appLink.getDisplayUrl()).path(uriPath).build();
    }
    
    public URI getAuthCallbackUri(ApplicationLink appLink)
    {
        return UriBuilder.fromUri(applicationProperties.getBaseUrl())
                .path(COMPLETION_SERVLET_PATH)
                .queryParam(OAuthCompletionServlet.APPLINK_ID_PARAM, appLink.getId())
                .build();
    }
}
