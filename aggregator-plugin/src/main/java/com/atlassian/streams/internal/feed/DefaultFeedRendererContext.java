package com.atlassian.streams.internal.feed;

import java.net.URI;

import com.atlassian.plugin.webresource.UrlMode;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.streams.api.common.Option;
import com.atlassian.streams.api.common.uri.Uri;
import com.atlassian.streams.api.common.uri.UriBuilder;

import com.google.common.collect.ImmutableList;

import org.springframework.beans.factory.annotation.Qualifier;

import static com.atlassian.streams.api.common.Option.none;
import static com.atlassian.streams.api.common.Option.some;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Provides data that a {@link FeedRenderer} may need that is independent of the output format.
 */
public class DefaultFeedRendererContext implements FeedRendererContext
{
    public static final String DEFAULT_FEED_AUTHOR = "streams.feed.title.default";
    public static final String DEFAULT_FEED_TITLE = "streams.feed.author.default";
    public static final String ANONYMOUS_USER_NAME = "streams.authors.unknown.capitalized";
    
    private final I18nResolver i18nResolver;
    private final WebResourceManager webResourceManager;

    private static final Iterable<Integer> PICTURE_SIZES = ImmutableList.of(16, 48);
    private final boolean DEV_MODE = Boolean.getBoolean("atlassian.dev.mode");

    public DefaultFeedRendererContext(@Qualifier("streamsI18nResolver") final I18nResolver i18nResolver,
                                      WebResourceManager webResourceManager)
    {
        this.i18nResolver = checkNotNull(i18nResolver, "i18nResolver");
        this.webResourceManager = checkNotNull(webResourceManager, "webResourceManager");
    }

    public String getAnonymousUserName()
    {
        return i18nResolver.getText(ANONYMOUS_USER_NAME);
    }
    
    public String getDefaultFeedAuthor()
    {
        return i18nResolver.getText(DEFAULT_FEED_AUTHOR);
    }
    
    public String getDefaultFeedTitle()
    {
        return i18nResolver.getText(DEFAULT_FEED_TITLE);
    }

    public Iterable<Integer> getDefaultUserPictureSizes()
    {
        return PICTURE_SIZES;
    }
    
    public Option<URI> getUserPictureUri(Option<URI> baseUri, int size, String application)
    {
        if ("com.atlassian.bamboo".equalsIgnoreCase(application)) // STRM-630 bamboo currently doesn't have user icons, so use a bamboo icon instead
        {
            String uri = webResourceManager.getStaticPluginResource("com.atlassian.streams:streamsWebResources",
                                                                    "images/bamboo-logo-" + size + ".png",
                                                                    UrlMode.ABSOLUTE);
            return some(URI.create(uri));
        }
        else
        {
            for (URI base: baseUri)
            {
                // add size parameter for Gravatar calls; will be ignored by non-Gravatar services
                return some(new UriBuilder(Uri.fromJavaUri(base))
                        .addQueryParameter("s", String.valueOf(size))
                        .toUri().toJavaUri());
            }
            return none();
        }
    }
    
    public boolean isDeveloperMode()
    {
        return DEV_MODE;
    }
}
