package com.atlassian.streams.internal.rest.representations;

import java.util.Collection;

import com.google.common.collect.ImmutableList;

import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;

public class ValidationErrorCollectionRepresentation
{
    @JsonProperty private final Collection<ValidationErrorEntry> errors;

    @JsonCreator
    public ValidationErrorCollectionRepresentation(@JsonProperty("errors") Collection<ValidationErrorEntry> errors)
    {
        this.errors = ImmutableList.copyOf(errors);
    }

    public Collection<ValidationErrorEntry> getErrors()
    {
        return errors;
    }

    public static class ValidationErrorEntry
    {
        @JsonProperty private final String field;
        @JsonProperty private final String error;
        @JsonProperty private final Collection<String> params;

        @JsonCreator
        public ValidationErrorEntry(@JsonProperty("field") String field,
                @JsonProperty("error") String error,
                @JsonProperty("params") Collection<String> params)
        {
            this.field = field;
            this.error = error;
            this.params = ImmutableList.copyOf(params);
        }

        public ValidationErrorEntry(String field, String error)
        {
            this.field = field;
            this.error = error;
            this.params = ImmutableList.of();
        }

        public String getField()
        {
            return field;
        }

        public String getError()
        {
            return error;
        }

        public Collection<String> getParams()
        {
            return params;
        }
    }
}
