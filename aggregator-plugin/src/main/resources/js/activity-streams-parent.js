var ActivityStreams = ActivityStreams || (function() {
    var inlineActions = {},
        baseUrl,
        streamsServletPath = '/plugins/servlet/streams',
        gadgetPrefs = new gadgets.Prefs(),
        datePrefs = {
            dateFormat: '',
            dateTimeFormat: '',
            timeFormat: ''
        };

    /**
     * Registers an HTML element to be added as an action to the specified type of activity item
     * Usage: ActivityStreams.registerAction('issue', actionTrigger)
     * where actionTrigger is a jQuery-wrapped DOM element with any necessary events already attached to it.
     * See inline-actions/comment.js for an example of this in action.
     * @method registerAction
     * @param {String} typeString The type of activity object type, eg 'issue' or 'build'.  Can be multiple space-delimited types
     * @param {HTMLElement} element The jQuery-wrapped "trigger" element, with any necessary events already attached to it
     * @param {Object} sortBy an object by which the action should be sorted against other actions
     */
    function registerAction(typeString, element, sortBy) {
        var types = typeString ? typeString.split(' ') : [];
        for (var i = 0, len = types.length; i < len; i++) {
            var type = types[i];
            if (inlineActions[type]) {
                inlineActions[type].push({element: element, sortBy: sortBy});
                inlineActions[type].sort(function (a, b) {
                    if (a.sortBy < b.sortBy) {
                        return -1;
                    } else {
                        return 1;
                    }
                });
            } else {
                inlineActions[type] = [{element: element, sortBy: sortBy}];
            }
        }
    }

    /**
     * Saves a given name/value pair to gadget prefs.  The gadget pref in question must be declared in the gadget xml.
     * @method setGadgetPref
     * @param {String} name The name of the preference to save
     * @param {String} value The value to save
     */
    function setGadgetPref(name, value) {
        // gadgets expect arrays to be stored as pipe-separated values
        // but streams expects usernames to be comma-separated
        if (AJS.$.isArray(value)) {
            value = (name == 'username') ? value.join(',') : value.join('|');
        }
        gadgetPrefs.set(name, encodeURIComponent(value));
    }

    /**
     * Retrieves a specified value from the gadget prefs
     * @method getGadgetPref
     * @param {String} name The name of the preference to retrieve
     * @return {String} The value of the preference
     */
    function getGadgetPref(name) {
        // We unescape the value because we'd like to get back the same thing we put in, not an escaped value. STRM-672
        return decodeURIComponent(gadgets.util.unescapeString(gadgetPrefs.getString(name)));
    }

    /**
     * Retreives a specified array of values from the gadget prefs
     * @method getGadgetPrefArray
     * @param {String} name The name of the preference to retreive
     * @return {Array} The array of values associated with the preference
     */
    function getGadgetPrefArray(name) {
        return gadgetPrefs.getArray(name);
    }

    /**
     * Retrieves an i18n message from the gadget prefs
     * @method getGadgetMessage
     * @param {String} key The key of the message to retrieve
     * @return {String} An internationalized message
     */
    function getGadgetMessage(key) {
        // STRM-1688:  Due to inconsistent SAL implementation in the products, escaped apostrophes ('')
        // may or may not have been changed to single apostrophes.  In any string that has {0} placeholders,
        // we always want our apostrophes doubled up (since we don't have any strings that use single
        // quotes for escaping of a literal '{0}').
        var msg = gadgetPrefs.getMsg(key);
        if (msg.search(/{[0-9]+}/) < 0) {
            return msg;
        }
        return msg.replace(/''/g, "'").replace(/'/g, "''");
        // (this double replace ensures that both '' and ' will end up as '')
    }

    /**
     * Given the current config options, returns the corresponding streams feed url
     * @method getStreamUrl
     * @param {Object} data (optional) Object representing current streams config options
     * @return {String} Url to hit to get the stream feed corresponding to the provided options
     */
    function getStreamUrl(data) {
        var url = baseUrl + streamsServletPath;
        url += (url.indexOf('?') == -1) ? '?' : '&';
        url += AJS.$.param(data);
        return url;
    }

    /**
     * Transforms a date into a format accepted by the back end
     * @method getDateValue
     * @param {String|Array} dateValue The string representing the date, usually in Dec/17/2010 format (can also be an array of strings)
     * @param {String} operator The operator associated with the date, so we can return the beginning or end of the day as necessary
     * @return {Number} The date in the desired format (number of millis since 1970) (space-separated if input was array)
     */
    function getDateValue(dateValue, operator) {
        // make sure we're dealing with a string so we can use indexOf and split without errors
        dateValue += '';
        var dateString,
            date;
        operator = operator.toUpperCase();
        dateValue = dateValue.indexOf(' ') != -1 ? dateValue.split(' ') : [dateValue];
        if (operator == 'BETWEEN' && dateValue.length == 1) {
            dateValue[1] = dateValue[0];
        }
        for (var i = 0; i < dateValue.length; i++) {
            // make sure we're adding numbers and not concatenating strings
            date = parseInt(dateValue[i]);
            if (operator == 'AFTER' || (operator == 'BETWEEN' && i == 1)) {
                date += (1000 * 60 * 60 * 24 - 1); // Add 23 hours, 59 minutes, 59 seconds to get to end of day
            }
            dateValue[i] = date;
        }
        return dateValue.join(' ');
    }

    /**
     * Handles modifications to rule operators and values for special cases
     * @method handleSpecialCases
     * @param {Object} rule An object representing the rule in question
     * @return {Object} The rule object with modifications for special cases
     */
    function handleSpecialCases(rule) {
        var value = rule.value,
            operator = rule.operator,
            type = rule.type;
        if (AJS.$.isArray(value)) {
            var encodedValues = [];
            for (var i = 0, n = value.length; i < n; i++) {
                // additional level of encoding for spaces because otherwise the spaces
            	// will be mistaken for value delimiters (STRM-1857); escape space as "_"
            	// and escape any existing "_" as "\_"
                encodedValues.push(value[i].replace(/_/g, "\\_").replace(/ /g, "_"));
            }
            value = encodedValues.join(' ');
        }
        if (type === 'date') {
            if (operator === 'is') {
                // back end doesn't support "IS", we have to turn it into a between
                operator = 'between';
            }
            value = getDateValue(value, operator);
        } else if (type === 'list' || type === 'user') {
            // allow comma-separated values (in addition to space separated) for issue keys and usernames
            value = value.replace(/,/g, ' ');
            // escape underscores as "\_" (because "_" is used to escape spaces in lists)
            value = value.replace(/_/g, "\\_");
        }
        return {
            value: value,
            operator: operator
        };
    }

    /**
     * For use when making UI changes before and after a synchronous requests.  Avoids "flickering" UI elements by:
     *  -- only running the the "startFn" if the request takes longer than a specified threshold
     *  -- if startFn is run, stopFn will not be run until a specified delay has passed
     * @method execCallbacksWithThreshold
     * @param {Function} startFn Function to run at the beginning of the request
     * @param {Function} stopFn Function to run once the request has been completed
     * @param {Number} latencyThreshold Amount of time to wait before calling startFn (in ms)
     * @param {Number} minShowTime Minimum amount of time between calling startFn and stopFn (in ms)
     * @return {Function} The function to run when request has completed (stopFn is ready to be executed)
     */
    function execCallbacksWithThreshold(startFn, stopFn, latencyThreshold, minShowTime) {
        latencyThreshold = latencyThreshold || 50;
        minShowTime = minShowTime || 1000;
        var stop,
            delay;

        delay = function (callback, l) {
            delay.t = setTimeout(function(){
                clearTimeout(delay.t);
                delay.t = undefined;
                callback && callback();
            }, l);
        };

        delay(function() {
            // if stop is already defined, returned fn has been called, so don't even execute startFn
            if (!stop) {
                startFn();
                delay(function() {
                    // if stop is defined here, returned fn was called during the delay, so call stop()
                    if (stop) {
                        stop();
                    }
                }, minShowTime);
            }
        }, latencyThreshold);

        return function() {
            // don't define stop until the returned function is called
            stop = stopFn;

            // only run stop() if no timeout is defined
            if (!delay.t) {
                stop();
            }
        };

    }

    /**
     * Shows a throbber (by setting the class to 'loading') after a specified threshold (to prevent flicker)
     * @method showThrobberAfterThreshold
     * @param {HTMLElement} element The container to show a throbber in
     * @param {Number} threshold Time to wait before showing the throbber
     * @return {Function} Function to run to hide the throbber
     */
    function showThrobberAfterThreshold(element, threshold) {
        var showThrobber = function() {
                element.addClass('loading');
            },
            hideThrobber = function() {
                element.removeClass('loading');
            };
        threshold = threshold || 50;
        return execCallbacksWithThreshold(showThrobber, hideThrobber, threshold, 500);
    }

    /**
     * Creates and returns the default settings object for the datepicker widget, based on info from the localized date.js file
     * @method getDatepickerDefaults
     * @return {Object} Default settings objects
     */
    function getDatepickerDefaults() {
        var cultureInfo = Date.CultureInfo;
        return getDatepickerDefaults.defaults = getDatepickerDefaults.defaults || (cultureInfo && {
            // load the localized info from date.js
            dayNames: cultureInfo.dayNames,
            dayNamesShort: cultureInfo.abbreviatedDayNames,
            dayNamesMin: cultureInfo.shortestDayNames,
            monthNames: cultureInfo.monthNames,
            monthNamesShort: cultureInfo.abbreviatedMonthNames,
            dateFormat: cultureInfo.formatPatterns.shortDate.replace('yyyy', 'yy'),
            // if year comes first in localized format, we need to tell the datepicker that
            showMonthAfterYear: cultureInfo.dateElementOrder.charAt(0) === 'y',
            // don't show the datepicker when input field is focused, only when button is clicked
            showOn: 'focus',
            // i18n
            currentText: getGadgetMessage('gadget.activity.stream.config.datepicker.today'),
            nextText: getGadgetMessage('gadget.activity.stream.config.datepicker.next'),
            prevText: getGadgetMessage('gadget.activity.stream.config.datepicker.previous'),
            closeText: getGadgetMessage('gadget.activity.stream.config.datepicker.close'),
            buttonText: '...'
        }) || {};
    }


    /**
     * Deletes the cached datepicker defaults.  Used when date localization info is updated.
     * @method resetDatepickerDefaults
     */
    function resetDatepickerDefaults() {
        delete getDatepickerDefaults.defaults;
    }

    /**
     * Ensures that AUI message boxes have a visible and usable close box when appropriate.
     * This is a workaround for the limited version of AJS.messages available within gadgets.
     * If the real AJS.messages is available, this is a no-op.
     * @method makeMessageCloseable
     * @param {Object} element  a JQuery object corresponding to a &lt;div&gt;
     *  element (or elements) having the CSS class "aui-message"
     */
    function makeMessageCloseable(element) {
        if (AJS.messages && AJS.messages.makeCloseable) {
            // If the real AJS function is present, it will have already been called.
            return;
        }
        element.each(function (index) {
            var subElement = AJS.$(this),
                icon = AJS.$('<span class="aui-icon icon-close"></span>');
            icon.click(function () {
                subElement.trigger("messageClose", [this]).remove();
            });
            subElement.append(icon);
            // The gadgetized versions of the message creation functions do not add the
            // "closeable" class as they should; this is necessary for styling
            subElement.addClass('closeable');
        });
    }

    return {

        // Returns all of the registered actions for a specified type
        getActions: function(type) {
            var actions = inlineActions[type] || [],
                actionsArray = [];
            for (var i = 0, len = actions.length; i < len; i++) {
                actionsArray.push(actions[i].element);
            }
            return actionsArray;
        },

        // Set the base url
        setBaseUrl: function(url) {
            baseUrl = url;
        },

        // Get the currently set base url
        getBaseUrl: function() {
            return baseUrl;
        },

        // Set the path to the servlet wich is appended to the base url
        setServletPath: function(path) {
            streamsServletPath = path;
        },

        // Get the path to the servlet wich is appended to the base url
        getServletPath: function() {
            return streamsServletPath;
        },

        // Get the gadgetPrefs object
        getPrefs: function() {
            return gadgetPrefs;
        },

        // Expose registerAction method for public use
        registerAction: registerAction,

        // Expose setGadgetPref method for public use
        setPref: setGadgetPref,

        // Expose getGadgetPref method for public use
        getPref: getGadgetPref,

        // Expose getGadgetPrefArray method for public use
        getPrefArray: getGadgetPrefArray,

        // Expose getGadgetMessage method for public use
        getMsg: getGadgetMessage,

        // Expose getStreamUrl method for public use
        getStreamUrl: getStreamUrl,

        // Expose getDateValue method for public use
        getDateValue: getDateValue,

        // Expose handleSpecialCases method for public use
        handleSpecialCases: handleSpecialCases,

        // Expose handleSpecialCases method for public use
        showThrobber: showThrobberAfterThreshold,

        // Expose getDatepickerDefaults method for public use
        getDatepickerDefaults: getDatepickerDefaults,

        // Expose resetDatepickerDefaults method for public use
        resetDatepickerDefaults: resetDatepickerDefaults,

        getDatePrefs: function() {
            return datePrefs;
        },

        getDateFormat: function() {
            return datePrefs.dateFormat;
        },

        getDateTimeFormat: function() {
            return datePrefs.dateTimeFormat;
        },

        getTimeFormat: function() {
            return datePrefs.timeFormat;
        },

        getDateRelativize: function() {
            return datePrefs.dateRelativize;
        },

        setDatePrefs: function(prefs) {
            AJS.$.extend(datePrefs, prefs);
            AJS.Date.setTimeFormat(datePrefs.timeFormat);
        },

        makeMessageCloseable: makeMessageCloseable
    };
})();
