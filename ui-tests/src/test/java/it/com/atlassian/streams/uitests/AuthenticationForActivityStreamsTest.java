package it.com.atlassian.streams.uitests;

import com.atlassian.integrationtesting.runner.TestGroups;
import com.atlassian.jira.pageobjects.JiraTestedProduct;
import com.atlassian.jira.pageobjects.pages.DashboardPage;
import com.atlassian.pageobjects.DefaultProductInstance;
import com.atlassian.pageobjects.ProductInstance;
import com.atlassian.pageobjects.util.Env;
import com.atlassian.streams.testing.UiTestsTesterRunner;
import com.atlassian.streams.testing.pageobjects.jira.JiraProfileActivityStream;
import com.atlassian.streams.testing.pageobjects.jira.JiraViewProfilePage;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.net.InetAddress;
import java.net.URI;
import java.net.UnknownHostException;

import static com.atlassian.streams.testing.UiTestGroups.MULTI;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;

@RunWith(UiTestsTesterRunner.class)
@TestGroups(MULTI)
public class AuthenticationForActivityStreamsTest {

    private static final String USER_PROFILE_PAGE_URL = "/secure/ViewProfile.jspa";
    private static final String CONTEXT_PATH = "/streams";
    private JiraViewProfilePage pageUnderTest;

    @Before
    public void goToViewProfilePage() {
        String jiraBaseUrl = "http://" + Env.getVar("WEBDRIVER_TARGET_HOST", getLocalHostName()) + ":" + Env.getVar("WEBDRIVER_TARGET_PORT", String.valueOf(2990)) + CONTEXT_PATH;
        ProductInstance jiraInstance = new DefaultProductInstance(jiraBaseUrl, "jira-applinks-oauth", 2990, CONTEXT_PATH);
        JiraTestedProduct jira = new JiraTestedProduct(null, jiraInstance);

        jira.gotoLoginPage().login("user", "user", DashboardPage.class);
        pageUnderTest = jira.visit(JiraViewProfilePage.class, URI.create(USER_PROFILE_PAGE_URL));
    }

    @Test
    public void assertThatAuthenticationMessagesAreShown() {
        String authenticationMessage = "Additional information may be available, please authenticate for more information";

        JiraProfileActivityStream activityStream = pageUnderTest.activityStreamView();

        assertThat(activityStream.getAuthenticationMessage(), containsString(authenticationMessage));
        assertThat(activityStream.getAllAuthenticationMessages().size(), is(2));
        assertThat(activityStream.getAuthenticationMessageAtIndex(0), containsString("Authenticate with "));
        assertThat(activityStream.getAuthenticationMessageAtIndex(1), containsString("Authenticate with "));
    }

    private static String getLocalHostName() {
        try {
            return InetAddress.getLocalHost().getHostName();
        } catch (UnknownHostException var1) {
            throw new RuntimeException(var1);
        }
    }

}
