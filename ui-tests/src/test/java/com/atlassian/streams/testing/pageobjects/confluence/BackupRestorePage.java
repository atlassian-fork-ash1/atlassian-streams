package com.atlassian.streams.testing.pageobjects.confluence;

import com.atlassian.pageobjects.Page;
import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.webdriver.AtlassianWebDriver;
import com.google.common.base.Function;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import javax.inject.Inject;

public class BackupRestorePage implements Page
{
    private static final String BACKUP_SELECT_XPATH = "//select[@name=\"localFileName\"]";

    @Inject
    private AtlassianWebDriver driver;

    @FindBy(xpath = BACKUP_SELECT_XPATH)
    private WebElement backupSelector;

    @FindBy(xpath = "//input[@value=\"Import\"]")
    private WebElement restoreButton;

    public String getUrl()
    {
        return "/admin/backup.action";
    }

    @WaitUntil
    public void waitUntilBackupSelectorIsVisible()
    {
        driver.waitUntilElementIsVisible(By.xpath(BACKUP_SELECT_XPATH));
    }

    public BackupRestorePage selectFile(String filename)
    {
        String xpath = "//option[@value=\"" + filename + "\"]";
        backupSelector.findElement(By.xpath(xpath)).click();
        return this;
    }

    public void submit()
    {
        restoreButton.click();
        driver.waitUntil(restoreCompletes, 180);
    }

    private static boolean isBackupRestoreComplete(WebDriver driver) {
        WebElement element = driver.findElement(By.id("taskCurrentStatus"));
        return element != null && element.getText().contains("Complete");
    }

    private static final Function<WebDriver, Boolean> restoreCompletes = new Function<WebDriver, Boolean>()
    {
        public Boolean apply(WebDriver driver)
        {
            if (isBackupRestoreComplete(driver))
            {
                return true;
            }
            else
            {
                try
                {
                    Thread.sleep(500);
                    return false;
                }
                catch (InterruptedException e)
                {
                    throw new RuntimeException(e);
                }
            }
        }
    };
}
