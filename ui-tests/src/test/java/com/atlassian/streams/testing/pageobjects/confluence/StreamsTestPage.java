package com.atlassian.streams.testing.pageobjects.confluence;

import com.atlassian.pageobjects.Page;

public class StreamsTestPage implements Page
{
    private static final String URI = "/display/~admin/Home";

    public String getUrl()
    {
        return URI;
    }

}
