package com.atlassian.streams.testing.pageobjects.confluence;

import java.net.URI;
import java.util.List;

import javax.inject.Inject;

import com.atlassian.pageobjects.Page;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.streams.pageobjects.Gadget;
import com.atlassian.webdriver.AtlassianWebDriver;

import com.google.common.base.Predicate;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static com.google.common.collect.Iterables.find;

/**
 * @deprecated Use the {@code c.a.c:confluence-webdriver-pageobjects} library instead. Since v6.1.
 */
@Deprecated
public class ConfluencePage implements Page
{
    private static final String EDIT_PAGE_ID = "editPageLink";
    private static final String WATCH_PAGE_ID = "page-watch";
    private static final String UNWATCH_PAGE_ID = "page-unwatch";
    private static final String WATCHING_SPACE_ID = "space-watching";
    private static final String SPACE_ADVANCED_ID = "space-advanced-link";
    
    private final URI uri;

    @Inject
    private PageBinder pageBinder;

    @Inject
    private AtlassianWebDriver driver;

    @FindBy(id = EDIT_PAGE_ID)
    private WebElement editPageLink;

    @FindBy(id = WATCH_PAGE_ID)
    private WebElement watchLink;

    @FindBy(id = UNWATCH_PAGE_ID)
    private WebElement unwatchLink;

    @FindBy(id = WATCHING_SPACE_ID)
    private WebElement watchingSpaceLink;

    @FindBy(id = SPACE_ADVANCED_ID)
    private WebElement spaceAdvancedLink;
    
    public ConfluencePage(URI uri)
    {
        this.uri = uri;
    }

    public String getUrl()
    {
        return uri.toString();
    }

    @WaitUntil
    public void waitUntilLinksAreVisible()
    {
        driver.waitUntilElementIsLocated(By.id(EDIT_PAGE_ID));
        
        // Don't wait for WATCH_PAGE_ID and UNWATCH_PAGE_ID links to show up,
        // because they're not generated in some contexts.
    }

    public EditPage openEditPage()
    {
        editPageLink.click();
        return pageBinder.bind(EditPage.class, this);
    }

    public Gadget getGadgetWithTitle(String gadgetTitle)
    {
        driver.waitUntilElementIsLocated(By.className("gadget"));
        List<WebElement> gadgetFrames = driver.findElements(By.className("gadget"));
        WebElement gadget = find(gadgetFrames, frameContains(gadgetTitle));
        return pageBinder.bind(Gadget.class, gadget.getAttribute("id"));
    }

    private Predicate<WebElement> frameContains(String string)
    {
        return new FrameContainsPredicate(string);
    }

    private class FrameContainsPredicate implements Predicate<WebElement>
    {
        private final String string;

        public FrameContainsPredicate(String string)
        {
            this.string = string;
        }

        public boolean apply(WebElement element)
        {
            try
            {
                String frameId = element.getAttribute("id");
                driver.switchTo().frame(frameId);
                return driver.findElement(By.tagName("body")).getText().contains(string);
            }
            finally
            {
                driver.switchTo().defaultContent();
            }
        }
    }

    public Boolean isWatching()
    {
        return watchLink.getAttribute("class").contains("inactive");
    }

    public Boolean isWatchingSpace()
    {
        // If you're not watching the space, the "Watching Space" link isn't just
        // disabled - it doesn't exist.
        return driver.elementExists(By.id(WATCHING_SPACE_ID));
    }

    public void toggleWatching()
    {
        if (isWatching())
        {
            unwatch();
        }
        else
        {
            watch();
        }
    }

    public void watch()
    {
        watchHelper(watchLink, WATCH_PAGE_ID);
    }

    public void unwatch()
    {
        watchHelper(unwatchLink, UNWATCH_PAGE_ID);
    }

    public ConfluenceSpaceAdvancedPage openAdvancedPage()
    {
        String url = spaceAdvancedLink.getAttribute("href");
        spaceAdvancedLink.click();
        return pageBinder.bind(ConfluenceSpaceAdvancedPage.class, URI.create(url));
    }
    
    public void unwatchSpace()
    {
        openAdvancedPage().unwatch();
    }
    
    private void watchHelper(WebElement link, String id)
    {
        link.click();
        driver.waitUntilElementIsLocated(By.cssSelector("a#" + id + ".inactive"));
    }

}
