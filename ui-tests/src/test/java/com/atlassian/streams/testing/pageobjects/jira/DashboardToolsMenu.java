package com.atlassian.streams.testing.pageobjects.jira;

import javax.inject.Inject;

import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.webdriver.AtlassianWebDriver;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class DashboardToolsMenu
{
    @Inject
    private AtlassianWebDriver driver;
    
    @Inject
    private PageBinder pageBinder;
    
    @FindBy(id = "tools-dropdown-items")
    private WebElement toolsMenu;
    
    @FindBy(id = "create_dashboard")
    private WebElement createDashboard;
    
    @WaitUntil
    public void waitUntilMenuIsVisible()
    {
       driver.waitUntilElementIsVisibleAt(By.tagName("ul"), toolsMenu);
    }
    
    public CreateDashboardPage createDashboard()
    {
        createDashboard.click();
        return pageBinder.bind(CreateDashboardPage.class);
    }

}
