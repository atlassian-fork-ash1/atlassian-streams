package com.atlassian.streams.crucible;

import com.atlassian.crucible.spi.services.ProjectService;
import com.atlassian.sal.api.component.ComponentLocator;
import com.atlassian.streams.api.common.Option;
import com.atlassian.streams.spi.EntityResolver;

import static com.google.common.base.Preconditions.checkNotNull;

public class ProjectEntityResolver implements EntityResolver
{
    private ProjectService projectService;

    public ProjectEntityResolver()
    {
        this(ComponentLocator.getComponent(ProjectService.class));
    }

    public ProjectEntityResolver(ProjectService projectService)
    {
        this.projectService = checkNotNull(projectService, "projectService");
    }

    public Option<Object> apply(String key)
    {
        return Option.<Object>option(projectService.getProject(key));
    }
}
