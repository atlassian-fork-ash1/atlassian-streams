package com.atlassian.streams.crucible;

import com.atlassian.fecru.user.FecruUser;
import com.cenqua.crucible.model.Comment;
import com.cenqua.crucible.model.Review;
import com.cenqua.crucible.model.managers.CommentManager;
import com.cenqua.crucible.model.managers.LogManager;
import com.cenqua.crucible.model.managers.UserActionManager;
import com.cenqua.crucible.tags.ReviewUtil;

/**
 * Unfortunate hack so that the comment implementation can be tested.
 * {@code CommentManager} cannot be listed as a component-import, and
 * cannot even be class loaded without being in a Hibernate container.
 */
public class CrucibleCommentManagerImpl implements CrucibleCommentManager
{
    private final CommentManager commentManager;

    public CrucibleCommentManagerImpl(CommentManager commentManager) {
        this.commentManager = commentManager;
    }

    @Override
    public Comment getById(int commentId)
    {
        return commentManager.getById(commentId);
    }

    @Override
    public Comment createComment(String message, Review review, FecruUser user)
    {
        return commentManager.createComment(message, review, user);
    }

    @Override
    public void announceCommentCreation(Review review, Comment newComment)
    {
        commentManager.announceCommentCreation(newComment);
        LogManager.logCommentAdded(review, newComment);

    }

    @Override
    public boolean canAddComment(Review review)
    {
        return ReviewUtil.canDoReviewAction(UserActionManager.ACTION_COMMENT, review);
    }
}
