package com.atlassian.streams.crucible;

import com.atlassian.streams.api.ActivityRequest;
import com.atlassian.streams.api.ActivityVerb;
import com.atlassian.streams.api.StreamsEntry;
import com.atlassian.streams.api.common.Option;
import com.atlassian.streams.api.common.Pair;

import java.net.URI;

public interface CrucibleEntryFactory
{
    /**
     * Converts a log entry to a {@code StreamsEntry}.
     *
     * @param baseUri the baseUri to use for links in the entries.
     * @param logEntry The log entry to convert into an entry.
     * @return The converted entry (or none).
     */
    Option<StreamsEntry> getEntryFromActivityItem(Pair<Pair<StreamsCrucibleActivityItem, ActivityVerb>, ActivityRequest> logEntry, URI baseUri);
}
