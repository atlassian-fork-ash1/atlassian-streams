package com.atlassian.streams.crucible;

import java.util.Date;

import com.cenqua.crucible.model.CrucibleActivityItem;
import com.cenqua.crucible.model.Review;
import com.cenqua.crucible.model.managers.LogAction;
import com.cenqua.crucible.model.managers.LogData;

/**
 * Created this class to delegate to {{CrucibleActivityItem}} instead of using {{CrucibleActivityItem}} directly, because
 * {{CrucibleActivityItem}} became final and thus can no longer be mocked in our tests.
 */
public class StreamsCrucibleActivityItem implements LogData
{
    private final CrucibleActivityItem crucibleActivityItem;

    public StreamsCrucibleActivityItem(CrucibleActivityItem crucibleActivityItem)
    {
        this.crucibleActivityItem = crucibleActivityItem;
    }

    public Review getReview()
    {
        return crucibleActivityItem.getReview();
    }

    public void setReview(Review review)
    {
        crucibleActivityItem.setReview(review);
    }

    @Override
    public LogAction getLogAction()
    {
        return crucibleActivityItem.getLogAction();
    }

    @Override
    public String getUsername()
    {
        return crucibleActivityItem.getUsername();
    }

    @Override
    public Integer getReviewId()
    {
        return crucibleActivityItem.getReviewId();
    }

    @Override
    public Integer getId()
    {
        return crucibleActivityItem.getId();
    }

    @Override
    public String getDescription()
    {
        return crucibleActivityItem.getDescription();
    }

    @Override
    public Date getCreateDate()
    {
        return crucibleActivityItem.getCreateDate();
    }

    @Override
    public long getCreateDateTime()
    {
        return crucibleActivityItem.getCreateDateTime();
    }

    @Override
    public Integer getEntityId()
    {
        return crucibleActivityItem.getEntityId();
    }

    @Override
    public Integer getEntityId2()
    {
        return crucibleActivityItem.getEntityId2();
    }

    @Override
    public String getOldValue()
    {
        return crucibleActivityItem.getOldValue();
    }

    @Override
    public String getNewValue()
    {
        return crucibleActivityItem.getNewValue();
    }

    @Override
    public String getEntityString()
    {
        return crucibleActivityItem.getEntityString();
    }
}
