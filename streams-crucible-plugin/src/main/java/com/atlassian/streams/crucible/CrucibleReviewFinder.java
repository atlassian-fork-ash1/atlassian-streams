package com.atlassian.streams.crucible;

import com.atlassian.streams.api.ActivityRequest;
import com.atlassian.streams.api.ActivityVerb;
import com.atlassian.streams.api.common.Pair;

/**
 * Finds Crucible objects to be used in streams production.
 */
public interface CrucibleReviewFinder
{
    /**
     * Get all notifications from the {@code ActivityRequest}'s filters.
     *
     * @param request the {@code ActivityRequest} containing the filters
     * @return the notifications
     */
    Iterable<Pair<StreamsCrucibleActivityItem, ActivityVerb>> getNotificationsFor(ActivityRequest request);
}
