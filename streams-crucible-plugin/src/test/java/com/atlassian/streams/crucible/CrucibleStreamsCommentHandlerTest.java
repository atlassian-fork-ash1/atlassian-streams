package com.atlassian.streams.crucible;

import com.atlassian.fecru.user.FecruUser;
import com.atlassian.fisheye.spi.TxTemplate;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.streams.api.common.Either;
import com.atlassian.streams.crucible.CrucibleStreamsCommentHandler.CommentCreatorCallback;
import com.atlassian.streams.spi.StreamsCommentHandler.PostReplyError;
import com.cenqua.crucible.model.Comment;
import com.cenqua.crucible.model.Review;
import com.cenqua.crucible.model.managers.ReviewManager;
import com.google.common.collect.ImmutableList;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.transaction.TransactionStatus;

import java.net.URI;

import static com.atlassian.streams.spi.StreamsCommentHandler.PostReplyError.Type.DELETED_OR_PERMISSION_DENIED;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CrucibleStreamsCommentHandlerTest
{
    private static final URI BASE_URI = URI.create("http://localhost:3390/streams");

    @Mock
    private CrucibleCommentManager commentManager;
    @Mock
    private UserManager salUserManager;
    @Mock
    private ReviewManager reviewManager;
    @Mock
    private TxTemplate txTemplate;
    @Mock
    private com.cenqua.fisheye.user.UserManager cenquaUserManager;
    @Mock
    private Comment comment;
    @Mock
    private Comment newComment;
    @Mock
    private Review review;
    @Mock
    private FecruUser user;
    @Mock
    private TransactionStatus txStatus;
    @Mock
    private UriProvider uriProvider;
    @Mock
    private CruciblePermissionAccessor permissionAccessor;
    @Mock
    private ApplicationProperties applicationProperties;

    private CrucibleStreamsCommentHandler test;

    @Before
    public void setUp()
    {
        when(commentManager.createComment(any(), any(), any())).thenReturn(newComment);
        when(reviewManager.getReviewById(42)).thenReturn(review);
        when(commentManager.getById(33)).thenReturn(comment);
        when(permissionAccessor.currentUserCanReplyToComment(any())).thenReturn(true);
        when(permissionAccessor.currentUserCanComment(any())).thenReturn(true);
        when(uriProvider.getCommentUri(any(), any()))
                .thenReturn(URI.create("http://foo.bar/path/to/comment#c33"));
        test = new CrucibleStreamsCommentHandler(commentManager, salUserManager, reviewManager, uriProvider,
                txTemplate, cenquaUserManager, permissionAccessor, applicationProperties);
    }

    @Test
    public void testPostReplyInvalidReview()
    {
        Either<PostReplyError, URI> reply = test.postReply(BASE_URI, ImmutableList.of("666", "42"), "hello world");
        assertTrue("error returned", reply.isLeft());
        assertThat(reply.left().get().getType(), is(DELETED_OR_PERMISSION_DENIED));
    }

    @Test
    public void testPostReplyInvalidReplyTo()
    {
        Either<PostReplyError, URI> reply = test.postReply(BASE_URI, ImmutableList.of("42", "666"), "hello world");
        assertTrue("error returned", reply.isLeft());
        assertThat(reply.left().get().getType(), is(DELETED_OR_PERMISSION_DENIED));
    }

    @Test
    public void testPostReplyValidPost()
    {
        Either<PostReplyError, URI> reply = test.postReply(BASE_URI, ImmutableList.of("42", "33"), "hello world");
        assertTrue("no error returned", reply.isRight());
        assertThat(reply.right().get(), is(URI.create("http://foo.bar/path/to/comment#c33")));
    }

    @Test
    public void testCommentCreatorCallback() throws Exception
    {
        CommentCreatorCallback innerTest = test.createCommentCreatorCallback("hello world", comment, review, user);
        innerTest.doInTransaction(txStatus);
        verify(newComment).setDeleted(false);
        verify(newComment).setDraft(false);
        verify(comment).addComment(newComment);
        verify(commentManager).announceCommentCreation(review, newComment);
    }
}
