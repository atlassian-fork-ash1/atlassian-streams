package com.atlassian.streams.crucible;

import com.atlassian.fisheye.spi.TxTemplate;
import com.cenqua.crucible.model.Comment;
import com.cenqua.crucible.model.Review;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CruciblePermissionAccessorImplTest
{
    @Mock
    private TxTemplate txTemplate;
    @Mock
    private CrucibleCommentManager commentManager;
    @Mock
    private Comment comment;
    @Mock
    private Review review;

    private CruciblePermissionAccessorImpl test;

    @Before
    public void setUp()
    {
        when(comment.getReview()).thenReturn(review);
        test = new CruciblePermissionAccessorImpl(txTemplate, commentManager);
    }

    @Test
    public void testCanReplyToCommentAffirmative()
    {
        when(review.checkWriteAccess(any())).thenReturn(true);
        when(commentManager.canAddComment(any())).thenReturn(true);
        assertTrue("canReplyToComment should return true", test.currentUserCanReplyToComment(comment));
    }

    @Test
    public void testCanReplyToCommentNegativeReview()
    {
        assertFalse("canReplyToComment should return false b/c the review isn't accepting comments",
                test.currentUserCanReplyToComment(comment));
    }

    @Test
    public void testCanReplyToCommentNegativeUser()
    {
        assertFalse("canReplyToComment should return false b/c the user can't comment",
                test.currentUserCanReplyToComment(comment));
    }
}
