package com.atlassian.streams.jira.renderer;

import com.atlassian.jira.issue.IssueFieldConstants;
import com.atlassian.jira.issue.changehistory.ChangeHistory;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.streams.api.Html;
import com.atlassian.streams.api.StreamsEntry;
import com.atlassian.streams.api.UserProfile;
import com.atlassian.streams.api.common.ImmutableNonEmptyList;
import com.atlassian.streams.api.common.Option;
import com.atlassian.streams.api.renderer.StreamsEntryRendererFactory;
import com.atlassian.streams.jira.JiraActivityItem;
import com.atlassian.streams.jira.JiraHelper;
import com.atlassian.streams.jira.UriProvider;
import com.atlassian.templaterenderer.TemplateRenderer;

import com.google.common.base.Function;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.ofbiz.core.entity.GenericValue;

import java.io.Serializable;
import java.net.URI;
import java.util.Collections;

import static com.atlassian.streams.api.ActivityVerbs.post;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class IssueTransitionRendererFactoryTest
{
    @Mock
    private I18nResolver i18nResolver;
    @Mock
    private JiraHelper jiraHelper;

    @Before
    public void before()
    {
        when(i18nResolver.getText(anyString(), any())).thenReturn("dummy-i18n");
    }

    @Test
    public void resolvedRendererReEscapesEscapedResolutionText()
    {
        String escapedText = getResolvedRendererEscapedResolution("<script>alert(1)</script>");
        assertEquals("Script tag should be escaped", "&lt;script&gt;alert(1)&lt;/script&gt;", escapedText);
    }

    @Test
    public void resolvedRendererEscapesNonEscapedResolutionText()
    {
        String escapedText = getResolvedRendererEscapedResolution("&amp;");
        assertEquals("encoded ampersand should be re-encoded", "&amp;amp;", escapedText);
    }

    private String getResolvedRendererEscapedResolution(String resolution)
    {
        when(jiraHelper.getNewChangeItemNameTranslation(any(GenericValue.class)))
                .thenReturn(Option.option(resolution));

        StreamsEntry.Renderer renderer = newIssueTransitionRendererFactory().newResolvedRenderer(mockJiraActivityItem(),
                URI.create("http://www.example.com"));
        renderer.renderTitleAsHtml(newStreamsEntry());

        ArgumentCaptor<Serializable> resolutionHtmlCaptor = ArgumentCaptor.forClass(Serializable.class);
        verify(i18nResolver).getText(anyString(), resolutionHtmlCaptor.capture());
        return (String) resolutionHtmlCaptor.getAllValues().get(2);
    }

    @Test
    public void customTransitionRendererEscapesResolutionText()
    {
        when(jiraHelper.getNewChangeItemNameTranslation(any(GenericValue.class)))
                .thenReturn(Option.option("<script>alert(1)</script>&amp;"));

        StreamsEntry.Renderer renderer = newIssueTransitionRendererFactory().newCustomTransitionRenderer(mockJiraActivityItem(),
                URI.create("http://www.example.com"), "dummy-status");
        renderer.renderTitleAsHtml(newStreamsEntry());

        ArgumentCaptor<Serializable> resolutionHtmlCaptor = ArgumentCaptor.forClass(Serializable.class);
        verify(i18nResolver, times(2)).getText(anyString(), resolutionHtmlCaptor.capture());
        assertEquals("Script tag should be escaped and ampersand should be re-escaped",
                "&lt;script&gt;alert(1)&lt;/script&gt;&amp;amp;", resolutionHtmlCaptor.getAllValues().get(3));
    }

    private IssueTransitionRendererFactory newIssueTransitionRendererFactory()
    {
        return new IssueTransitionRendererFactory(mockStreamsEntryRendererFactory(), i18nResolver,
                mockIssueActivityObjectRendererFactory(), mock(TemplateRenderer.class), mock(UriProvider.class),
                jiraHelper);
    }

    private JiraActivityItem mockJiraActivityItem()
    {
        GenericValue changeItem = mock(GenericValue.class);
        when(changeItem.getString("field")).thenReturn(IssueFieldConstants.RESOLUTION);
        ChangeHistory history = mock(ChangeHistory.class);
        when(history.getChangeItems()).thenReturn(Collections.singletonList(changeItem));
        JiraActivityItem jiraActivityItem = mock(JiraActivityItem.class);
        when(jiraActivityItem.getChangeHistory()).thenReturn(Option.some(history));
        return jiraActivityItem;
    }

    private StreamsEntryRendererFactory mockStreamsEntryRendererFactory()
    {
        @SuppressWarnings("unchecked")
        Function<Iterable<UserProfile>, Html> authorsRenderer = mock(Function.class);
        when(authorsRenderer.apply(any())).thenReturn(new Html("dummy-author-html"));
        StreamsEntryRendererFactory streamsEntryRendererFactory = mock(StreamsEntryRendererFactory.class);
        when(streamsEntryRendererFactory.newAuthorsRenderer()).thenReturn(authorsRenderer);
        return streamsEntryRendererFactory;
    }

    private IssueActivityObjectRendererFactory mockIssueActivityObjectRendererFactory()
    {
        @SuppressWarnings("unchecked")
        Function<Iterable<StreamsEntry.ActivityObject>, Option<Html>> issueActivityRenderer = mock(Function.class);
        when(issueActivityRenderer.apply(any()))
                .thenReturn(Option.some(new Html("dummy-activity-object-html")));
        IssueActivityObjectRendererFactory issueActivityObjectRendererFactory = mock(IssueActivityObjectRendererFactory.class);
        when(issueActivityObjectRendererFactory.newIssueActivityObjectsRenderer(any()))
                .thenReturn(issueActivityRenderer);
        return issueActivityObjectRendererFactory;
    }

    public StreamsEntry newStreamsEntry()
    {
        return new StreamsEntry(StreamsEntry.params()
                    .id(URI.create("http://example.com"))
                    .postedDate(new DateTime())
                    .alternateLinkUri(URI.create("http://example.com"))
                    .applicationType("test")
                    .authors(ImmutableNonEmptyList.of(new UserProfile.Builder("someone").build()))
                    .verb(post()).renderer(mock(StreamsEntry.Renderer.class)),
                i18nResolver);
    }
}
