package com.atlassian.streams.jira.renderer;

import java.net.URI;
import java.net.URISyntaxException;

import com.atlassian.jira.issue.changehistory.metadata.HistoryMetadata;
import com.atlassian.jira.issue.changehistory.metadata.HistoryMetadataParticipant;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.streams.api.Html;
import com.atlassian.streams.api.StreamsEntry;
import com.atlassian.streams.api.UserProfile;
import com.atlassian.streams.api.common.ImmutableNonEmptyList;
import com.atlassian.streams.api.common.Option;
import com.atlassian.streams.api.renderer.StreamsEntryRendererFactory;

import com.google.common.base.Function;
import com.google.common.collect.Iterables;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static com.atlassian.streams.api.ActivityObjectTypes.comment;
import static com.atlassian.streams.api.ActivityVerbs.post;
import static com.atlassian.streams.api.common.Option.some;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class HistoryMetadataRendererFactoryTest
{
    @Mock
    private I18nResolver i18nResolver;

    @Mock
    private StreamsEntryRendererFactory streamsEntryRendererFactory;

    private HistoryMetadataRendererFactory factory;

    @Before
    public void setUp()
    {
        factory = new HistoryMetadataRendererFactory(i18nResolver, streamsEntryRendererFactory,
                input -> new Html(input.getId()));
        when(streamsEntryRendererFactory.newAuthorsRenderer())
                .thenReturn(input -> new Html(Iterables.getOnlyElement(input).getFullName()));
    }

    @Test
    public void testSuffixRender() throws Exception
    {
        // having
        StreamsEntry entry = new StreamsEntry(createParams(input -> "original entry"), i18nResolver);

        // when
        final Html html = factory.newSuffixRenderer(entry, "- this is a test").renderTitleAsHtml(entry);

        // then
        assertThat(html, equalTo(new Html("original entry - this is a test")));
    }

    @Test
    public void testCustomKeyRenderer() throws Exception
    {
        // having
        StreamsEntry entry = new StreamsEntry(
                createParams(input -> Iterables.getOnlyElement(input.getAuthors()).getFullName() + " did a thing"),
                i18nResolver);
        HistoryMetadata historyMetadata = createMetadata();
        when(i18nResolver.getText(
                eq("testCustomKeyRenderer"),
                eq(new Html("JIRA User did a thing")),
                eq(new Html("JIRA User")),
                eq(new Html(" did a thing")),
                eq(new Html(historyMetadata.getGenerator().getId())),
                eq(new Html(historyMetadata.getCause().getId()))
        )).thenReturn("formatted");

        // when
        final Html html = factory.newCustomKeyRenderer(entry, historyMetadata).renderTitleAsHtml(entry);

        // then
        assertThat(html, equalTo(new Html("formatted")));
    }

    private HistoryMetadata createMetadata()
    {
        return HistoryMetadata.builder("test")
                .generator(HistoryMetadataParticipant.builder("generator", "test").build())
                .cause(HistoryMetadataParticipant.builder("cause", "test").build())
                .activityDescriptionKey("testCustomKeyRenderer")
                .build();
    }

    private StreamsEntry.Parameters<StreamsEntry.HasId, StreamsEntry.HasPostedDate, StreamsEntry.HasAlternateLinkUri,
            StreamsEntry.HasApplicationType, StreamsEntry.HasRenderer, StreamsEntry.HasVerb,
            StreamsEntry.HasAuthors> createParams(final Function<StreamsEntry, String> titleRenderer)
            throws URISyntaxException
    {
        return StreamsEntry.params()
                .id(new URI("http://localhost/id"))
                .postedDate(new DateTime())
                .alternateLinkUri(URI.create("http://example.com"))
                .applicationType("test")
                .authors(ImmutableNonEmptyList.of(new UserProfile.Builder("jirauser").fullName("JIRA User")
                        .build()))
                .addActivityObject(new StreamsEntry.ActivityObject(StreamsEntry.ActivityObject.params()
                        .id("activity")
                        .title(some("Some activity"))
                        .alternateLinkUri(URI.create("http://example.com"))
                        .activityObjectType(comment())))
                .verb(post()).renderer(new StreamsEntry.Renderer()
                {
                    @Override
                    public Html renderTitleAsHtml(final StreamsEntry entry)
                    {
                        return new Html(titleRenderer.apply(entry));
                    }

                    @Override
                    public Option<Html> renderSummaryAsHtml(final StreamsEntry entry)
                    {
                        return Option.some(new Html("Summary"));
                    }

                    @Override
                    public Option<Html> renderContentAsHtml(final StreamsEntry entry)
                    {
                        return Option.some(new Html("Content"));
                    }
                })
                .baseUri(URI.create("http://example.com"));
    }
}
