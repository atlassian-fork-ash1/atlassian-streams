package com.atlassian.streams.jira.builder;

import java.util.Collections;
import java.util.List;

import com.atlassian.jira.issue.IssueFieldConstants;
import com.atlassian.jira.issue.changehistory.ChangeHistory;
import com.atlassian.jira.issue.changehistory.metadata.HistoryMetadataManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.streams.api.ActivityObjectType;
import com.atlassian.streams.api.ActivityRequest;
import com.atlassian.streams.api.StreamsEntry;
import com.atlassian.streams.api.common.Option;
import com.atlassian.streams.api.common.Pair;
import com.atlassian.streams.jira.AggregatedJiraActivityItem;
import com.atlassian.streams.jira.JiraActivityItem;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.ofbiz.core.entity.GenericValue;

import static com.atlassian.streams.api.ActivityVerbs.update;
import static com.atlassian.streams.jira.JiraActivityVerbs.transition;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.same;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ChangeEntryBuilderTest
{
    private final JiraActivityItem activityItem = mock(JiraActivityItem.class);

    @Mock
    private HistoryMetadataEntryBuilder historyMetadataEntryBuilder;
    @Mock
    private HistoryMetadataManager historyMetadataManager;
    @Mock
    private JiraAuthenticationContext authenticationContext;
    @Mock
    private ApplicationUser user;

    @InjectMocks
    private ChangeEntryBuilder changeEntryBuilder;

    @Before
    public void setUp()
    {
        when(authenticationContext.getUser()).thenReturn(user);
    }

    @Test
    public void testBuildStatusChangeEntry()
    {
        final Option<StreamsEntry> result = Option.none();

        // having
        when(activityItem.getActivity()).thenReturn(Pair.pair(mock(ActivityObjectType.class), transition()));

        final AggregatedJiraActivityItem item = new AggregatedJiraActivityItem(activityItem);
        when(historyMetadataEntryBuilder.buildStatusChangeEntry(same(activityItem), any(), any()))
                .thenReturn(result);

        prepareChangeHistory(makeStatusChangeHistory());

        // when
        final Option<StreamsEntry> entry = changeEntryBuilder.build(item, mock(ActivityRequest.class));

        // then
        assertThat(entry, sameInstance(result));
    }

    @Test
    public void testBuildGeneralUpdateEntry()
    {
        final Option<StreamsEntry> result = Option.none();

        // having
        when(activityItem.getActivity()).thenReturn(Pair.pair(mock(ActivityObjectType.class), update()));

        final AggregatedJiraActivityItem item = new AggregatedJiraActivityItem(activityItem);
        when(historyMetadataEntryBuilder.buildGeneralUpdateEntry(any(), same(item))).thenReturn(result);

        prepareChangeHistory(mock(ChangeHistory.class));

        // when
        final Option<StreamsEntry> entry = changeEntryBuilder.build(item, mock(ActivityRequest.class));

        // then
        assertThat(entry, sameInstance(result));

    }

    private ChangeHistory makeStatusChangeHistory()
    {
        return new ChangeHistory(null, null, null) {
            @Override
            public List<GenericValue> getChangeItems()
            {
                final GenericValue gv = mock(GenericValue.class);
                when(gv.getString("fieldtype")).thenReturn("jira");
                when(gv.getString("field")).thenReturn(IssueFieldConstants.STATUS);
                return Collections.singletonList(gv);
            }
        };
    }

    private void prepareChangeHistory(ChangeHistory history)
    {
        final HistoryMetadataManager.HistoryMetadataResult metadataResult = mock(HistoryMetadataManager.HistoryMetadataResult.class);
        when(historyMetadataManager.getHistoryMetadata(eq(history), eq(user))).thenReturn(metadataResult);
        when(activityItem.getChangeHistory()).thenReturn(Option.some(history));
    }
}
