package com.atlassian.streams.jira.builder;

import java.net.URI;

import com.atlassian.jira.issue.changehistory.ChangeHistory;
import com.atlassian.jira.issue.comments.Comment;
import com.atlassian.streams.api.StreamsEntry;
import com.atlassian.streams.api.StreamsEntry.ActivityObject;
import com.atlassian.streams.api.StreamsEntry.HasAlternateLinkUri;
import com.atlassian.streams.api.StreamsEntry.HasApplicationType;
import com.atlassian.streams.api.StreamsEntry.HasId;
import com.atlassian.streams.api.StreamsEntry.HasPostedDate;
import com.atlassian.streams.api.StreamsEntry.NeedsAuthors;
import com.atlassian.streams.api.StreamsEntry.NeedsRenderer;
import com.atlassian.streams.api.StreamsEntry.NeedsVerb;
import com.atlassian.streams.api.StreamsEntry.Parameters;
import com.atlassian.streams.api.StreamsEntry.Renderer;
import com.atlassian.streams.api.UserProfile;
import com.atlassian.streams.api.common.ImmutableNonEmptyList;
import com.atlassian.streams.api.common.Option;
import com.atlassian.streams.jira.AggregatedJiraActivityItem;
import com.atlassian.streams.jira.JiraActivityItem;
import com.atlassian.streams.jira.JiraHelper;
import com.atlassian.streams.jira.renderer.AttachmentRendererFactory;
import com.atlassian.streams.jira.renderer.IssueUpdateRendererFactory;
import com.atlassian.streams.spi.StreamsI18nResolver;

import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableList;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.ofbiz.core.entity.GenericValue;

import static com.atlassian.jira.issue.IssueFieldConstants.ISSUE_TYPE;
import static com.atlassian.streams.api.ActivityVerbs.update;
import static com.atlassian.streams.api.StreamsEntry.params;
import static com.atlassian.streams.api.common.Option.none;
import static com.atlassian.streams.api.common.Option.some;
import static com.atlassian.streams.api.common.Pair.pair;
import static com.atlassian.streams.jira.JiraActivityObjectTypes.issue;
import static com.google.common.base.Predicates.alwaysTrue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class GeneralUpdateEntryBuilderTest
{
    private final ImmutableNonEmptyList<UserProfile> userProfiles = ImmutableNonEmptyList.of(newUserProfile());
    @Mock
    private AttachmentRendererFactory attachmentRendererFactory;
    @Mock
    private IssueUpdateRendererFactory issueUpdateRendererFactory;
    @Mock
    private JiraHelper helper;
    @Mock
    private StreamsI18nResolver i18nResolver;
    private Predicate<GenericValue> validAttachment = alwaysTrue();

    private GeneralUpdateEntryBuilder builder;

    private final String uri = "http://example.com/1";

    @Before
    public void prepareBuilder()
    {
        builder = new GeneralUpdateEntryBuilder(helper, attachmentRendererFactory, issueUpdateRendererFactory, i18nResolver);
    }

    @Before
    public void prepareHelper()
    {
        Parameters<HasId, HasPostedDate, HasAlternateLinkUri, HasApplicationType, NeedsRenderer, NeedsVerb, NeedsAuthors> params = newParams();
        when(helper.newBuilder(any(), any())).thenReturn(params);
        when(helper.newCommentBuilder(any(), any())).thenReturn(params);
        when(helper.buildActivityObject(any(), any(), any())).thenReturn(newActivityObject());
        when(helper.validAttachment()).thenReturn(validAttachment);
    }

    @Before
    public void prepareIssueUpdateRendererFactory()
    {
        Renderer r = newRenderer();
        when(issueUpdateRendererFactory.newRenderer(any(), any(), any())).thenReturn(r);
    }

    @Test
    public void assertThatEmptyChangeItemsReturnNone()
    {
        AggregatedJiraActivityItem item = newActivityItemWithoutComment();

        URI baseUri = URI.create("http://www.example.com");
        assertThat(builder.build(baseUri, item), is(none(StreamsEntry.class)));
    }

    @Test
    public void assertThatEntryBuilderReturnsStreamEntryForValidChangeItem()
    {
        AggregatedJiraActivityItem item = newActivityItemWithoutComment(newChangeItem(ISSUE_TYPE));
        URI baseUri = URI.create("http://www.example.com");
        when(helper.getUserProfiles(baseUri, item.getActivityItem())).thenReturn(userProfiles);

        assertThat(builder.build(baseUri, item), is(not(none(StreamsEntry.class))));
    }

    private Parameters<HasId, HasPostedDate, HasAlternateLinkUri, HasApplicationType, NeedsRenderer, NeedsVerb, NeedsAuthors> newParams()
    {
        return params().
            id(URI.create("urn:1")).
            postedDate(new DateTime()).
            alternateLinkUri(URI.create(uri)).
            applicationType("test");
    }

    private Renderer newRenderer()
    {
        return mock(Renderer.class);
    }

    @Test
    public void assertThatEntryBuilderSkipsProjectImportChanges()
    {
        AggregatedJiraActivityItem item = newActivityItemWithoutComment(newChangeItem("projectimport"));

        URI baseUri = URI.create("http://www.example.com");
        assertThat(builder.build(baseUri, item), is(none(StreamsEntry.class)));
    }

    @Test
    public void assertThatEntryBuilderSkipsWorkflowChanges()
    {
        AggregatedJiraActivityItem item = newActivityItemWithoutComment(newChangeItem("workflow"));

        URI baseUri = URI.create("http://www.example.com");
        assertThat(builder.build(baseUri, item), is(none(StreamsEntry.class)));
    }

    @Test
    public void assertCommentLinkIsProvided()
    {
        AggregatedJiraActivityItem item = newActivityItemWithComment(newChangeItem(ISSUE_TYPE));
        URI baseUri = URI.create("http://www.example.com");
        when(helper.getUserProfiles(baseUri, item.getActivityItem())).thenReturn(userProfiles);

        URI commentUri = URI.create(uri);
        Option<StreamsEntry> entry = builder.build(baseUri, item);
        assertEquals(commentUri, entry.get().getAlternateLink());
    }

    private ChangeHistory newChangeHistory(GenericValue... changeItems)
    {
        ChangeHistory history = mock(ChangeHistory.class);
        when(history.getChangeItems()).thenReturn(ImmutableList.copyOf(changeItems));
        return history;
    }

    private AggregatedJiraActivityItem newActivityItem(JiraActivityItem item)
    {
        AggregatedJiraActivityItem aggregatedItem = mock(AggregatedJiraActivityItem.class);
        Option<Iterable<JiraActivityItem>> relatedItems = none();
        when(aggregatedItem.getRelatedActivityItems()).thenReturn(relatedItems);
        when(aggregatedItem.getActivityItem()).thenReturn(item);
        return aggregatedItem;
    }

    private JiraActivityItem newJiraActivityItem(GenericValue... changeItems) {
        ChangeHistory history = newChangeHistory(changeItems);

        JiraActivityItem item = mock(JiraActivityItem.class);
        when(item.getChangeHistory()).thenReturn(some(history));
        when(item.getActivity()).thenReturn(pair(issue(), update()));
        return item;
    }

    private AggregatedJiraActivityItem newActivityItemWithoutComment(GenericValue... changeItems) {
        JiraActivityItem item = newJiraActivityItem(changeItems);
        when(item.getComment()).thenReturn(Option.none());

        return newActivityItem(item);
    }

    private AggregatedJiraActivityItem newActivityItemWithComment(GenericValue... changeItems) {
        JiraActivityItem item = newJiraActivityItem(changeItems);

        Comment mockComment = mock(Comment.class);
        Option<Comment> comment = some(mockComment);
        when(item.getComment()).thenReturn(comment);

        return newActivityItem(item);
    }

    private GenericValue newChangeItem(String field)
    {
        GenericValue changeItem = mock(GenericValue.class);
        when(changeItem.getString("field")).thenReturn(field);
        return changeItem;
    }

    private ActivityObject newActivityObject()
    {
        return new ActivityObject(ActivityObject.params().
                id("urn:1").
                alternateLinkUri(URI.create(uri)).
                activityObjectType(issue()));
    }

    private UserProfile newUserProfile()
    {
        return mock(UserProfile.class);
    }
}
