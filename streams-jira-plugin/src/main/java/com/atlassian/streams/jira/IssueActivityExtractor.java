package com.atlassian.streams.jira;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.changehistory.ChangeHistory;
import com.atlassian.jira.issue.comments.Comment;
import com.atlassian.jira.issue.comments.CommentManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.ApplicationUsers;
import com.atlassian.streams.api.ActivityObjectType;
import com.atlassian.streams.api.ActivityVerb;
import com.atlassian.streams.api.common.Option;
import com.atlassian.streams.api.common.Pair;
import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableList;
import org.apache.commons.lang3.StringUtils;
import org.ofbiz.core.entity.GenericValue;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import static com.atlassian.jira.issue.IssueFieldConstants.DESCRIPTION;
import static com.atlassian.jira.issue.IssueFieldConstants.SUMMARY;
import static com.atlassian.streams.api.ActivityObjectTypes.comment;
import static com.atlassian.streams.api.ActivityVerbs.post;
import static com.atlassian.streams.api.common.Option.none;
import static com.atlassian.streams.api.common.Option.option;
import static com.atlassian.streams.api.common.Option.some;
import static com.atlassian.streams.api.common.Pair.pair;
import static com.atlassian.streams.jira.ChangeItems.getChangeItems;
import static com.atlassian.streams.jira.JiraActivityObjectTypes.issue;
import static com.google.common.collect.Iterables.filter;
import static org.apache.commons.lang3.StringUtils.isBlank;

class IssueActivityExtractor
{
    private final ApplicationUser user;

    private final CommentManager commentManager;
    private final JiraHelper helper;
    private final ImmutableList.Builder<JiraActivityItem> builder;
    private final JiraStreamsActivityProvider.ChangeHistoryQuery changeHistories;

    private final Predicate<String> inUsers;
    private final Predicate<Date> containsDate;
    private final Predicate<Pair<ActivityObjectType, ActivityVerb>> inJiraActivities;

    IssueActivityExtractor(ApplicationUser user, CommentManager commentManager, JiraHelper helper,
            ImmutableList.Builder<JiraActivityItem> builder, JiraStreamsActivityProvider.ChangeHistoryQuery changeHistories,
            Predicate<String> inUsers, Predicate<Date> containsDate,
            Predicate<Pair<ActivityObjectType, ActivityVerb>> inJiraActivities)
    {
        this.user = user;
        this.commentManager = commentManager;
        this.helper = helper;
        this.builder = builder;
        this.changeHistories = changeHistories;
        this.inUsers = inUsers;
        this.containsDate = containsDate;
        this.inJiraActivities = inJiraActivities;
    }

    public void extract(Issue issue) {
        final List<ChangeHistory> histories = changeHistories.sortedChangeHistories(issue);
        Option<String> issueSummary = getInitialSummary(issue, histories);

        // Put the issue in the list, it's created
        if (issue.getCreator() != null && inUsers.apply(issue.getCreator().getName()) &&
                containsDate.apply(issue.getCreated()) &&
                inJiraActivities.apply(pair(issue(), post()))) {
            builder.add(new JiraActivityItem(issue,
                    issueSummary.getOrElse(""),
                    pair(issue(), post()),
                    getInitialDescription(issue, histories)));
        }

        // Put comments into a new list, so that we can remove them if they are associated with a change
        final List<Comment> comments = new ArrayList<Comment>();
        comments.addAll(commentManager.getCommentsForUser(issue, user));
        // Iterate through history
        for (final ChangeHistory changeHistory : histories) {
            Option<Pair<ActivityObjectType, ActivityVerb>> activityOption = helper.jiraActivity(changeHistory);

            if (activityOption.isDefined()) {
                Pair<ActivityObjectType, ActivityVerb> activity = activityOption.get();

                if (containsDate.apply(changeHistory.getTimePerformed()) &&
                        inJiraActivities.apply(activity)) {
                    // STRM-1363 - use the issue summary at the time of the change history
                    issueSummary = option(getNewSummaryIfChanged(changeHistory).getOrElse(issueSummary.get()));

                    if (inUsers.apply(getUserNameFromApplicationUser(changeHistory.getAuthorObject()))) {
                        // See if there is an associated comment
                        final Iterator<Comment> iter = comments.iterator();
                        Option<Comment> comment = none();
                        while (iter.hasNext()) {
                            final Comment c = iter.next();
                            if (areRelated(changeHistory, c)) {
                                iter.remove();
                                comment = some(c);
                                break;
                            }
                        }
                        if (comment.isDefined()) {
                            builder.add(new JiraActivityItem(issue, issueSummary.getOrElse(""), activity, comment
                                    .get(), changeHistory));
                        } else {
                            builder.add(new JiraActivityItem(issue,
                                    issueSummary.getOrElse(""),
                                    activity,
                                    changeHistory));
                        }
                    }
                }
            }
        }
        // Iterate through the remaining comments
        for (final Comment comment : comments)
        {
            if (inUsers.apply(getUserNameFromApplicationUser(comment.getAuthorApplicationUser())) &&
                    containsDate.apply(comment.getCreated()) &&
                    inJiraActivities.apply(pair(comment(), post())))
            {
                builder.add(new JiraActivityItem(issue, issueSummaryAtTimeOfComment(comment, issue, histories)
                        .getOrElse(""),
                        pair(comment(), post()), comment));
            }
        }
    }


    private Option<String> issueSummaryAtTimeOfComment(Comment comment, Issue issue, Iterable<ChangeHistory> histories)
    {
        String issueSummary = null;
        long commentCreateTime = comment.getCreated().getTime();

        for (final ChangeHistory changeHistory : histories)
        {
            long changeHistoryCreateTime = changeHistory.getTimePerformed().getTime();
            if (changeHistoryCreateTime < commentCreateTime || isCloseEnough(changeHistoryCreateTime, commentCreateTime))
            {
                for (String newSummary : getNewSummaryIfChanged(changeHistory))
                {
                    issueSummary = newSummary;
                }
            }
        }

        if (!isBlank(issueSummary))
        {
            return option(issueSummary);
        }

        // summary was not changed before or during the the of comment
        return getInitialSummary(issue, histories);
    }

    private Option<String> getInitialSummary(Issue issue, Iterable<ChangeHistory> histories)
    {
        for (final ChangeHistory changeHistory : histories)
        {
            for (String oldSummary : getSummaryIfChanged(changeHistory, true))
            {
                return option(oldSummary);
            }
        }
        return option(issue.getSummary());
    }

    private Option<String> getNewSummaryIfChanged(ChangeHistory changeHistory)
    {
        return getSummaryIfChanged(changeHistory, false);
    }

    private Option<String> getSummaryIfChanged(ChangeHistory changeHistory, boolean old)
    {
        for (GenericValue changeItem : filter(getChangeItems(changeHistory), summary))
        {
            String summary = changeItem.getString(old ? "oldstring" : "newstring");
            if (!isBlank(summary))
            {
                return some(summary);
            }
        }
        return none();
    }

    private Option<String> getInitialDescription(Issue issue, Iterable<ChangeHistory> histories)
    {
        for (final ChangeHistory changeHistory : histories)
        {
            for (GenericValue changeItem : filter(getChangeItems(changeHistory), description))
            {
                return option(changeItem.getString("oldstring"));
            }
        }
        return option(issue.getDescription());
    }

    private final Predicate<GenericValue> summary = new Predicate<GenericValue>()
    {
        @Override
        public boolean apply(GenericValue item)
        {
            return SUMMARY.equals(item.getString("field").toLowerCase());
        }
    };

    private final Predicate<GenericValue> description = new Predicate<GenericValue>()
    {
        @Override
        public boolean apply(GenericValue item)
        {
            return DESCRIPTION.equals(item.getString("field").toLowerCase());
        }
    };

    private static boolean areRelated(final ChangeHistory change, final Comment comment)
    {
        return StringUtils.equals(ApplicationUsers.getKeyFor(comment.getAuthorApplicationUser()), change.getAuthor()) &&
                isCloseEnough(comment.getCreated().getTime(), change.getTimePerformed().getTime());
    }

    private static boolean isCloseEnough(long time1, long time2)
    {
        return Math.abs(time1 - time2) < JiraStreamsActivityProvider.CLOSE_ENOUGH_TIME_LIMIT_MS;
    }

    private String getUserNameFromApplicationUser(ApplicationUser user)
    {
        if (user == null)
        {
            return null;
        }
        else
        {
            return user.getUsername();
        }
    }
}
