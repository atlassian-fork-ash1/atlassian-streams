package com.atlassian.streams.jira;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.jira.issue.vote.VoteManager;
import com.atlassian.jira.issue.watchers.WatcherManager;
import com.atlassian.jira.permission.ProjectPermissions;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.user.ApplicationUser;

import static com.google.common.collect.Iterables.size;
import static java.util.Objects.requireNonNull;

public class JiraInlineActionHandlerImpl implements JiraInlineActionHandler
{
    private final WatcherManager watcherManager;
    private final IssueManager issueManager;
    private final JiraAuthenticationContext authenticationContext;
    private final VoteManager voteManager;
    private final PermissionManager permissionManager;

    public JiraInlineActionHandlerImpl(final WatcherManager watcherManager,
                                       final IssueManager issueManager,
                                       final JiraAuthenticationContext authenticationContext,
                                       final VoteManager voteManager,
                                       final PermissionManager permissionManager)
    {
        this.watcherManager = requireNonNull(watcherManager, "watcherManager");
        this.issueManager = requireNonNull(issueManager, "issueManager");
        this.authenticationContext = requireNonNull(authenticationContext, "authenticationContext");
        this.voteManager = requireNonNull(voteManager, "voteManager");
        this.permissionManager = requireNonNull(permissionManager, "permissionManager");
    }
    
    public boolean startWatching(String issueKey)
    {
        ApplicationUser user = authenticationContext.getLoggedInUser();
        Issue issue = issueManager.getIssueObject(issueKey);

        // Don't let users watch issues they can't see.
        if (!permissionManager. hasPermission(ProjectPermissions.BROWSE_PROJECTS, issue, user)) {
            return false;
        }

        int previousWatcherCount = size(watcherManager.getCurrentWatcherUsernames(issue));
        watcherManager.startWatching(user, issue);
        int updatedWatcherCount = size(watcherManager.getCurrentWatcherUsernames(issue));
        return updatedWatcherCount == previousWatcherCount + 1;
    }

    public boolean hasPreviouslyWatched(String issueKey)
    {
        ApplicationUser user = authenticationContext.getLoggedInUser();
        Issue issue = issueManager.getIssueObject(issueKey);

        return watcherManager.isWatching(user, issue);
    }

    public boolean voteOnIssue(String issueKey)
    {
        ApplicationUser user = authenticationContext.getLoggedInUser();
        Issue issue = issueManager.getIssueObject(issueKey);

        // Don't let users vote on issues they can't see.
        if (!permissionManager. hasPermission(ProjectPermissions.BROWSE_PROJECTS, issue, user)) {
            return false;
        }

        return voteManager.addVote(user, issue);
    }

    public boolean hasPreviouslyVoted(String issueKey)
    {
        ApplicationUser user = authenticationContext.getLoggedInUser();
        Issue issue = issueManager.getIssueObject(issueKey);

        return voteManager.hasVoted(user, issue);
    }
}
