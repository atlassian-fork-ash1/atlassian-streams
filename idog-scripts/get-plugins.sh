#!/bin/sh

set -e

PID=$$ # get PID of current process
LogUID=`cat /proc/"$PID"/loginuid` # get loginuid of current process
username=`getent passwd "$LogUID" | awk -F ":" '{print $1}'` # translate loginuid to username

default_plugins="io.atlassian.util.concurrent:atlassian-util-concurrent:4.0.1                           \
                 com.atlassian.upm:atlassian-universal-plugin-manager-plugin:1.6-m2                     \
                 com.atlassian.bundles:google-collections:1.0                                           \
                 com.atlassian.security.auth.trustedapps:atlassian-trusted-apps-api:3.0.0-plugin2       \
                 com.atlassian.security.auth.trustedapps:atlassian-trusted-apps-plugin:3.0.0-plugin2    \
                 com.atlassian.jira.plugins:atlassian-jira-rpc-plugin:5.0-alpha3                        \
                 com.atlassian.activeobjects:activeobjects-plugin:0.15.9                                \
                 com.atlassian.activeobjects:activeobjects-jira-spi:0.15.9                              \
                 com.atlassian.activeobjects:activeobjects-bamboo-spi:0.15.9                            \
                 com.atlassian.activeobjects:activeobjects-confluence-spi:0.15.9"

jars=""

for details in $default_plugins
do
  path=`echo $details | awk '{ print substr($0, 0, index($0, ":")-1 ) }' | sed 's/\./\//g'`
  plugin=`echo $details | awk '{ print substr($0, index($0, ":")+1, length($0)) }' | awk '{ print substr($0, 0, index($0, ":")-1) }'`
  version=`echo $details | awk '{ print substr($0, index($0, ":")+1, length($0)) }' | awk '{ print substr($0, index($0, ":")+1, length($0)) }'`
  jars="$jars --remote-name https://maven.atlassian.com/service/local/repo_groups/internal/content/$path/$plugin/$version/$plugin-$version.jar"
done

curl --user $username $jars
f