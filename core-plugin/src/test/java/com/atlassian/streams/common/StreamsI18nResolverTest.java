package com.atlassian.streams.common;

import java.util.Locale;

import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.sal.api.message.LocaleResolver;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class StreamsI18nResolverTest
{
    private static final String ENGLISH_NAME = "English";
    private static final String FRENCH_NAME = "Fran\u00e7ais";
    private static final String LANGUAGE_KEY = "lang";

    @Mock
    private I18nResolver i18nResolver;
    @Mock
    private LocaleResolver localeResolver;
    private StreamsI18nResolverImpl streamsI18nResolver;
    
    @Before
    public void setUp()
    {
        streamsI18nResolver = new StreamsI18nResolverImpl(i18nResolver, localeResolver);
        when(localeResolver.getSupportedLocales()).thenReturn(ImmutableSet.of(Locale.getDefault(),
                                                                              Locale.FRANCE,
                                                                              Locale.GERMANY,
                                                                              Locale.JAPAN));
        when(i18nResolver.getAllTranslationsForPrefix(LANGUAGE_KEY, Locale.FRANCE)).thenReturn(ImmutableMap.of(LANGUAGE_KEY, FRENCH_NAME));
        when(i18nResolver.getText(LANGUAGE_KEY)).thenReturn(ENGLISH_NAME);
    }

    @Test
    public void testRequestLanguagesForFrenchFrench()
    {
        streamsI18nResolver.setRequestLanguages("fr-fr, fr;q=0.9, en-gb;q=0.8, en;q=0.7, *;q=0.5");
        assertThat(streamsI18nResolver.getText(LANGUAGE_KEY), is(equalTo(FRENCH_NAME)));
    }
    
    @Test
    public void testRequestLanguagesForCanadianFrench()
    {
        streamsI18nResolver.setRequestLanguages("fr-ca, fr;q=0.9, en-ca;q=0.8, en;q=0.7, *;q=0.5");
        assertThat(streamsI18nResolver.getText(LANGUAGE_KEY), is(equalTo(FRENCH_NAME)));
    }

    @Test
    public void testRequestLanguagesForDefaultLocale()
    {
        // Catalan or Spanish
        streamsI18nResolver.setRequestLanguages("ca-es, ca;q=0.9, es-es;q=0.8, es;q=0.7, *;q=0.5");
        assertThat(streamsI18nResolver.getText(LANGUAGE_KEY), is(equalTo(ENGLISH_NAME)));
    }

    @Test
    public void testSetRequestLanguageWithNullRestoresDefaultLocale()
    {
        // Set StreamsI18nResolver to a French locale
        streamsI18nResolver.setRequestLanguages("fr-fr, fr;q=0.9, en-gb;q=0.8, en;q=0.7, *;q=0.5");
        assertThat(streamsI18nResolver.getText(LANGUAGE_KEY), is(equalTo(FRENCH_NAME)));

        streamsI18nResolver.setRequestLanguages(null);
        assertThat(streamsI18nResolver.getText(LANGUAGE_KEY), is(equalTo(ENGLISH_NAME)));
    }

}
