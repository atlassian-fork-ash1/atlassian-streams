package com.atlassian.streams.confluence;

import java.net.URI;

import com.atlassian.confluence.security.SpacePermission;
import com.atlassian.confluence.security.SpacePermissionManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.streams.spi.StreamsEntityAssociationProvider;
import com.atlassian.streams.testing.AbstractEntityAssociationProviderTest;

import org.junit.Before;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static com.atlassian.streams.confluence.ConfluenceActivityObjectTypes.space;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class ConfluenceEntityAssociationProviderTest extends AbstractEntityAssociationProviderTest
{
    @Mock
    private SpaceManager spaceManager;
    @Mock
    private SpacePermissionManager spacePermissionManager;
    @Mock
    private ConfluenceUser user;
    @Mock
    private Space space;

    @Before
    public void setup()
    {
        AuthenticatedUserThreadLocal.set(user);
    }

    @Override
    public StreamsEntityAssociationProvider createProvider()
    {
        return new ConfluenceEntityAssociationProvider(applicationProperties, spaceManager, spacePermissionManager);
    }

    @Override
    protected String getProjectUriPath(String key)
    {
        return "/display/" + key;
    }

    @Override
    protected URI getProjectEntityType()
    {
        return space().iri();
    }

    @Override
    protected void setProjectExists(String key, boolean exists)
    {
        when(spaceManager.getSpace(key)).thenReturn(exists ? space : null);
    }

    @Override
    protected void setProjectViewPermission(String key, boolean permitted)
    {
        when(spaceManager.getSpace(key)).thenReturn(space);
        when(spacePermissionManager.hasPermission(SpacePermission.VIEWSPACE_PERMISSION, space, user)).thenReturn(permitted);
    }

    @Override
    protected void setProjectEditPermission(String key, boolean permitted)
    {
        when(spaceManager.getSpace(key)).thenReturn(space);
        when(spacePermissionManager.hasPermission(SpacePermission.COMMENT_PERMISSION, space, user)).thenReturn(permitted);
    }
}
