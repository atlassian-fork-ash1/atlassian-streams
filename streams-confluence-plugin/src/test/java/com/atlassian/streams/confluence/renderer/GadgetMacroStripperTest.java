package com.atlassian.streams.confluence.renderer;

import com.atlassian.confluence.content.render.xhtml.DefaultXmlEventReaderFactory;
import com.atlassian.confluence.content.render.xhtml.DelegateXmlOutputFactory;
import com.atlassian.confluence.content.render.xhtml.XmlOutputFactoryFactoryBean;
import org.junit.Ignore;
import org.junit.Test;

import javax.xml.stream.XMLOutputFactory;

import static org.junit.Assert.assertEquals;

@Ignore
public class GadgetMacroStripperTest
{
    private final String CONTENT = "&ndash;&nbsp;&Egrave;";
    private final String GADGET_MACRO = "<ac:macro name=\"gadget\"></ac:macro>";
    private final String RESULT = "<div>" + CONTENT + "</div>";

    @Test
    public void testStripGadgetMacrosWithoutGadgetMacro() throws Exception
    {
        GadgetMacroStripper stripper = new GadgetMacroStripper(new DefaultXmlEventReaderFactory(), new DelegateXmlOutputFactory((XMLOutputFactory) new XmlOutputFactoryFactoryBean(true).getObject()));
        String stripped = stripper.stripGadgetMacros(CONTENT);
        assertEquals(RESULT, stripped);
    }

    @Test
    public void testStripGadgetMacrosWithGadgetMacro() throws Exception
    {
        GadgetMacroStripper stripper = new GadgetMacroStripper(new DefaultXmlEventReaderFactory(), new DelegateXmlOutputFactory((XMLOutputFactory) new XmlOutputFactoryFactoryBean(true).getObject()));
        String stripped = stripper.stripGadgetMacros(CONTENT+GADGET_MACRO);
        assertEquals(RESULT, stripped);
    }
}
