package com.atlassian.streams.confluence;

import java.net.URI;

import com.atlassian.confluence.security.SpacePermission;
import com.atlassian.confluence.security.SpacePermissionManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.confluence.user.actions.ProfilePictureInfo;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.user.UserProfile;
import com.atlassian.streams.spi.StreamsI18nResolver;
import com.atlassian.streams.testing.AbstractUserProfileAccessorTestSuite;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static com.atlassian.streams.api.common.Option.some;
import static com.atlassian.streams.api.common.uri.Uris.encode;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ConfluenceUserProfileAccessorTest extends AbstractUserProfileAccessorTestSuite
{
    private static final URI BASE_URI = URI.create("http://localhost/streams");
    private static final String PROFILE_PICTURE_PATH = "/download/attachments/393217/{profilePictureParameter}-34270-pp-{profilePictureParameter}.jpg";
    private static final String USER = "user";
    private static final String USER_WITH_SPACE_IN_USERNAME = "user 2";
    private static final String USER_WITH_FUNNY_CHARACTERS_IN_NAME = "user3";

    @Mock
    private UserManager userManager;
    @Mock
    private SpacePermissionManager spacePermissionManager;
    @Mock
    private StreamsI18nResolver i18nResolver;

    @Before
    public void createUserProfileBuilder()
    {
        userProfileAccessor = new ConfluenceUserProfileAccessor(userManager, getApplicationProperties(), i18nResolver, spacePermissionManager);
    }

    @Before
    public void setup()
    {
        prepareUserManager();
    }

    public void prepareUserManager()
    {
        ConfluenceUser currentUser = mock(ConfluenceUser.class);
        AuthenticatedUserThreadLocal.set(currentUser);
        
        prepareUser(USER, "User", "u@c.com", URI.create(PROFILE_PICTURE_PATH.replace("{profilePictureParameter}", encode(USER))));
        prepareUser(USER_WITH_SPACE_IN_USERNAME, "User 2", "u2@c.com", URI.create(PROFILE_PICTURE_PATH.replace("{profilePictureParameter}", encode(USER_WITH_SPACE_IN_USERNAME))));
        prepareUser(USER_WITH_FUNNY_CHARACTERS_IN_NAME, "User <3&'>", "u3@c.com", URI.create(PROFILE_PICTURE_PATH.replace("{profilePictureParameter}", encode(USER_WITH_FUNNY_CHARACTERS_IN_NAME))));
    }
    
    private void prepareUser(String username, String fullName, String email, URI avatarUrl)
    {
        UserProfile user = mock(UserProfile.class);
        when(user.getFullName()).thenReturn(fullName);
        when(user.getEmail()).thenReturn(email);
        when(user.getProfilePictureUri()).thenReturn(avatarUrl);
        when(userManager.getUserProfile(username)).thenReturn(user);        
    }

    @Override
    protected String getProfilePathTemplate()
    {
        return "/display/~{username}";
    }

    @Override
    protected String getProfilePicturePathTemplate()
    {
        return PROFILE_PICTURE_PATH;
    }

    @Test
    public void assertThatProfilePictureUriIsNotEncodedTwice()
    {
        String userWithEncodedAvatar = "John";
        String encodedAvatarUrl = "/user%202.jpg";
        prepareUser(userWithEncodedAvatar, userWithEncodedAvatar, "john@c.com", URI.create(encodedAvatarUrl));
        
        assertThat(userProfileAccessor.getUserProfile(BASE_URI, userWithEncodedAvatar).getProfilePictureUri(),
                is(equalTo(some(URI.create(getProfilePicApplicationBaseUrl() + encodedAvatarUrl)))));
    }

    @Test
    public void assertThatDefaultPictureIsReturnedForAnonymousUser()
    {
        AuthenticatedUserThreadLocal.set(null);
        when(spacePermissionManager.hasPermission(SpacePermission.VIEW_USER_PROFILES_PERMISSION, null, null)).thenReturn(false);
        assertThat(userProfileAccessor.getUserProfile(BASE_URI, USER).getProfilePictureUri(),
                 is(equalTo(some(URI.create(getProfilePicApplicationBaseUrl() + ProfilePictureInfo.DEFAULT_PROFILE_PATH)))));
    }

    @Test
    public void assertThatDefaultPictureIfUserDoesNotHaveAvatarInConfluence()
    {
        String userWithoutAvatar = "James";
        prepareUser(userWithoutAvatar, userWithoutAvatar, "james@c.com", null);
        
        assertThat(userProfileAccessor.getUserProfile(BASE_URI, userWithoutAvatar).getProfilePictureUri(),
                is(equalTo(some(URI.create(getProfilePicApplicationBaseUrl() + ProfilePictureInfo.DEFAULT_PROFILE_PATH)))));
    }

    @Test
    public void assertThatProfilePictureIsNotChangedIfItsAbsolute()
    {
        String userWithGravatar = "Matt";
        URI mattsGravatar = URI.create("http://gravatar.com/767fc9c115a1b989744c755db47feb60");
        prepareUser(userWithGravatar, userWithGravatar, "matt@c.com", mattsGravatar);

        assertThat(userProfileAccessor.getUserProfile(BASE_URI, userWithGravatar).getProfilePictureUri(),
                is(equalTo(some(mattsGravatar))));
    }
}
