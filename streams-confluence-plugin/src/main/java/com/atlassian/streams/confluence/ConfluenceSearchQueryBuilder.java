package com.atlassian.streams.confluence;

import com.atlassian.confluence.search.service.ContentTypeEnum;
import com.atlassian.confluence.search.v2.SearchQuery;
import com.atlassian.confluence.search.v2.query.BooleanQuery;
import com.atlassian.confluence.search.v2.query.ContentTypeQuery;
import com.atlassian.confluence.search.v2.query.ContributorQuery;
import com.atlassian.confluence.search.v2.query.CreatorQuery;
import com.atlassian.confluence.search.v2.query.DateRangeQuery;
import com.atlassian.confluence.search.v2.query.InSpaceQuery;
import com.atlassian.confluence.search.v2.query.MultiTextFieldQuery;
import com.atlassian.streams.api.ActivityObjectType;
import com.atlassian.streams.api.common.Option;
import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.ImmutableSet.Builder;

import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.atlassian.confluence.search.service.ContentTypeEnum.ATTACHMENT;
import static com.atlassian.confluence.search.service.ContentTypeEnum.BLOG;
import static com.atlassian.confluence.search.service.ContentTypeEnum.COMMENT;
import static com.atlassian.confluence.search.service.ContentTypeEnum.PAGE;
import static com.atlassian.confluence.search.service.ContentTypeEnum.PERSONAL_SPACE_DESCRIPTION;
import static com.atlassian.confluence.search.service.ContentTypeEnum.SPACE_DESCRIPTION;
import static com.atlassian.confluence.search.v2.BooleanOperator.OR;
import static com.atlassian.confluence.search.v2.query.BooleanQuery.composeOrQuery;
import static com.atlassian.confluence.search.v2.query.DateRangeQuery.DateRangeQueryType.MODIFIED;
import static com.atlassian.streams.api.ActivityObjectTypes.article;
import static com.atlassian.streams.api.ActivityObjectTypes.comment;
import static com.atlassian.streams.api.ActivityObjectTypes.file;
import static com.atlassian.streams.api.common.Option.none;
import static com.atlassian.streams.api.common.Option.option;
import static com.atlassian.streams.api.common.Option.some;
import static com.atlassian.streams.api.common.Options.catOptions;
import static com.atlassian.streams.confluence.ConfluenceActivityObjectTypes.page;
import static com.atlassian.streams.confluence.ConfluenceActivityObjectTypes.personalSpace;
import static com.atlassian.streams.confluence.ConfluenceActivityObjectTypes.space;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Iterables.addAll;
import static com.google.common.collect.Iterables.concat;
import static com.google.common.collect.Iterables.isEmpty;
import static com.google.common.collect.Iterables.transform;
import static com.google.common.collect.Sets.newHashSet;
import static com.google.common.collect.Sets.newLinkedHashSet;
import static java.util.Arrays.asList;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.trimToNull;

public class ConfluenceSearchQueryBuilder
{
    /**
     * All possible content types to be found by the confluence streams plugin
     */
    static final List<ContentTypeEnum> CONTENT_TYPES =
        ImmutableList.of(PAGE, BLOG, COMMENT, ATTACHMENT, SPACE_DESCRIPTION, PERSONAL_SPACE_DESCRIPTION);

    // names of the text fields used by Confluence internally, will break if any is renamed by Confluence
    static final Set<String> TEXT_FIELDS = ImmutableSet.of(
            "title", "labelText", "contentBody", "filename", "username", "fullName", "email", "from", "recipients");

    private final Set<String> createdByUsers = newLinkedHashSet();
    private final Set<String> lastModifiedByUsers = newLinkedHashSet();
    private final Set<String> spaceKeys = newLinkedHashSet();
    private final Set<String> searchTerms = newLinkedHashSet();
    private final Set<String> excludedSearchTerms = newLinkedHashSet();
    private final Set<ActivityObjectType> activityObjects = newLinkedHashSet();
    private Option<Date> minDate = none();
    private Option<Date> maxDate = none();

    public ConfluenceSearchQueryBuilder createdOrLastModifiedBy(Iterable<String> userNames)
    {
        return createdBy(userNames).lastModifiedBy(userNames);
    }

    public ConfluenceSearchQueryBuilder createdOrLastModifiedBy(String... userNames)
    {
        return createdOrLastModifiedBy(asList(userNames));
    }

    public ConfluenceSearchQueryBuilder createdBy(Iterable<String> userNames)
    {
        addAll(createdByUsers, catOptions(transform(userNames, StringTrimmer.INSTANCE)));
        return this;
    }

    public ConfluenceSearchQueryBuilder createdBy(String... userNames)
    {
        return createdBy(asList(userNames));
    }

    public ConfluenceSearchQueryBuilder lastModifiedBy(Iterable<String> userNames)
    {
        addAll(lastModifiedByUsers, catOptions(transform(userNames, StringTrimmer.INSTANCE)));
        return this;
    }

    public ConfluenceSearchQueryBuilder lastModifiedBy(String... userNames)
    {
        return lastModifiedBy(asList(userNames));
    }

    public ConfluenceSearchQueryBuilder inSpace(Iterable<String> spaceKeys)
    {
        addAll(this.spaceKeys, catOptions(transform(spaceKeys, StringTrimmer.INSTANCE)));
        return this;
    }

    public ConfluenceSearchQueryBuilder inSpace(String... spaceKeys)
    {
        return inSpace(asList(spaceKeys));
    }

    public ConfluenceSearchQueryBuilder searchFor(Iterable<String> searchTerms)
    {
        addAll(this.searchTerms, catOptions(transform(searchTerms, StringTrimmer.INSTANCE)));
        return this;
    }

    public ConfluenceSearchQueryBuilder searchFor(String... searchTerms)
    {
        return searchFor(asList(searchTerms));
    }

    public ConfluenceSearchQueryBuilder excludeTerms(Iterable<String> excludedSearchTerms)
    {
        addAll(this.excludedSearchTerms, catOptions(transform(excludedSearchTerms, StringTrimmer.INSTANCE)));
        return this;
    }

    public ConfluenceSearchQueryBuilder minDate(Option<Date> minDate)
    {
        this.minDate = minDate;
        return this;
    }

    public ConfluenceSearchQueryBuilder maxDate(Option<Date> maxDate)
    {
        this.maxDate = maxDate;
        return this;
    }

    public ConfluenceSearchQueryBuilder activityObjects(Iterable<ActivityObjectType> activityObjects)
    {
        addAll(this.activityObjects, activityObjects);
        return this;
    }

    public SearchQuery build()
    {
        AndBooleanQueryBuilder finalQuery = new AndBooleanQueryBuilder(new ContentTypeQuery(getContentTypes(activityObjects)));

        if (!createdByUsers.isEmpty() || !lastModifiedByUsers.isEmpty())
        {
            finalQuery.and(composeOrQuery(ImmutableSet.copyOf(concat(
                    catOptions(transform(createdByUsers, ToCreatorQuery.INSTANCE)),
                    catOptions(transform(lastModifiedByUsers, ToLastModifierQuery.INSTANCE))))));
        }
        if (!isEmpty(spaceKeys))
        {
            finalQuery.and(new InSpaceQuery(new HashSet<>(spaceKeys)));
        }
        if (!isEmpty(searchTerms))
        {
            finalQuery.and(buildMultiTextFieldQuery(searchTerms));
        }
        if (!isEmpty(excludedSearchTerms))
        {
            finalQuery.not(buildMultiTextFieldQuery(excludedSearchTerms));
        }
        if (minDate.isDefined() || maxDate.isDefined())
        {
            finalQuery.and(new DateRangeQuery(minDate.getOrElse((Date) null), maxDate.getOrElse((Date) null), true, true, MODIFIED));
        }
        return finalQuery.build();
    }

    private static SearchQuery buildMultiTextFieldQuery(Set<String> keywords)
    {
        StringBuilder sb = new StringBuilder();
        for (String keyword : keywords)
        {
            if (sb.length() > 0)
            {
                sb.append(' ');
            }
            sb.append(keyword);
        }
        return new MultiTextFieldQuery(sb.toString(), TEXT_FIELDS, OR);
    }

    private static Collection<ContentTypeEnum> getContentTypes(Set<ActivityObjectType> activityObjectTypes)
    {
        Builder<ContentTypeEnum> builder = ImmutableSet.builder();
        for (ActivityObjectType type : activityObjectTypes)
        {
            if (type.equals(article()))
            {
                builder.add(BLOG);
            }
            else if (type.equals(comment()))
            {
                builder.add(COMMENT);
            }
            else if (type.equals(file()))
            {
                builder.add(ATTACHMENT);
            }
            else if (type.equals(page()))
            {
                builder.add(PAGE);
            }
            else if (type.equals(personalSpace()))
            {
                builder.add(PERSONAL_SPACE_DESCRIPTION);
            }
            else if (type.equals(space()))
            {
                builder.add(SPACE_DESCRIPTION);
                // It's not currently possible for the user to specifically request personal-space activity, so search for both here.
                builder.add(PERSONAL_SPACE_DESCRIPTION);
            }
        }
        Set<ContentTypeEnum> types = builder.build();
        return types.isEmpty() ? CONTENT_TYPES : types;
    }

    enum StringTrimmer implements Function<String, Option<String>>
    {
        INSTANCE;

        public Option<String> apply(String s)
        {
            return option(trimToNull(s));
        }
    }

    enum ToCreatorQuery implements Function<String, Option<CreatorQuery>>
    {
        INSTANCE;

        public Option<CreatorQuery> apply(String username)
        {
            if (isBlank(username))
            {
                return none();
            }
            return some(new CreatorQuery(username));
        }
    }

    enum ToLastModifierQuery implements Function<String, Option<ContributorQuery>>
    {
        INSTANCE;

        public Option<ContributorQuery> apply(String username)
        {
            if (isBlank(username))
            {
                return none();
            }
            return some(new ContributorQuery(username));
        }
    }

    static class AndBooleanQueryBuilder
    {
        private final Set<SearchQuery> queries;
        private final Set<SearchQuery> nots = newHashSet();

        private AndBooleanQueryBuilder(ContentTypeQuery contentTypeQuery)
        {
            queries = newHashSet();
            queries.add(contentTypeQuery);
        }

        public void not(SearchQuery query)
        {
            nots.add(query);
        }

        AndBooleanQueryBuilder and(Iterable<SearchQuery> queries)
        {
            addAll(this.queries, checkNotNull(queries, "queries"));
            return this;
        }

        AndBooleanQueryBuilder and(SearchQuery... queries)
        {
            return and(asList(checkNotNull(queries, "Sub queries")));
        }

        AndBooleanQueryBuilder and(AndBooleanQueryBuilder queryBuilder)
        {
            return and(queryBuilder.build());
        }

        SearchQuery build()
        {
            return new BooleanQuery(queries, ImmutableSet.of(), nots);
        }
    }
}
