package com.atlassian.streams.confluence;

import com.atlassian.confluence.mail.notification.Notification;
import com.atlassian.confluence.mail.notification.NotificationManager;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.streams.api.common.Pair;
import com.atlassian.user.User;
import com.google.common.base.Function;

import java.util.List;

import static java.util.Objects.requireNonNull;

public class ConfluenceWatchSpaceInlineActionHandler implements ConfluenceWatchInlineActionHandler<String> {
    private final NotificationManager notificationManager;
    private final SpaceManager spaceManager;
    private final PermissionManager permissionManager;
    private final ConfluenceWatchHelper<Space, Long> watchHelper = new ConfluenceWatchHelper<>();

    public ConfluenceWatchSpaceInlineActionHandler(
            final NotificationManager notificationManager,
            final SpaceManager spaceManager,
            final PermissionManager permissionManager) {
        this.notificationManager = requireNonNull(notificationManager, "notificationManager");
        this.spaceManager = requireNonNull(spaceManager, "spaceManager");
        this.permissionManager = requireNonNull(permissionManager, "permissionManager");
    }

    public boolean startWatching(String key) {
        Space space = spaceManager.getSpace(key);
        User user = AuthenticatedUserThreadLocal.getUser();

        // Note that if space is null, that is, key is invalid, hasPermission is documented to say no permission, so
        // we 412 this which is desirable - it means we don't leak valid space keys
        if (!permissionManager.hasPermission(user, Permission.VIEW, space)) {
            // no permission to see space, don't let them watch it
            return false;
        }

        if (user != null && notificationManager.isUserWatchingPageOrSpace(user, space, null)) {
            //already watching the space. return true
            return true;
        }

        return watchHelper.startWatching(space, addSpaceNotification, getSpaceNotifications);
    }

    private AddSpaceNotification addSpaceNotification = new AddSpaceNotification();

    private class AddSpaceNotification implements Function<Pair<User, Space>, Void> {
        public Void apply(Pair<User, Space> params) {
            notificationManager.addSpaceNotification(params.first(), params.second(), null);
            return null;
        }
    }

    private GetSpaceNotifications getSpaceNotifications = new GetSpaceNotifications();

    private class GetSpaceNotifications implements Function<Space, List<Notification>> {
        public List<Notification> apply(Space entity) {
            return notificationManager.getNotificationsBySpaceAndType(entity, null);
        }
    }
}
