package com.atlassian.streams.confluence.renderer;

import com.atlassian.confluence.spaces.SpaceDescription;
import com.atlassian.streams.api.Html;
import com.atlassian.streams.api.StreamsEntry;
import com.atlassian.streams.api.StreamsEntry.ActivityObject;
import com.atlassian.streams.api.StreamsEntry.Renderer;
import com.atlassian.streams.api.common.Option;
import com.atlassian.streams.api.renderer.StreamsEntryRendererFactory;
import com.atlassian.streams.spi.renderer.Renderers;
import com.atlassian.templaterenderer.TemplateRenderer;
import com.google.common.base.Function;
import com.google.common.collect.ImmutableMap;

import static com.atlassian.streams.api.common.Option.none;
import static com.atlassian.streams.api.common.Option.some;
import static com.atlassian.streams.confluence.ConfluenceActivityObjectTypes.personalSpace;
import static com.atlassian.streams.confluence.ConfluenceActivityObjectTypes.space;
import static com.google.common.base.Preconditions.checkNotNull;

public final class SpaceRendererFactory
{
    private final StreamsEntryRendererFactory rendererFactory;
    private final TemplateRenderer templateRenderer;

    SpaceRendererFactory(StreamsEntryRendererFactory rendererFactory, TemplateRenderer templateRenderer)
    {
        this.rendererFactory = checkNotNull(rendererFactory, "rendererFactory");
        this.templateRenderer = checkNotNull(templateRenderer, "templateRenderer");
    }

    public Renderer newInstance(SpaceDescription space)
    {
        return new SpaceRenderer(space);
    }

    private final class SpaceRenderer implements Renderer
    {
        private final SpaceDescription space;

        public SpaceRenderer(SpaceDescription space)
        {
            this.space = space;
        }

        @Override
        public Option<Html> renderContentAsHtml(StreamsEntry entry)
        {
            return none();
        }

        @Override
        public Option<Html> renderSummaryAsHtml(StreamsEntry entry)
        {
            return none();
        }

        @Override
        public Html renderTitleAsHtml(StreamsEntry entry)
        {
            String key = "streams.item.confluence." +
                (space.isPersonalSpace() ? personalSpace().key() : space().key()) + "." +
                entry.getVerb().key();
            Function<StreamsEntry, Html> titleRenderer = rendererFactory.newTitleRenderer(key,
                    rendererFactory.newAuthorsRenderer(),
                    some(rendererFactory.newActivityObjectsRenderer(spaceObjectRenderer())),
                    Option.none());
            return titleRenderer.apply(entry);
        }

        private Function<ActivityObject, Option<Html>> spaceObjectRenderer()
        {
            return o -> some(new Html(Renderers.render(templateRenderer,
                    "activity-object-link-space.vm",
                    ImmutableMap.of(
                            "activityObject", o,
                            "isPersonalSpace", o.getActivityObjectType().equals(some(personalSpace()))
                    ))));
        }
    }
}
