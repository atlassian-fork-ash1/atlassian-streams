package com.atlassian.streams.confluence;

import com.atlassian.confluence.core.ConfluenceEntityObject;
import com.atlassian.confluence.pages.AbstractPage;
import com.atlassian.hibernate.PluginHibernateSessionFactory;
import com.atlassian.streams.spi.Evictor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.sf.hibernate.HibernateException;
import net.sf.hibernate.Session;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Evicts {@link ConfluenceEntityObject} instances from Confluence's Hibernate {@link Session}.
 *
 * According to the comment thread on STRM-1518, too many page revisions are being held
 * in Hibernate's cache. We need to evict them from the cache to allow them to be GC'ed.
 */
public class ConfluenceEvictor implements Evictor<ConfluenceEntityObject>
{
    private static final Logger log = LoggerFactory.getLogger(ConfluenceEvictor.class);
    private final PluginHibernateSessionFactory sessionFactory;

    public ConfluenceEvictor(PluginHibernateSessionFactory sessionFactory)
    {
        this.sessionFactory = checkNotNull(sessionFactory, "sessionFactory");
    }

    /**
     * Evicts the given instance from the current {@link Session}.
     *
     * @param entity the instance to evict.
     */
    @Override
    public Void apply(ConfluenceEntityObject entity)
    {
        if (!(entity instanceof AbstractPage))
        {
            return null;
        }

        try
        {
            sessionFactory.getSession().evict(entity);
        }
        catch (HibernateException e)
        {
            log.warn("Failed to evict the entity from the session", e);
        }
        return null;
    }
}
