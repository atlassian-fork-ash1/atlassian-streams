package com.atlassian.streams.confluence.changereport;

import com.atlassian.confluence.pages.AbstractPage;
import com.atlassian.confluence.pages.Comment;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.user.UserAccessor;

import com.google.common.base.Predicate;

import static com.google.common.base.Preconditions.checkNotNull;

public class CanCommentPredicateFactory
{
    private final PermissionManager permissionManager;
    private final UserAccessor userAccessor;

    CanCommentPredicateFactory(PermissionManager permissionManager,
                                        UserAccessor userAccessor)
    {
        this.permissionManager = checkNotNull(permissionManager, "permissionManager");
        this.userAccessor = checkNotNull(userAccessor, "userAccessor");
    }

    Predicate<String> canCommentOn(final AbstractPage abstractPage)
    {
        return username -> permissionManager.hasCreatePermission(userAccessor.getUserByName(username), abstractPage, Comment.class);
    }

    Predicate<String> canCommentOn(final Comment comment)
    {
        return username -> permissionManager.hasCreatePermission(userAccessor.getUserByName(username), comment.getContainer(), Comment.class);
    }
}
