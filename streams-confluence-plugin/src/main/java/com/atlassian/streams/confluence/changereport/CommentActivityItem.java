package com.atlassian.streams.confluence.changereport;

import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.core.SpaceContentEntityObject;
import com.atlassian.confluence.pages.Comment;
import com.atlassian.streams.api.ActivityVerb;
import com.atlassian.streams.api.StreamsEntry.ActivityObject;
import com.atlassian.streams.api.StreamsEntry.Renderer;
import com.atlassian.streams.api.common.Option;
import com.google.common.base.Predicate;

import static com.atlassian.streams.api.ActivityVerbs.post;
import static com.atlassian.streams.api.common.Option.none;
import static com.atlassian.streams.api.common.Option.option;

final class CommentActivityItem extends ContentEntityActivityItem
{
    private final Comment comment;

    public CommentActivityItem(Comment comment,
           Iterable<ActivityObject> activityObjects,
           Option<ActivityObject> target,
           Renderer renderer,
           Predicate<String> canCommentPredicate)
    {
        super(comment, activityObjects, target, renderer, canCommentPredicate);
        this.comment = comment;
    }

    @Override
    public String getIconPath()
    {
        return "/images/icons/comment_16.gif";
    }

    @Override
    public Option<String> getSpaceKey()
    {
        final ContentEntityObject container = comment.getContainer();
        if (container instanceof SpaceContentEntityObject)
        {
            return option(((SpaceContentEntityObject) container).getSpaceKey());
        }
        else
        {
            return none();
        }
    }

    @Override
    public String getType()
    {
        return "comment.added";
    }

    @Override
    public ActivityVerb getVerb()
    {
        return post();
    }
}
