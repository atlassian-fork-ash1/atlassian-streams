package com.atlassian.streams.confluence.changereport;

import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.pages.Attachment;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.core.util.thumbnail.Thumbnail;
import com.atlassian.streams.api.ActivityVerb;
import com.atlassian.streams.api.StreamsEntry.ActivityObject;
import com.atlassian.streams.api.StreamsEntry.Renderer;
import com.atlassian.streams.api.common.Option;

import javax.annotation.Nullable;
import java.util.Date;

import static com.atlassian.streams.api.ActivityVerbs.post;
import static com.atlassian.streams.api.ActivityVerbs.update;
import static com.atlassian.streams.api.common.Functions.trimToNone;
import static com.atlassian.streams.api.common.Option.none;
import static com.atlassian.streams.api.common.Option.option;
import static com.google.common.base.Objects.equal;
import static org.apache.commons.lang3.time.DateUtils.MILLIS_PER_MINUTE;

public final class AttachmentActivityItem implements ActivityItem
{
    private final Attachment attachment;
    private final Iterable<Entry> entries;
    private final Iterable<ActivityObject> activityObjects;
    private final Option<ActivityObject> target;
    private final Renderer renderer;

    public AttachmentActivityItem(Attachment attachment,
            Iterable<Entry> entries,
            Iterable<ActivityObject> activityObjects,
            Option<ActivityObject> target,
            Renderer renderer)
    {
        this.attachment = attachment;
        this.entries = entries;
        this.activityObjects = activityObjects;
        this.target = target;
        this.renderer = renderer;
    }

    public Iterable<ActivityObject> getActivityObjects()
    {
        return activityObjects;
    }

    public String getChangedBy()
    {
        final ConfluenceUser lastModifier = attachment.getLastModifier();
        return lastModifier != null ? lastModifier.getName() : null;
    }

    public String getContentType()
    {
        return "attachment";
    }

    public String getIconPath()
    {
        return "/images/icons/attachments/file.gif";
    }

    public Long getId()
    {
        return null;
    }

    public Date getModified()
    {
        return attachment.getLastModificationDate();
    }

    public Renderer getRenderer()
    {
        return renderer;
    }

    public int getVersion()
    {
        return attachment.getVersion();
    }

    public Option<String> getSpaceKey()
    {
        if (attachment.getSpace() != null)
        {
            return option(attachment.getSpace().getKey());
        }
        else
        {
            return none();
        }
    }

    public Option<ActivityObject> getTarget()
    {
        return target;
    }

    public String getType()
    {
        return "attachment." + (attachment.isNew() ? "added" : "modified");
    }

    public String getUrlPath()
    {
        return extractUrlPath(attachment);
    }

    public ActivityVerb getVerb()
    {
        return attachment.isNew() ? post() : update();
    }

    public boolean isAcceptingCommentsFromUser(String username)
    {
        return false;
    }

    public Iterable<Entry> getAttachments()
    {
        return entries;
    }

    public Attachment getEntity()
    {
        return attachment;
    }

    public boolean matches(final Attachment attachment)
    {
        final ConfluenceUser lastModifier = attachment.getLastModifier();
        return equal(getChangedBy(), lastModifier != null ? lastModifier.getName() : null) &&
                getUrlPath().equals(extractUrlPath(attachment)) &&
                Math.abs(getModified().getTime() - attachment.getLastModificationDate().getTime()) < MILLIS_PER_MINUTE;
    }

    @Nullable
    private String extractUrlPath(final Attachment attachment)
    {
        final ContentEntityObject container = attachment.getContainer();
        return container != null ? container.getUrlPath() : null;
    }

    public static final class Entry
    {

        private final Attachment attachment;
        private final Option<Preview> preview;

        public Entry(Attachment attachment, Option<Preview> preview)
        {
            this.attachment = attachment;
            this.preview = preview;
        }

        public String getDownloadPath()
        {
            return attachment.getDownloadPath();
        }

        public String getName()
        {
            return attachment.getFileName();
        }

        public Option<String> getComment()
        {
            return option(attachment.getVersionComment()).flatMap(trimToNone());
        }

        public Option<Preview> getPreview()
        {
            return preview;
        }
    }

    public static final class Preview
    {
        private final String downloadPath;
        private final int height;
        private final int width;

        public Preview(Attachment attachment, Thumbnail thumbnail)
        {
            this.downloadPath = attachment.getDownloadPath();
            this.height = thumbnail.getHeight();
            this.width = thumbnail.getWidth();
        }

        public String getDownloadPath()
        {
            return downloadPath;
        }

        public int getHeight()
        {
            return height;
        }

        public int getWidth()
        {
            return width;
        }
    }
}
