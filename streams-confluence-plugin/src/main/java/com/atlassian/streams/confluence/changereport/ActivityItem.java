package com.atlassian.streams.confluence.changereport;

import java.util.Date;

import com.atlassian.confluence.core.ConfluenceEntityObject;
import com.atlassian.streams.api.ActivityVerb;
import com.atlassian.streams.api.StreamsEntry.ActivityObject;
import com.atlassian.streams.api.StreamsEntry.Renderer;
import com.atlassian.streams.api.common.Option;

public interface ActivityItem
{
    Long getId();

    String getUrlPath();

    Option<String> getSpaceKey();

    String getChangedBy();

    Date getModified();

    String getType();

    String getIconPath();

    boolean isAcceptingCommentsFromUser(String username);

    String getContentType();

    Iterable<ActivityObject> getActivityObjects();

    ActivityVerb getVerb();

    Option<ActivityObject> getTarget();

    Renderer getRenderer();

    int getVersion();

    ConfluenceEntityObject getEntity();
}
