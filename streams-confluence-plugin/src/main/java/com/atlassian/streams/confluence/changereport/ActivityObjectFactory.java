package com.atlassian.streams.confluence.changereport;

import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.pages.BlogPost;
import com.atlassian.confluence.pages.Comment;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceDescription;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.UrlMode;
import com.atlassian.streams.api.ActivityObjectType;
import com.atlassian.streams.api.StreamsEntry.ActivityObject;
import com.atlassian.streams.confluence.RemoteAttachment;
import com.atlassian.streams.confluence.UriProvider;

import java.net.URI;

import static com.atlassian.streams.api.ActivityObjectTypes.article;
import static com.atlassian.streams.api.ActivityObjectTypes.comment;
import static com.atlassian.streams.api.ActivityObjectTypes.file;
import static com.atlassian.streams.api.common.Option.option;
import static com.atlassian.streams.confluence.ConfluenceActivityObjectTypes.page;
import static com.atlassian.streams.confluence.ConfluenceActivityObjectTypes.personalSpace;
import static com.atlassian.streams.confluence.ConfluenceActivityObjectTypes.space;
import static com.google.common.base.Preconditions.checkNotNull;

public class ActivityObjectFactory
{
    private final ApplicationProperties applicationProperties;
    private final UriProvider uriProvider;

    public ActivityObjectFactory(ApplicationProperties applicationProperties, UriProvider uriProvider)
    {
        this.applicationProperties = checkNotNull(applicationProperties, "applicationProperties");
        this.uriProvider = checkNotNull(uriProvider, "uriProvider");
    }

    public ActivityObject newActivityObject(URI baseUri, BlogPost blog)
    {
        return newActivityObjectForEntity(baseUri, blog, article());
    }

    public ActivityObject newActivityObject(URI baseUri, Page page)
    {
        return newActivityObjectForEntity(baseUri, page, page());
    }

    public ActivityObject newActivityObject(SpaceDescription space)
    {
        return new ActivityObject(ActivityObject.params().
            id(applicationProperties.getBaseUrl(UrlMode.CANONICAL) + space.getUrlPath()).
            activityObjectType(space.isPersonalSpace() ? personalSpace() : space()).
            title(option(space.getDisplayTitle())).
            alternateLinkUri(URI.create(applicationProperties.getBaseUrl(UrlMode.CANONICAL) + space.getUrlPath())));
    }

    public ActivityObject newActivityObject(Space space)
    {
        return newActivityObject(new SpaceDescription(space));
    }

    public ActivityObject newActivityObject(RemoteAttachment attachment)
    {
        return new ActivityObject(ActivityObject.params().
            id(applicationProperties.getBaseUrl(UrlMode.CANONICAL) + attachment.getDownloadUrl()).
            activityObjectType(file()).
            title(option(attachment.getName())).
            alternateLinkUri(URI.create(applicationProperties.getBaseUrl(UrlMode.CANONICAL) + attachment.getDownloadUrl())));
    }

    public ActivityObject newActivityObject(AttachmentActivityItem.Entry attachment)
    {
        return new ActivityObject(ActivityObject.params().
            id(applicationProperties.getBaseUrl(UrlMode.CANONICAL) + attachment.getDownloadPath()).
            activityObjectType(file()).
            title(option(attachment.getName())).
            alternateLinkUri(URI.create(applicationProperties.getBaseUrl(UrlMode.CANONICAL) + attachment.getDownloadPath())));
    }

    public ActivityObject newActivityObject(final URI baseUri, final Comment comment)
    {
        return newActivityObjectForEntity(baseUri, comment, comment());
    }

    private ActivityObject newActivityObjectForEntity(final URI baseUri,
                                                      final ContentEntityObject entity,
                                                      final ActivityObjectType type)
    {
        final URI uri = uriProvider.getEntityUri(baseUri, entity);
        return new ActivityObject(ActivityObject.params().
            id(uri.toASCIIString()).
            activityObjectType(type).
            title(option(entity.getTitle())).
            alternateLinkUri(uri));
    }
}
