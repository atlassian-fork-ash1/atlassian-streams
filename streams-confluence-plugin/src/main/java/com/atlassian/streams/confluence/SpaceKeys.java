package com.atlassian.streams.confluence;

import com.atlassian.confluence.core.ListBuilder;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.spaces.SpacesQuery;
import com.atlassian.sal.api.transaction.TransactionCallback;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.atlassian.streams.spi.StreamsKeyProvider.StreamsKey;
import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Ordering;

import java.util.Comparator;

import static com.atlassian.confluence.spaces.SpaceType.GLOBAL;
import static com.atlassian.confluence.spaces.SpaceType.PERSONAL;
import static com.atlassian.confluence.spaces.SpacesQuery.newQuery;
import static com.atlassian.confluence.user.AuthenticatedUserThreadLocal.getUser;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Iterables.transform;

public class SpaceKeys
{
    private final SpaceManager spaceManager;
    private final TransactionTemplate transactionTemplate;

    public SpaceKeys(SpaceManager spaceManager, 
            TransactionTemplate transactionTemplate)
    {
        this.spaceManager = checkNotNull(spaceManager, "spaceManager");
        this.transactionTemplate = checkNotNull(transactionTemplate, "transactionTemplate");
    }

    public Iterable<StreamsKey> get()
    {
        return ImmutableList.<StreamsKey>builder().addAll(spaceKeys()).build();
    }

    private Iterable<StreamsKey> spaceKeys()
    {
        return transform(fetchSpaces(), toStreamsKey());
    }

    private final Function<Space, StreamsKey> toStreamsKey = space -> new StreamsKey(space.getKey(), space.getName());

    private Function<Space, StreamsKey> toStreamsKey()
    {
        return toStreamsKey;
    }

    @SuppressWarnings("unchecked")
    private Iterable<Space> fetchSpaces()
    {
        return (Iterable<Space>) transactionTemplate.execute((TransactionCallback) () -> {
            SpacesQuery spacesQuery = newQuery().forUser(getUser()).build();
            ListBuilder<Space> spaces = spaceManager.getSpaces(spacesQuery);
            return Ordering.from(spaceSorter).sortedCopy(spaces.getRange(0, spaces.getAvailableSize()));
        });
    }

    private Comparator<Space> spaceSorter = (space1, space2) -> {
        //sort all global spaces above personal spaces, then sort alphabetically
        if (space1.getSpaceType().equals(PERSONAL) && space2.getSpaceType().equals(GLOBAL))
        {
            return 1;
        }
        else if (space1.getSpaceType().equals(GLOBAL) && space2.getSpaceType().equals(PERSONAL))
        {
            return -1;
        }
        else
        {
            return space1.getName().compareToIgnoreCase(space2.getName());
        }
    };
}
