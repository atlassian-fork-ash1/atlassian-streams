package com.atlassian.streams.confluence.renderer;

import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.content.render.xhtml.XmlEventReaderFactory;
import com.atlassian.confluence.content.render.xhtml.XmlOutputFactory;
import com.atlassian.confluence.core.BodyContent;
import com.atlassian.confluence.core.BodyType;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.pages.AbstractPage;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.RenderContextOutputType;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.streams.api.ActivityObjectType;
import com.atlassian.streams.api.Html;
import com.atlassian.streams.api.StreamsEntry;
import com.atlassian.streams.api.StreamsEntry.Renderer;
import com.atlassian.streams.api.common.Function2;
import com.atlassian.streams.api.common.Option;
import com.atlassian.streams.api.renderer.StreamsEntryRendererFactory;
import com.atlassian.streams.confluence.UriProvider;
import com.atlassian.streams.spi.renderer.Renderers;
import com.atlassian.templaterenderer.TemplateRenderer;
import com.google.common.base.Function;
import com.google.common.collect.ImmutableMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URI;
import java.util.Map;
import java.util.function.Supplier;

import static com.atlassian.streams.api.Html.html;
import static com.atlassian.streams.api.Html.trimHtmlToNone;
import static com.atlassian.streams.api.common.Option.none;
import static com.atlassian.streams.api.common.Option.some;
import static com.atlassian.streams.confluence.changereport.ContentEntityObjects.isComment;
import static com.atlassian.streams.spi.renderer.Renderers.render;
import static com.atlassian.streams.spi.renderer.Renderers.replaceNbsp;
import static com.atlassian.streams.spi.renderer.Renderers.truncate;
import static com.atlassian.streams.spi.renderer.Renderers.unescapeLineBreaks;
import static com.google.common.base.Functions.compose;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Iterables.get;
import static org.apache.commons.lang3.StringEscapeUtils.escapeHtml4;
import static org.apache.commons.lang3.StringUtils.isBlank;

public final class ContentEntityRendererFactory
{
    private static final Logger log = LoggerFactory.getLogger(ContentEntityRendererFactory.class);

    private final PageManager pageManager;
    private final I18nResolver i18nResolver;
    private final UriProvider uriProvider;
    private final StreamsEntryRendererFactory rendererFactory;
    private final TemplateRenderer templateRenderer;
    private final com.atlassian.confluence.content.render.xhtml.Renderer xhtmlRenderer;
    private XmlEventReaderFactory xmlEventReaderFactory;
    private XmlOutputFactory xmlOutputFactory;

    public ContentEntityRendererFactory(
            PageManager pageManager,
            I18nResolver i18nResolver,
            UriProvider uriProvider,
            StreamsEntryRendererFactory rendererFactory,
            TemplateRenderer templateRenderer,
            com.atlassian.confluence.content.render.xhtml.Renderer xhtmlRenderer,
            XmlEventReaderFactory xmlEventReaderFactory,
            XmlOutputFactory xmlOutputFactory)
    {
        this.pageManager = checkNotNull(pageManager, "pageManager");
        this.i18nResolver = checkNotNull(i18nResolver, "i18nResolver");
        this.uriProvider = checkNotNull(uriProvider, "uriProvider");
        this.rendererFactory = checkNotNull(rendererFactory, "rendererFactory");
        this.templateRenderer = checkNotNull(templateRenderer, "templateRenderer");
        this.xhtmlRenderer = checkNotNull(xhtmlRenderer, "xhtmlRenderer");
        this.xmlEventReaderFactory = checkNotNull(xmlEventReaderFactory, "xmlEventReaderFactory");
        this.xmlOutputFactory = checkNotNull(xmlOutputFactory, "xmlOutputFactory");
    }

    public Renderer newInstance(URI baseUri, ContentEntityObject entity)
    {
        return new ContentEntityRenderer(baseUri, entity);
    }

    private final class ContentEntityRenderer implements Renderer
    {
        private final Function<StreamsEntry, Html> titleRenderer;
        private final Function2<StreamsEntry, Boolean, Option<Html>> contentRenderer;
        private final URI baseUri;

        ContentEntityRenderer(final URI baseUri,
                              final ContentEntityObject entity)
        {
            this.baseUri = baseUri;
            final BodyContent bodyContent = entity.getBodyContent();
            this.contentRenderer = renderContent(entity, bodyContent);
            this.titleRenderer = renderTitle(entity);
        }

        @Override
        public Option<Html> renderContentAsHtml(StreamsEntry entry)
        {
            return contentRenderer.apply(entry, false);
        }

        @Override
        public Option<Html> renderSummaryAsHtml(StreamsEntry entry)
        {
            return contentRenderer.apply(entry, true);
        }

        @Override
        public Html renderTitleAsHtml(StreamsEntry entry)
        {
            return titleRenderer.apply(entry);
        }

        private Function<StreamsEntry, Html> renderTitle(final ContentEntityObject entity)
        {
            if (isComment(entity))
            {
                return rendererFactory.newCommentTitleRenderer();
            }
            else
            {
                return entry -> {
                    Option<ActivityObjectType> objectType = get(entry.getActivityObjects(), 0).getActivityObjectType();
                    String key = String.format("streams.item.confluence.%s.%s",
                            objectType.isDefined() ? objectType.get().key() : "unknown",
                            entry.getVerb().key());

                    return rendererFactory.newTitleRenderer(key).apply(entry);
                };
            }
        }
        private Option<String> getPageDiffUri(final URI baseUri,
                                              final ContentEntityObject entity,
                                              final ContentEntityObject lastVersion)
        {
            if (lastVersion != null)
            {
                int revisedVersion = lastVersion.getVersion();
                return some(uriProvider.getPageDiffUri(baseUri, entity, revisedVersion, revisedVersion + 1)
                        .toASCIIString());
            }
            return none();
        }

        private Function2<StreamsEntry, Boolean, Option<Html>> renderContent(final ContentEntityObject entity,
                final BodyContent bodyContent)
        {
            //lazy load the content to prevent loading the previous page version until we have to
            final Supplier<Option<Html>> content = contentAsHtml(entity, bodyContent);

            return new Function2<StreamsEntry, Boolean, Option<Html>>()
            {
                public Option<Html> apply(StreamsEntry entry, Boolean truncate)
                {
                    return content.get().flatMap(renderContent(entry, truncate));
                }

                private Function<Html, Option<Html>> renderContent(final StreamsEntry entry, final boolean truncate)
                {
                    return c -> {
                        Html description = truncate ? truncate(SUMMARY_LIMIT, c) : c;
                        if (truncate && c.equals(description))
                        {
                            return none(); // we don't want a summary if it will be the same as the content
                        }
                        Map<String, Object> context = ImmutableMap.<String, Object>builder().
                                put("contentHtml", description).
                                put("truncated", truncate).
                                put("isComment", isComment(entity)).
                                put("contentUri", entry.getAlternateLink()).
                                build();

                        return some(new Html(render(templateRenderer, "confluence-content-block.vm", context)));
                    };
                }
            };
        }

        private Supplier<Option<Html>> contentAsHtml(final ContentEntityObject entity, final BodyContent bodyContent)
        {
            return () -> content(entity, pageManager.getPreviousVersion(entity), bodyContent)
                    .map(compose(html(), unescapeLineBreaks())).flatMap(trimHtmlToNone());
        }

        private Option<String> content(ContentEntityObject entity, ContentEntityObject lastVersion,
                final BodyContent bodyContent)
        {
            if (entity.isVersionCommentAvailable())
            {
                return some(blockquote(escapeHtml4(entity.getVersionComment())));
            }
            //we handle multiple page versions but only the latest comment version
            else if (lastVersion != null && entity instanceof AbstractPage)
            {
                boolean renamed = !entity.getTitle().equals(lastVersion.getTitle());
                return getEditedPageContent(entity, lastVersion, renamed);
            }
            else
            {
                RenderContext renderContext = entity.toPageContext();
                renderContext.setOutputType(RenderContextOutputType.FEED);

                if (BodyType.XHTML.equals(bodyContent.getBodyType()))
                {
                    String xml = stripGadgetMacros(entity, entity.getBodyAsString());
                    return some(replaceNbsp(xhtmlRenderer.render(xml, new DefaultConversionContext(renderContext))));
                }
                else
                {
                    return none();
                }
            }
        }

        private String blockquote(String content)
        {
            return content != null ? "<blockquote>" + content + "</blockquote>" : null;
        }

        private String stripGadgetMacros(ContentEntityObject entity, String content)
        {
            if (isBlank(content))
            {
                return content;
            }

            try
            {
                GadgetMacroStripper stripper = new GadgetMacroStripper(xmlEventReaderFactory, xmlOutputFactory);
                return stripper.stripGadgetMacros(content);
            }
            catch(Exception e)
            {
                log.warn("Unable to parse content for entity '" + entity.getDisplayTitle() + "' at " + entity.getUrlPath(), e);
            }

            return content;
        }

        private Option<String> getEditedPageContent(ContentEntityObject entity, ContentEntityObject lastVersion, boolean renamed)
        {
            Option<String> renamedText = renamed ?
                    some(i18nResolver.getText("stream.item.confluence.action.page.renamed", lastVersion.getTitle(), entity.getTitle())) :
                    none(String.class);

            return some(Renderers.render(templateRenderer, "confluence-updated-page.vm",
                    ImmutableMap.<String, Object>of("pageDiffUri",
                            getPageDiffUri(baseUri, entity, lastVersion),
                            "renamedText", renamedText)));
        }
    }
}
