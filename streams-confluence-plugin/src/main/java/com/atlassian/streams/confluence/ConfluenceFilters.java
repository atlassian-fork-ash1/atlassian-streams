package com.atlassian.streams.confluence;

import com.atlassian.streams.api.ActivityRequest;
import com.atlassian.streams.confluence.changereport.ActivityItem;

import com.google.common.base.Predicate;

import static com.atlassian.streams.spi.Filters.getIsValues;
import static com.atlassian.streams.spi.Filters.getNotValues;
import static com.atlassian.streams.spi.StandardStreamsFilterOption.ISSUE_KEY;

public class ConfluenceFilters
{
    /**
     * Determines the search terms (i.e. issue keys) based on the {@code ActivityRequest}.
     *
     * @param request the {@code ActivityRequest} containing the filter information
     * @return the search terms to be searched
     */
    public static Iterable<String> getSearchTerms(ActivityRequest request)
    {
        return getIsValues(request.getStandardFilters().get(ISSUE_KEY.getKey()));
    }

    /**
     * Determines the search terms (i.e. issue keys) to exclude based on the {@code ActivityRequest}.
     *
     * @param request the {@code ActivityRequest} containing the filter information
     * @return the search terms to be excluded
     */
    public static Iterable<String> getExcludedSearchTerms(ActivityRequest request)
    {
        return getNotValues(request.getStandardFilters().get(ISSUE_KEY.getKey()));
    }

    /**
     * {@code Predicate} for filtering based on project "key" (space) filters.
     *
     * @param inProjectPredicate the project predicate
     * @return the {@code Predicate}
     */
    public static Predicate<ActivityItem> activityItemSpace(Predicate<String> inProjectPredicate)
    {
        return new ActivityItemSpace(inProjectPredicate);
    }

    private static final class ActivityItemSpace implements Predicate<ActivityItem>
    {
        private final Predicate<String> inProjectPredicate;

        public ActivityItemSpace(Predicate<String> inProjectPredicate)
        {
            this.inProjectPredicate = inProjectPredicate;
        }

        public boolean apply(ActivityItem activityItem)
        {
            for (String spaceKey : activityItem.getSpaceKey())
            {
                return inProjectPredicate.apply(spaceKey);
            }
            return false;
        }
    }
}
