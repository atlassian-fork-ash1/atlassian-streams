package com.atlassian.streams.confluence.changereport;

import com.atlassian.confluence.pages.AbstractPage;
import com.atlassian.confluence.pages.Comment;
import com.atlassian.confluence.spaces.SpaceDescription;

import java.net.URI;

interface ContentEntityActivityItemFactory
{

    /**
     * Creates an activity item for page entities. If there is a version comment, that is set as the summary.
     * Otherwise, if there is a previous version then the summary indicates the number of changes made.
     *
     *
     * @param baseUri
     * @param page
     * @return activity item
     */
    ActivityItem newActivityItem(URI baseUri, AbstractPage page);

    ActivityItem newActivityItem(URI baseUri, Comment comment);

    ActivityItem newActivityItem(SpaceDescription space, boolean isCreationEvent);

}
