package com.atlassian.streams.confluence;

import java.util.List;

import com.atlassian.confluence.core.ConfluenceEntityObject;
import com.atlassian.confluence.mail.notification.Notification;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.streams.api.common.Pair;
import com.atlassian.user.User;

import com.google.common.base.Function;

import static com.atlassian.streams.api.common.Pair.pair;
import static com.google.common.collect.Iterables.size;

/**
 * Handles Confluence-specific inline action tasks.
 */
public class ConfluenceWatchHelper<T extends ConfluenceEntityObject, K>
{
    public boolean startWatching(T entity,
                                 Function<Pair<User, T>, Void> addNotification,
                                 Function<T, List<Notification>> getNotifications)
    {
        User user = AuthenticatedUserThreadLocal.getUser();
        if (user == null)
        {
            throw new SecurityException("User is not authenticated");
        }

        int previousWatcherCount = size(getNotifications.apply(entity));
        addNotification.apply(pair(user, entity));
        int updatedWatcherCount = size(getNotifications.apply(entity));
        return (updatedWatcherCount == (previousWatcherCount + 1));
    }
}
