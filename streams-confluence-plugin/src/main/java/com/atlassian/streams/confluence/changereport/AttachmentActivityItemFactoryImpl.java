package com.atlassian.streams.confluence.changereport;

import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.pages.Attachment;
import com.atlassian.confluence.pages.BlogPost;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.thumbnail.ThumbnailManager;
import com.atlassian.core.util.thumbnail.Thumbnail;
import com.atlassian.streams.api.StreamsEntry.ActivityObject;
import com.atlassian.streams.api.common.Option;
import com.atlassian.streams.confluence.changereport.AttachmentActivityItem.Entry;
import com.atlassian.streams.confluence.changereport.AttachmentActivityItem.Preview;
import com.atlassian.streams.confluence.renderer.AttachmentRendererFactory;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URI;

import static com.atlassian.streams.api.common.Option.none;
import static com.atlassian.streams.api.common.Option.some;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Iterables.concat;
import static com.google.common.collect.Iterables.transform;

final class AttachmentActivityItemFactoryImpl implements AttachmentActivityItemFactory
{
    private static final Logger log = LoggerFactory.getLogger(AttachmentActivityItemFactoryImpl.class);

    private final ActivityObjectFactory activityObjectFactory;
    private final ThumbnailManager thumbnailManager;
    private final AttachmentRendererFactory attachmentRendererFactory;

    AttachmentActivityItemFactoryImpl(ActivityObjectFactory activityObjectFactory,
            ThumbnailManager thumbnailManager,
            AttachmentRendererFactory attachmentRendererFactory)
    {
        this.activityObjectFactory = checkNotNull(activityObjectFactory, "activityObjectFactory");
        this.thumbnailManager = checkNotNull(thumbnailManager, "thumbnailManager");
        this.attachmentRendererFactory = checkNotNull(attachmentRendererFactory, "attachmentRendererFactory");
    }

    /* (non-Javadoc)
     * @see com.atlassian.streams.confluence.changereport.AttachmentActivityItemFactory#newActivityItem(com.atlassian.confluence.pages.Attachment)
     */
    @Override
    public ActivityItem newActivityItem(URI baseUri, Attachment attachment)
    {
        return newActivityItem(baseUri, attachment, ImmutableList.of(buildAttachmentEntry(attachment)));
    }

    /* (non-Javadoc)
     * @see com.atlassian.streams.confluence.changereport.AttachmentActivityItemFactory#newActivityItem(com.atlassian.confluence.pages.Attachment, com.atlassian.streams.confluence.changereport.AttachmentActivityItem)
     */
    @Override
    public ActivityItem newActivityItem(URI baseUri, Attachment attachment, AttachmentActivityItem attachmentItem)
    {
        return newActivityItem(baseUri, attachment, concat(attachmentItem.getAttachments(),
                ImmutableList.of(buildAttachmentEntry(attachment))));
    }

    private ActivityItem newActivityItem(URI baseUri, Attachment attachment, Iterable<Entry> entries)
    {
        return new AttachmentActivityItem(attachment,
                entries,
                getActivityObjects(entries),
                getTarget(baseUri, attachment.getContainer()),
                attachmentRendererFactory.newInstance(entries));
    }

    private Iterable<ActivityObject> getActivityObjects(Iterable<AttachmentActivityItem.Entry> attachments)
    {
        return ImmutableList.copyOf(transform(attachments, toAttachmentActivityObjects()));
    }

    private Function<AttachmentActivityItem.Entry, ActivityObject> toAttachmentActivityObjects()
    {
        return toActivityObjects;
    }

    private final Function<AttachmentActivityItem.Entry, ActivityObject> toActivityObjects  = new Function<AttachmentActivityItem.Entry, ActivityObject>()
    {
        public ActivityObject apply(AttachmentActivityItem.Entry attachment)
        {
            return activityObjectFactory.newActivityObject(attachment);
        }
    };

    private Option<ActivityObject> getTarget(URI baseUri, ContentEntityObject owner)
    {
        if (owner instanceof BlogPost)
        {
            return some(activityObjectFactory.newActivityObject(baseUri, (BlogPost) owner));
        }
        else if (owner instanceof Page)
        {
            return some(activityObjectFactory.newActivityObject(baseUri, (Page) owner));
        }
        else
        {
            return none();
        }
    }

    private AttachmentActivityItem.Entry buildAttachmentEntry(Attachment attachment)
    {
        Option<AttachmentActivityItem.Preview> preview;

        if (thumbnailManager.isThumbnailable(attachment))
        {
            try
            {
                final Thumbnail thumbnail = thumbnailManager.getThumbnail(attachment);
                preview = some(new Preview(attachment, thumbnail));
            }
            // Confluence throws an IllegalArgumentException if the thumbnail file cannot be opened
            // This is probably because re-indexing is required as it seems to solve the problem.
            catch (final Exception e)
            {
                log.warn("Exception caught trying to get a thumbnail.", e);
                preview = none();
            }
        }
        else
        {
            preview = none();
        }

        return new AttachmentActivityItem.Entry(attachment, preview);
    }
}
