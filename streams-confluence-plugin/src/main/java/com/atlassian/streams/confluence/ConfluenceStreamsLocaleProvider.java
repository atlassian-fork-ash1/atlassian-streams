package com.atlassian.streams.confluence;

import java.util.Locale;

import com.atlassian.confluence.languages.LocaleManager;
import com.atlassian.confluence.languages.LocaleParser;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.streams.spi.StreamsLocaleProvider;

import static com.google.common.base.Preconditions.checkNotNull;

public class ConfluenceStreamsLocaleProvider implements StreamsLocaleProvider
{
    private final LocaleManager localeManager;
    private final SettingsManager settingsManager;

    public ConfluenceStreamsLocaleProvider(final LocaleManager localeManager, final SettingsManager settingsManager)
    {
        this.localeManager = checkNotNull(localeManager, "localeManager");
        this.settingsManager = checkNotNull(settingsManager, "settingsManager");
    }
    
    public Locale getApplicationLocale()
    {
        return LocaleParser.toLocale(settingsManager.getGlobalSettings().getGlobalDefaultLocale());
    }
    
    public Locale getUserLocale()
    {
        return localeManager.getLocale(AuthenticatedUserThreadLocal.getUser());
    }

}
