/**
 * Helper class for hitting review resource to summarize and close reviews.
 */
var ActivityStreams = ActivityStreams || {};

ActivityStreams.ReviewTransitions = ActivityStreams.ReviewTransitions || (function() {

    /**
     * Summarize a review.
     *
     * @param {Event} e Event object
     * @param {Object} activityItem the activity item object
     * @param {String} url the Review REST resource URL
     * @param {Object} feedItem the original feed item data
     */
    function summarize(e, activityItem, url, feedItem) {
        summarizeInternal(e, activityItem, url, false, feedItem);
    }

    /**
     * Summarize a review. If successful, trigger the close action as well.
     *
     * @param {Event} e Event object
     * @param {Object} activityItem the activity item object
     * @param {String} url the Review REST resource URL
     * @param {Object} feedItem the original feed item data
     */
    function summarizeAndClose(e, activityItem, url, feedItem) {
        summarizeInternal(e, activityItem, url, true, feedItem);
    }

    /**
     * Close a review.
     *
     * @param {Event} e Event object
     * @param {Object} activityItem the activity item object
     * @param {String} url the Review REST resource URL
     * @param {Object} feedItem the original feed item data
     */
    function close(e, activityItem, url, feedItem) {
        closeInternal(e, activityItem, url, false, feedItem);
    }

    /**
     * Summarize a review. If successful, trigger the close action as well.
     *
     * @param {Event} e Event object
     * @param {Object} activityItem the activity item object
     * @param {String} url the Review REST resource URL
     * @param {Boolean} closeReview true if the summarize action should be followed with a close action, false if not
     * @param {Object} feedItem the original feed item data
     */
    function summarizeInternal(e, activityItem, url, closeReview, feedItem) {
        var target = AJS.$(e.target);
        AJS.$.ajax({
            type : 'POST',
            contentType: 'application/json',
            url : ActivityStreams.InlineActions.proxy(url + "/transition?action=action:summarizeReview", feedItem),
            global: false,
            beforeSend: function() {
                target.trigger('beginInlineAction');
            },
            complete: function() {
                target.trigger('completeInlineAction');
            },
            success : function() {
                if (closeReview) {
                    closeInternal(target, activityItem, url, true, feedItem);
                } else {
                    ActivityStreams.InlineActions.statusMessage(activityItem, AJS.I18n.getText('streams.fecru.action.review.summarize.success'), 'info');
                }
            },
            error : function(request) {
                ActivityStreams.InlineActions.statusMessage(activityItem, AJS.I18n.getText('streams.fecru.action.review.summarize.failure.general'), 'error');
            }
        });
    }

    /**
     * Close a review.
     *
     * @param {Event} e Event object
     * @param {Object} activityItem the activity item object
     * @param {String} url the transition REST resource URL (minus the "action" parameter)
     * @param {Boolean} alsoSummarized true if summarize action occurred just before, false if not
     * @param {Object} feedItem the original feed item data
     */
    function closeInternal(e, activityItem, url, alsoSummarized, feedItem) {
        var target = AJS.$(e.target);
        AJS.$.ajax({
            type : 'POST',
            contentType: 'application/json',
            url : ActivityStreams.InlineActions.proxy(url + "/transition?action=action:closeReview", feedItem),
            global: false,
            beforeSend: function() {
                target.trigger('beginInlineAction');
            },
            complete: function() {
                target.trigger('completeInlineAction');
            },
            success : function() {
                if (alsoSummarized) {
                    ActivityStreams.InlineActions.statusMessage(activityItem, AJS.I18n.getText('streams.fecru.action.review.summarize.close.success'), 'info');
                } else {
                    ActivityStreams.InlineActions.statusMessage(activityItem, AJS.I18n.getText('streams.fecru.action.review.close.success'), 'info');
                }
            },
            error : function(request) {
                if (alsoSummarized) {
                    ActivityStreams.InlineActions.statusMessage(activityItem, AJS.I18n.getText('streams.fecru.action.review.close.failure.summarize.success'), 'error');
                } else {
                    ActivityStreams.InlineActions.statusMessage(activityItem, AJS.I18n.getText('streams.fecru.action.review.close.failure.general'), 'error');
                }
            }
        });
    }

    return {
        summarize: summarize,
        summarizeAndClose: summarizeAndClose,
        close: close
    };
})();